<?
$lang['language_setting']       ="Language";
$lang['language_en']         	="English";
$lang['language_zhtw']         	="Traditional Chinese";
//foot.php
$lang['foot.copyright']       ="Copyright © Institute for Information Industry";
$lang['foot.privatePolicy']       ="Privacy Policy";
$lang['foot.personalInfoProt']       ="Personal Information Protection";
//welcome.php
$lang['welcome.title']         	="e-DM System";
$lang['welcome.description']    ="Please sign in your account.";
$lang['welcome.input.userID.label']         ="Username";
$lang['welcome.input.userID.placeholder']   ="Please input your username.";
$lang['welcome.input.password.label']       ="Password";
$lang['welcome.input.password.placeholder'] ="Please input your password.";
$lang['welcome.btn.signIn'] ="Sign In";

$lang['welcome.role.si'] ="SI";
$lang['welcome.role.si.description'] ="description here";
$lang['welcome.role.si.function.accountManage'] ="Account Manage";
$lang['welcome.role.si.function.solutionManage'] ="Solution Manage";
$lang['welcome.role.si.function.productReview'] ="Product Review";

$lang['welcome.role.vendor'] ="Vendor";
$lang['welcome.role.vendor.description'] ="description here";
$lang['welcome.role.vendor.function.productUpload'] ="Product Upload";

$lang['welcome.role.buyer'] ="Buyer";
$lang['welcome.role.buyer.description'] ="description here";
$lang['welcome.role.buyer.function.solutionReview'] ="Solution Review";

//head.php
$lang['head.title']						="e-DM System";
$lang['head.accountAdmin']				="Account Admin";
$lang['head.accountAdmin.vender']		="Vender Admin";
$lang['head.accountAdmin.buyer']		="Buyer Admin";
$lang['head.accountAdmin.si']			="SI Admin";
$lang['head.accountAdmin.root']			="ROOT Admin";

$lang['head.products']			="Products";
$lang['head.products.review']	="Review";

$lang['head.actions']			="Actions";
$lang['head.actions.add']		="Add";
$lang['head.actions.products']	="Products";
$lang['head.actions.solutions']	="Solutions";


$lang['head.total_solution']		="Total Solution";
$lang['head.total_solution.add_new']="Add New";
$lang['head.total_solution.list']	="List";

$lang['head.account.edit']			="Edit";
$lang['head.account.logout']		="Logout";
//login_view.php
$lang['login_view.accountAdmin']				="Account Admin";
$lang['login_view.accountAdmin.description']	="description here";
$lang['login_view.accountAdmin.vender']		="Vender Admin";
$lang['login_view.accountAdmin.buyer']		="Buyer Admin";
$lang['login_view.accountAdmin.si']			="SI Admin";
$lang['login_view.accountAdmin.root']			="ROOT Admin";

$lang['login_view.products']				="Products";
$lang['login_view.products.description']	="description here";
$lang['login_view.products.review']			="Review";

$lang['login_view.actions']				="Actions";
$lang['login_view.actions.add']			="Add";
$lang['login_view.actions.products']	="Products";
$lang['login_view.actions.solutions']	="Solutions";

$lang['login_view.total_solution']			="Total Solution";
$lang['login_view.total_solution.description']	="description here";
$lang['login_view.total_solution.solutions']	="Solutions";

//listRoleAccount.php
$lang['account/listRoleAccount.btn.add']		="Add";
$lang['account/listRoleAccount.btn.modify']		="Modify";
$lang['account/listRoleAccount.btn.remove']		="Remove";
$lang['account/listRoleAccount.btn.permission']	="Permission";
$lang['account/listRoleAccount.btn.close']		="Close";
$lang['account/listRoleAccount.btn.save']		="Save changes";

$lang['account/listRoleAccount.notice.noUsers']	="No user list, please add a user.";
$lang['account/listRoleAccount.accountAdmin']	="Account Admin";
$lang['account/listRoleAccount.role.vendor']	="Vender";
$lang['account/listRoleAccount.role.buyer']		="Buyer";
$lang['account/listRoleAccount.role.si']		="SI";
$lang['account/listRoleAccount.role.root']		="ROOT";

$lang['account/listRoleAccount.table.column.userid']	="USER ID";
$lang['account/listRoleAccount.table.column.name']		="NAME";
$lang['account/listRoleAccount.table.column.password']	="PASSWORD";
$lang['account/listRoleAccount.table.column.company']	="COMPANY";
$lang['account/listRoleAccount.table.column.tel']		="TELPHONE";
$lang['account/listRoleAccount.table.column.email']		="EMAIL";
$lang['account/listRoleAccount.table.column.site']		="Company Web Site";
$lang['account/listRoleAccount.table.column.actions']	="ACTIONS";

$lang['account/listRoleAccount.formModal.title']			="Account Setting";
$lang['account/listRoleAccount.siPermissionModal.title']	="Permission Setting";
$lang['account/listRoleAccount.siPermissionModal.si_list']	="SI List";
$lang['account/listRoleAccount.siPermissionModal.view_list']="View List";

$lang['account/listRoleAccount.js.rule.msg.password.required']="Please input 4~8 words";
$lang['account/listRoleAccount.js.rule.msg.userid.required']="Please input your id";
$lang['account/listRoleAccount.js.rule.msg.userid.rangelength']="user id is less than 20 words";
$lang['account/listRoleAccount.js.rule.msg.userid.remote']="This user id is already uesd!";
$lang['account/listRoleAccount.js.alert.removeCheck']="Are you sure to delete?";

//account/editForm.php
$lang['account/editForm.userid']	="USER ID";
$lang['account/editForm.name']		="NAME";
$lang['account/editForm.password']	="PASSWORD";
$lang['account/editForm.company']	="COMPANY";
$lang['account/editForm.tel']		="TELPHONE";
$lang['account/editForm.email']		="EMAIL";
$lang['account/editForm.site']		="Company Web Site";

$lang['account/editForm.password.old']		="Original Password";
$lang['account/editForm.password.new']		="New Password";
$lang['account/editForm.password.repeat']	="Password Again";

$lang['account/editForm.nav.personalInfo']	="Edit Personal Info.";
$lang['account/editForm.nav.password']		="Edit Password";
$lang['account/editForm.nav.picture']		="Edit Picture";

$lang['account/editForm.btn.close']		="Close";
$lang['account/editForm.btn.save']		="Save changes";

$lang['account/editForm.picture.process']	="Image Process";
$lang['account/editForm.alert.saveSuccess']	="Save Successfully!";
$lang['account/editForm.alert.cropSuccess']	="Modify Successfully!";
$lang['account/editForm.alert.fail']		="ERROR! Can not save.";

//product/viewList.php
$lang['product/viewList.title']		="Product List";
$lang['product/viewList.btn.add']	="Add";
$lang['product/viewList.btn.showCataArea']	="show/hide";

$lang['product/viewList.btn.modify']	="Modify";
$lang['product/viewList.btn.remove']	="Remove";
$lang['product/viewList.btn.detail']	="Detail";

$lang['product/viewList.search.placeholder']	="Search for...";
$lang['product/viewList.notice.noProducts']	="You have no product!";

$lang['product/viewList.table.column.order']		="order";
$lang['product/viewList.table.column.img']			="Image";
$lang['product/viewList.table.column.name']			="Name";
$lang['product/viewList.table.column.brand']		="Brands/Manufacturer";
$lang['product/viewList.table.column.description']	="Description";
$lang['product/viewList.table.column.isVerified']	="isVerified";
$lang['product/viewList.table.column.uploader']		="Uploader";
$lang['product/viewList.table.column.actions']		="Actions";

$lang['product/viewList.isVerified.yes']	="Yes";
$lang['product/viewList.isVerified.no']		="No";

$lang['product/viewList.alert.changed']		="Changed Successfully!";
$lang['product/viewList.alert.error']		="something error...";
$lang['product/viewList.alert.remove']		="Are you sure to delete？";
//Category
$lang['Category.iCampus']				="iCampus Category";
$lang['Category.iCampus.iLearning']		="iLearning";
$lang['Category.iCampus.iManagement']	="iManagement";
$lang['Category.iCampus.iGovernance']	="iGovernance";
$lang['Category.iCampus.iSocial']		="iSocial";
$lang['Category.iCampus.iHealth']		="iHealth";
$lang['Category.iCampus.iGreen']		="iGreen";
$lang['Category.iCampus.others']		="others";

$lang['Category.product']					="Product Category";
$lang['Category.product.total_solution']	="Total Solution";
$lang['Category.product.service']			="Service";
$lang['Category.product.software']			="Software";
$lang['Category.product.hardware']			="Hardware";
$lang['Category.product.digital_content']	="Digital Content";
$lang['Category.product.others']			="Others";

$lang['Category.eduLevel']					="Eduction Level";
$lang['Category.eduLevel.kindergarden']		="Kindergarden";
$lang['Category.eduLevel.elementary']		="Elementary School";
$lang['Category.eduLevel.high_school']		="High School";
$lang['Category.eduLevel.higher_education']="Higher Education";
$lang['Category.eduLevel.vocational']		="Vocational Training";
$lang['Category.eduLevel.others']			="Others";

//Product
$lang['Product.info']					="Product";
$lang['Product.info.name']				="Product Name";
$lang['Product.info.brand']				="Brands / Manufacturer";
$lang['Product.info.model']				="Product Model";
$lang['Product.info.icampus_category']	="iCampus Category";
$lang['Product.info.product_category']	="Product Category";
$lang['Product.info.eduLevel']			="Eduction Level";

$lang['Product.detail']					="Product Detail";
$lang['Product.detail.introduction']	="Product Introduction";
$lang['Product.detail.description']		="Function Description";
$lang['Product.detail.techSpec']		="Technological Spec";
$lang['Product.detail.largeImage']		="Large Picture (800p*300p)";
$lang['Product.detail.smallImage']		="Small Picture (200p*200p)";
$lang['Product.detail.document']		="Document Upload(PDF, DOC)";
$lang['Product.detail.youtubeURL']		="Youtube Video URL";
$lang['Product.detail.homepage']		="Product Homepage";

$lang['Product.warranty']				="Warranty and After-Service ";
$lang['Product.warranty.warranty']		="Installation and Deployment";
$lang['Product.warranty.deployment']	="Function Description";
$lang['Product.warranty.training']		="Training Service";

$lang['Product.pricing']				="Pricing";
$lang['Product.pricing.unit_price']		="Unit Price";
$lang['Product.pricing.batch_prices']	="Batch Prices";
$lang['Product.pricing.detail']			="Prices Detail and Notes";
$lang['Product.pricing.si.unit_price']	="Unit Price(SI)";
$lang['Product.pricing.si.batch_prices']="Batch Prices(SI)";
$lang['Product.pricing.si.detail']		="Prices Detail and Notes(SI)";

$lang['Product.contact']				="Contact Information";
$lang['Product.contact.contact_name']	="Contact Name";
$lang['Product.contact.company_name']	="Company Name";
$lang['Product.contact.company_site']	="Company Website";
$lang['Product.contact.tel']			="Tel";
$lang['Product.contact.email']			="Email";
$lang['Product.contact.other']			="Other Product Information / Memo";

//product/view.php
$lang['product/view.btn.view_list']	="View List";
$lang['product/view.btn.modify']	="Modify";
$lang['product/view.btn.next']		="Next";

//product/productForm.php
$lang['product/productForm.btn.next']		="Next";
$lang['product/productForm.btn.save']		="Save";
$lang['product/productForm.btn.close']		="Close";
$lang['product/productForm.btn.saveChanges']		="Save changes";
$lang['product/productForm.modal.imgProcess']		="Image Process";
$lang['product/productForm.confirm.save']		="You cannot modify after you press 'OK'!";

//Solution
$lang['Solution.name']			="Total Solution Name";
$lang['Solution.introduction']	="Total Solution Introduction";
$lang['Solution.description']	="Total Solution Description";
$lang['Solution.notes']			="Notes";

//solution/detail.php
$lang['solution/detail.btn.list']		="Total Solution List";
$lang['solution/detail.btn.detail']		="Detail";
$lang['solution/detail.heading']		="Total Solution View";
$lang['solution/detail.tab.product']	="Products";
$lang['solution/detail.tab.comment']	="Comment";
$lang['solution/detail.table.column.order']		="order";
$lang['solution/detail.table.column.img']			="Image";
$lang['solution/detail.table.column.name']			="Name";
$lang['solution/detail.table.column.brand']		="Brands/Manufacturer";
$lang['solution/detail.table.column.description']	="Description";
$lang['solution/detail.table.column.actions']		="Actions";
$lang['solution/detail.noComments']		="No comments!";
$lang['solution/detail.noProducts']		="Please search products and add to total souction!";

//solution/viewList.php
$lang['solution/viewList.btn.add']		="Add";
$lang['solution/viewList.btn.detail']	="Detail";
$lang['solution/viewList.btn.setting']	="Setting";
$lang['solution/viewList.btn.modify']	="Modify";
$lang['solution/viewList.btn.pdf']		="Pdf";
$lang['solution/viewList.btn.remove']	="Remove";
$lang['solution/viewList.btn.showCataArea']	="show/hide";
$lang['solution/viewList.btn.list']		="All Total Solutions";

$lang['solution/viewList.heading']		="Total Solution List";
$lang['solution/viewList.search.placeholder']	="Search for...";
$lang['solution/viewList.table.column.actions']		="Actions";
$lang['solution/viewList.table.column.order']		="#";
$lang['solution/viewList.table.column.creator']		="Creator";
$lang['solution/viewList.alert.remove']		="Are you sure to delete？";

$lang['Solution.step.new']	= "New Total Solution";
$lang['Solution.step.add']	= "Add Product";
$lang['Solution.step.view']	= "View solution";
$lang['Solution.step.auth']	= "Authority & Export";

//solution/gotoAuthority.php
$lang['solution/gotoAuthority.heading']		="Set Total Solution View/ Authority and Export";
$lang['solution/gotoAuthority.isActive']	="Total Solution isActive?";
$lang['solution/gotoAuthority.isShowPrice']	="Show Price?";
$lang['solution/gotoAuthority.btn.yes']		="YES";
$lang['solution/gotoAuthority.btn.no']		="NO";
$lang['solution/gotoAuthority.btn.save']	="Save";
$lang['solution/gotoAuthority.btn.list']	="Total Solution List";
$lang['solution/gotoAuthority.customerAuth']	="Customer Authority Setting(Which Customer Can See This Total Solution)";
$lang['solution/gotoAuthority.customerAuth.customerList']	="Customer List";
$lang['solution/gotoAuthority.customerAuth.viewerList']		="Viewer List";
$lang['solution/gotoAuthority.alert.saveOK']	="Save Success";
$lang['solution/gotoAuthority.alert.saveFail']	="Something error...";

//solution/form.php
$lang['solution/form.heading']		="New Total Solution";
$lang['solution/form.name']			="Name";
$lang['solution/form.brand']		="Brands/Manufacturer";
$lang['solution/form.introduction']	="Introduction";
$lang['solution/form.description']	="Description";
$lang['solution/form.notes']		="Notes";

$lang['solution/form.btn.create']	="Create New Total Solution";
$lang['solution/form.btn.list']		="List Total Solutions";

//solution/addProduct.php
$lang['solution/addProduct.btn.preview']	="Save Total Solution";
$lang['solution/addProduct.btn.detail']		="Detail";
$lang['solution/addProduct.btn.addInto']	="Add Into Solution";
$lang['solution/addProduct.btn.delFrom']	="Delete From Solution";

$lang['solution/addProduct.searchHeading_start']	="Product Search - Add";
$lang['solution/addProduct.searchHeading_end']		="items";
$lang['solution/addProduct.search.placeholder']	="Search for...";
$lang['solution/addProduct.notice.noProducts']	="You have no product!";
$lang['solution/addProduct.btn.showCataArea']	="show/hide";
$lang['solution/addProduct.btn.list']	="product list";

$lang['solution/addProduct.table.column.order']		="order";
$lang['solution/addProduct.table.column.img']			="Image";
$lang['solution/addProduct.table.column.name']			="Name";
$lang['solution/addProduct.table.column.brand']		="Brands/Manufacturer";
$lang['solution/addProduct.table.column.description']	="Description";
$lang['solution/addProduct.table.column.isVerified']	="isVerified";
$lang['solution/addProduct.table.column.uploader']		="Uploader";
$lang['solution/addProduct.table.column.actions']		="Actions";
$lang['solution/addProduct.table.column.upDown']	="UP/DOWN";

$lang['solution/addProduct.isVerified.yes']		="Yes";
$lang['solution/addProduct.isVerified.no']		="No";

//solution/viewSolution.php
$lang['solution/viewSolution.heading']		="Total Solution View";
$lang['solution/viewSolution.btn.addMore']	="Add More Product";
$lang['solution/viewSolution.btn.finish']	="Finish";
$lang['solution/viewSolution.alert.saveOK']	="save success";
$lang['solution/viewSolution.alert.saveFail']	="save fail";
$lang['solution/viewSolution.alert.top']	="This item is the top.";
$lang['solution/viewSolution.alert.bottom']	="This item is the bottom.";

//buyer/viewList
$lang['buyer/viewList.heading']	="Your Solution List";
$lang['buyer/viewList.btn.detail']	="Detail";
$lang['buyer/viewList.search.placeholder']	="Search for...";
$lang['buyer/viewList.notice.noProducts']	="You have no product!";
$lang['buyer/viewList.btn.showCataArea']	="show/hide";
$lang['buyer/viewList.btn.list']	="solution list";
$lang['buyer/viewList.noSolutions']	="You have no Solutions!";

//buyer/detail.php
$lang['buyer/detail.btn.list']		="Total Solution List";
$lang['buyer/detail.btn.detail']	="Detail";
$lang['buyer/detail.btn.post']		="Post";
$lang['buyer/detail.heading']		="Total Solution View";
$lang['buyer/detail.tab.product']	="Products";
$lang['buyer/detail.tab.comment']	="Comment";
$lang['buyer/detail.table.column.order']		="order";
$lang['buyer/detail.table.column.img']			="Image";
$lang['buyer/detail.table.column.name']			="Name";
$lang['buyer/detail.table.column.brand']		="Brands/Manufacturer";
$lang['buyer/detail.table.column.description']	="Description";
$lang['buyer/detail.table.column.actions']		="Actions";
$lang['buyer/detail.noComments']		="No comments!";
$lang['buyer/detail.noProducts']		="Please search products and add to total souction!";
$lang['buyer/detail.reply']		="Reply";
$lang['buyer/detail.on']		="on";
$lang['buyer/detail.noComments']		="No comments!";
?>
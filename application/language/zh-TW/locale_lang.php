<?
$lang['language_setting']       ="語系";
$lang['language_en']         	="英文";
$lang['language_zhtw']         	="正體中文";
//foot.php
$lang['foot.copyright']       	="財團法人資策會版權所有";
$lang['foot.privatePolicy']     ="隱私權政策";
$lang['foot.personalInfoProt']  ="個資宣告";
//welcome.php
$lang['welcome.title']          			="數位型錄系統";
$lang['welcome.description']    			="請登入帳號。";
$lang['welcome.input.userID.label']         ="帳號";
$lang['welcome.input.userID.placeholder']   ="請輸入帳號";
$lang['welcome.input.password.label']       ="密碼";
$lang['welcome.input.password.placeholder'] ="請輸入密碼";
$lang['welcome.btn.signIn'] 				="登入";

$lang['welcome.role.si'] 						="整合商";
$lang['welcome.role.si.description'] 			="功能描述";
$lang['welcome.role.si.function.accountManage'] ="帳號管理";
$lang['welcome.role.si.function.solutionManage']="解決方案管理";
$lang['welcome.role.si.function.productReview'] ="產品檢閱";

$lang['welcome.role.vendor'] 						="產品廠商";
$lang['welcome.role.vendor.description'] 			="功能描述";
$lang['welcome.role.vendor.function.productUpload'] ="商品上傳";

$lang['welcome.role.buyer'] 						="產品買家";
$lang['welcome.role.buyer.description'] 			="功能描述";
$lang['welcome.role.buyer.function.solutionReview'] ="解決方案一覽";

//head.php
$lang['head.title']					="數位型錄系統";
$lang['head.accountAdmin']			="帳號管理";
$lang['head.accountAdmin.vender']	="廠商管理";
$lang['head.accountAdmin.buyer']	="買家管理";
$lang['head.accountAdmin.si']		="整合商管理";
$lang['head.accountAdmin.root']		="最高權限管理";

$lang['head.products']			="產品";
$lang['head.products.review']	="檢閱";

$lang['head.actions']			="動作";
$lang['head.actions.add']		="新增";
$lang['head.actions.products']	="產品";
$lang['head.actions.solutions']	="解決方案";


$lang['head.total_solution']			="解決方案";
$lang['head.total_solution.add_new']	="新增";
$lang['head.total_solution.list']		="列表";

$lang['head.account.edit']			="編輯";
$lang['head.account.logout']		="登出";
//login_view.php
$lang['login_view.accountAdmin']				="帳號管理";
$lang['login_view.accountAdmin.description']	="功能描述";
$lang['login_view.accountAdmin.vender']			="廠商管理";
$lang['login_view.accountAdmin.buyer']			="買家管理";
$lang['login_view.accountAdmin.si']				="整合商管理";
$lang['login_view.accountAdmin.root']			="最高權限管理";

$lang['login_view.products']				="產品";
$lang['login_view.products.description']	="功能描述";
$lang['login_view.products.review']			="檢閱";

$lang['login_view.actions']				="動作";
$lang['login_view.actions.add']			="新增";
$lang['login_view.actions.products']	="商品";
$lang['login_view.actions.solutions']	="解決方案";

$lang['login_view.total_solution']				="整體解決方案";
$lang['login_view.total_solution.description']	="功能描述";
$lang['login_view.total_solution.solutions']	="解決方案";

//listRoleAccount.php
$lang['account/listRoleAccount.btn.add']		="新增帳號";
$lang['account/listRoleAccount.btn.modify']		="修改";
$lang['account/listRoleAccount.btn.remove']		="移除";
$lang['account/listRoleAccount.btn.permission']	="權限";
$lang['account/listRoleAccount.btn.close']		="關閉";
$lang['account/listRoleAccount.btn.save']		="儲存";

$lang['account/listRoleAccount.notice.noUsers']	="找不到任何帳號，請新增";
$lang['account/listRoleAccount.accountAdmin']	="帳號管理";
$lang['account/listRoleAccount.role.vendor']	="廠商";
$lang['account/listRoleAccount.role.buyer']		="買家";
$lang['account/listRoleAccount.role.si']		="整合商";
$lang['account/listRoleAccount.role.root']		="最高權限";

$lang['account/listRoleAccount.table.column.userid']	="帳號";
$lang['account/listRoleAccount.table.column.name']		="名字";
$lang['account/listRoleAccount.table.column.password']	="密碼";
$lang['account/listRoleAccount.table.column.company']	="公司";
$lang['account/listRoleAccount.table.column.tel']		="電話";
$lang['account/listRoleAccount.table.column.email']		="信箱";
$lang['account/listRoleAccount.table.column.site']		="網址";
$lang['account/listRoleAccount.table.column.actions']	="動作";

$lang['account/listRoleAccount.formModal.title']			="帳號設定";
$lang['account/listRoleAccount.siPermissionModal.title']	="權限設定";
$lang['account/listRoleAccount.siPermissionModal.si_list']	="整合商列表";
$lang['account/listRoleAccount.siPermissionModal.view_list']="觀看";

$lang['account/listRoleAccount.js.rule.msg.password.required']="請輸入4~8的密碼";
$lang['account/listRoleAccount.js.rule.msg.userid.required']="請輸入名稱";
$lang['account/listRoleAccount.js.rule.msg.userid.rangelength']="請輸入20字以內";
$lang['account/listRoleAccount.js.rule.msg.userid.remote']="該帳號已被使用!";
$lang['account/listRoleAccount.js.alert.removeCheck']="確認刪除該帳號?";

//account/editForm.php
$lang['account/editForm.userid']	="帳號";
$lang['account/editForm.name']		="名字";
$lang['account/editForm.password']	="密碼";
$lang['account/editForm.company']	="公司";
$lang['account/editForm.tel']		="電話";
$lang['account/editForm.email']		="信箱";
$lang['account/editForm.site']		="網址";

$lang['account/editForm.nav.personalInfo']	="編輯個人資訊";
$lang['account/editForm.nav.password']		="更新密碼";
$lang['account/editForm.nav.picture']		="更新圖片";

$lang['account/editForm.password.old']		="原始密碼";
$lang['account/editForm.password.new']		="新密碼";
$lang['account/editForm.password.repeat']	="再輸入一次";

$lang['account/editForm.btn.close']		="關閉";
$lang['account/editForm.btn.save']		="儲存";

$lang['account/editForm.picture.process']	="照片處理";
$lang['account/editForm.alert.saveSuccess']	="儲存成功!";
$lang['account/editForm.alert.cropSuccess']	="修改成功!";
$lang['account/editForm.alert.fail']		="發生錯誤!";

//product/viewList.php
$lang['product/viewList.title']		="產品一覽";
$lang['product/viewList.btn.add']	="新增";
$lang['product/viewList.btn.showCataArea']	="顯示/隱藏";

$lang['product/viewList.btn.modify']	="修改";
$lang['product/viewList.btn.remove']	="刪除";
$lang['product/viewList.btn.detail']	="詳細";

$lang['product/viewList.search.placeholder']	="搜尋...";
$lang['product/viewList.notice.noProducts']	="目前沒有產品!";

$lang['product/viewList.table.column.order']		="排序";
$lang['product/viewList.table.column.img']			="圖片";
$lang['product/viewList.table.column.name']			="名稱";
$lang['product/viewList.table.column.brand']		="品牌/製造商";
$lang['product/viewList.table.column.description']	="描述";
$lang['product/viewList.table.column.isVerified']	="是否審核通過?";
$lang['product/viewList.table.column.uploader']		="上傳者";
$lang['product/viewList.table.column.actions']		="動作";

$lang['product/viewList.isVerified.yes']	="是";
$lang['product/viewList.isVerified.no']		="否";

$lang['product/viewList.alert.changed']		="修改成功!";
$lang['product/viewList.alert.error']		="發生錯誤...";
$lang['product/viewList.alert.remove']		="確認要刪除？";
//Category
$lang['Category.iCampus']				="智慧校園分類";
$lang['Category.iCampus.iLearning']		="i學習";
$lang['Category.iCampus.iManagement']	="i管理";
$lang['Category.iCampus.iGovernance']	="i校務";
$lang['Category.iCampus.iSocial']		="i社群";
$lang['Category.iCampus.iHealth']		="i健康";
$lang['Category.iCampus.iGreen']		="i綠能";
$lang['Category.iCampus.others']		="其它";

$lang['Category.product']					="產品分類";
$lang['Category.product.total_solution']	="整體解決方案";
$lang['Category.product.service']			="服務";
$lang['Category.product.software']			="軟體";
$lang['Category.product.hardware']			="硬體";
$lang['Category.product.digital_content']	="數位內容";
$lang['Category.product.others']			="其他";

$lang['Category.eduLevel']					="教育程度";
$lang['Category.eduLevel.kindergarden']		="幼稚園";
$lang['Category.eduLevel.elementary']		="國小";
$lang['Category.eduLevel.high_school']		="中學(國高中)";
$lang['Category.eduLevel.higher_education']="大學教育";
$lang['Category.eduLevel.vocational']		="職業學校";
$lang['Category.eduLevel.others']			="其他";

//Product
$lang['Product.info']					="商品資訊";
$lang['Product.info.name']				="商品名稱";
$lang['Product.info.brand']				="品牌/製造商";
$lang['Product.info.model']				="產品模式";
$lang['Product.info.icampus_category']	="智慧校園分類";
$lang['Product.info.product_category']	="產品分類";
$lang['Product.info.eduLevel']			="教育程度";

$lang['Product.detail']					="詳細資訊";
$lang['Product.detail.introduction']	="商品介紹";
$lang['Product.detail.description']		="功能描述";
$lang['Product.detail.techSpec']		="技術規格";
$lang['Product.detail.largeImage']		="大圖 (800p*300p)";
$lang['Product.detail.smallImage']		="縮圖 (200p*200p)";
$lang['Product.detail.document']		="文件(PDF, DOC)";
$lang['Product.detail.youtubeURL']		="Youtube";
$lang['Product.detail.homepage']		="商品網頁";

$lang['Product.warranty']				="保固與售貨服務";
$lang['Product.warranty.warranty']		="保固與售貨服務";
$lang['Product.warranty.deployment']	="安裝與配置";
$lang['Product.warranty.training']		="訓練服務";

$lang['Product.pricing']				="價錢";
$lang['Product.pricing.unit_price']		="單價";
$lang['Product.pricing.batch_prices']	="批發價";
$lang['Product.pricing.detail']			="價格說明";
$lang['Product.pricing.si.unit_price']	="單價(SI)";
$lang['Product.pricing.si.batch_prices']="批發價(SI)";
$lang['Product.pricing.si.detail']		="價格說明(SI)";

$lang['Product.contact']				="連絡資訊";
$lang['Product.contact.contact_name']	="連絡人";
$lang['Product.contact.company_name']	="廠商名稱";
$lang['Product.contact.company_site']	="廠商網站";
$lang['Product.contact.tel']			="電話";
$lang['Product.contact.email']			="信箱";
$lang['Product.contact.other']			="其他資訊";

//product/view.php
$lang['product/view.btn.view_list']	="總覽";
$lang['product/view.btn.modify']	="修改";
$lang['product/view.btn.next']		="下一步";

//product/productForm.php
$lang['product/productForm.btn.next']		="下一步";
$lang['product/productForm.btn.save']		="儲存";
$lang['product/productForm.btn.close']		="關閉";
$lang['product/productForm.btn.saveChanges']		="儲存改變";
$lang['product/productForm.modal.imgProcess']		="影像處理";
$lang['product/productForm.confirm.save']		="儲存後將不能再修正?";

//Solution
$lang['Solution.name']			="名稱";
$lang['Solution.introduction']	="介紹";
$lang['Solution.description']	="描述";
$lang['Solution.notes']			="備註";

//solution/detail.php
$lang['solution/detail.btn.list']		="解決方案列表";
$lang['solution/detail.btn.detail']		="詳細";
$lang['solution/detail.heading']		="解決方案一覽";
$lang['solution/detail.tab.product']	="商品列表";
$lang['solution/detail.tab.comment']	="意見回饋";
$lang['solution/detail.table.column.order']			="排序";
$lang['solution/detail.table.column.img']			="圖片";
$lang['solution/detail.table.column.name']			="名稱";
$lang['solution/detail.table.column.brand']			="品牌/製造商";
$lang['solution/detail.table.column.description']	="描述";
$lang['solution/detail.table.column.actions']		="動作";
$lang['solution/detail.noComments']					="目前沒有任何問題!";
$lang['solution/detail.noProducts']					="目前沒有任何商品!";

//solution/viewList.php
$lang['solution/viewList.btn.add']			="新增";
$lang['solution/viewList.btn.detail']		="詳細";
$lang['solution/viewList.btn.setting']		="設定";
$lang['solution/viewList.btn.modify']		="修改";
$lang['solution/viewList.btn.pdf']			="Pdf";
$lang['solution/viewList.btn.remove']		="移除";
$lang['solution/viewList.btn.showCataArea']	="顯示/隱藏";
$lang['solution/viewList.btn.list']			="全部列表";

$lang['solution/viewList.heading']				="解決方案列表";
$lang['solution/viewList.search.placeholder']	="尋找...";
$lang['solution/viewList.table.column.actions']	="動作";
$lang['solution/viewList.table.column.order']	="排序";
$lang['solution/viewList.table.column.creator']	="建立者";
$lang['solution/viewList.alert.remove']			="確認要刪除？";

$lang['Solution.step.new']	= "新增解決方案";
$lang['Solution.step.add']	= "增加商品";
$lang['Solution.step.view']	= "確認解決方案";
$lang['Solution.step.auth']	= "權限設定";

//solution/gotoAuthority.php
$lang['solution/gotoAuthority.heading']		="權限設定";
$lang['solution/gotoAuthority.isActive']	="是否顯示?";
$lang['solution/gotoAuthority.isShowPrice']	="是否顯示價格?";
$lang['solution/gotoAuthority.btn.yes']		="是";
$lang['solution/gotoAuthority.btn.no']		="否";
$lang['solution/gotoAuthority.btn.save']	="儲存";
$lang['solution/gotoAuthority.btn.list']	="解決方案列表";
$lang['solution/gotoAuthority.customerAuth']	="可觀看帳號列表";
$lang['solution/gotoAuthority.customerAuth.customerList']	="顧客列表";
$lang['solution/gotoAuthority.customerAuth.viewerList']		="可觀看顧客列表";
$lang['solution/gotoAuthority.alert.saveOK']	="儲存成功";
$lang['solution/gotoAuthority.alert.saveFail']	="發生錯誤...";

//solution/form.php
$lang['solution/form.heading']		="新增解決方案";
$lang['solution/form.name']			="名稱";
$lang['solution/form.brand']		="品牌/製造商";
$lang['solution/form.description']	="描述";
$lang['solution/form.introduction']	="介紹";
$lang['solution/form.notes']		="備註";

$lang['solution/form.btn.create']	="建立";
$lang['solution/form.btn.list']		="解決方案列表";

//solution/addProduct.php
$lang['solution/addProduct.btn.preview']	="儲存解決方案";
$lang['solution/addProduct.btn.detail']		="詳細";
$lang['solution/addProduct.btn.addInto']	="加入";
$lang['solution/addProduct.btn.delFrom']	="移除";

$lang['solution/addProduct.searchHeading_start']	="產品搜尋 - 已增加";
$lang['solution/addProduct.searchHeading_end']		="物品";
$lang['solution/addProduct.search.placeholder']		="搜尋...";
$lang['solution/addProduct.notice.noProducts']		="目前沒有產品!";
$lang['solution/addProduct.btn.showCataArea']	="顯示/隱藏";
$lang['solution/addProduct.btn.list']			="產品一覽";

$lang['solution/addProduct.table.column.order']		="排序";
$lang['solution/addProduct.table.column.img']			="圖片";
$lang['solution/addProduct.table.column.name']			="名稱";
$lang['solution/addProduct.table.column.brand']		="品牌/製造商";
$lang['solution/addProduct.table.column.description']	="描述";
$lang['solution/addProduct.table.column.isVerified']	="是否審核通過?";
$lang['solution/addProduct.table.column.uploader']		="上傳者";
$lang['solution/addProduct.table.column.actions']		="動作";
$lang['solution/addProduct.table.column.upDown']	="往上/往下";

$lang['solution/addProduct.isVerified.yes']		="是";
$lang['solution/addProduct.isVerified.no']		="否";

//solution/viewSolution.php
$lang['solution/viewSolution.heading']		="解決方案確認";
$lang['solution/viewSolution.btn.addMore']	="繼續新增產品";
$lang['solution/viewSolution.btn.finish']	="完成";
$lang['solution/viewSolution.alert.saveOK']	="儲存成功";
$lang['solution/viewSolution.alert.saveFail']	="儲存失敗";
$lang['solution/viewSolution.alert.top']	="已在頂端";
$lang['solution/viewSolution.alert.bottom']	="已在底端";

//buyer/viewList
$lang['buyer/viewList.heading']	="您的解決方案列表";
$lang['buyer/viewList.btn.detail']="詳細";
$lang['buyer/viewList.search.placeholder']	="搜尋...";
$lang['buyer/viewList.btn.showCataArea']	="顯示/隱藏";
$lang['buyer/viewList.btn.list']	="解決方案列表";
$lang['buyer/viewList.noSolutions']	="目前沒有解決方案!";

//buyer/detail.php
$lang['buyer/detail.btn.list']		="解決方案列表";
$lang['buyer/detail.btn.detail']	="詳細";
$lang['buyer/detail.btn.post']		="張貼";
$lang['buyer/detail.heading']		="解決方案一覽";
$lang['buyer/detail.tab.product']	="商品列表";
$lang['buyer/detail.tab.comment']	="意見回饋";
$lang['buyer/detail.table.column.order']		="排序";
$lang['buyer/detail.table.column.img']			="圖片";
$lang['buyer/detail.table.column.name']			="名稱";
$lang['buyer/detail.table.column.brand']		="品牌/製造商";
$lang['buyer/detail.table.column.description']	="描述";
$lang['buyer/detail.table.column.actions']		="動作";
$lang['buyer/detail.noComments']				="目前沒有任何問題!";
$lang['buyer/detail.noProducts']				="目前沒有任何商品!";
$lang['buyer/detail.reply']		="回應";
$lang['buyer/detail.on']		="在";
$lang['buyer/detail.noComments']="目前沒有任何意見!";
?>
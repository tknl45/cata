<?
class Buyer_model extends CI_Model {
   
    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
    //解決方案總筆數
    function viewList_search_total_rows($query= null){ 
        $sql="SELECT * FROM solution LEFT JOIN assign_buyers  ON solution.sid = assign_buyers.sid WHERE solution.isActive=1 AND assign_buyers.userid ='".$this->session->userdata('account')['userid']."'".$this->composeSolutionQuery($query);
        //die($sql);
        $query = $this->db->query($sql);
        return $query->num_rows();
    } 
    //搜尋解決方案
    function viewList_search($query= null,$offset=0,$limit=10)
    {
  
        $sql="SELECT * FROM solution LEFT JOIN assign_buyers  ON solution.sid = assign_buyers.sid WHERE solution.isActive=1 AND assign_buyers.userid ='".$this->session->userdata('account')['userid']."'".$this->composeSolutionQuery($query)." LIMIT ".$offset.",".$limit;   
     
        //die($sql);
        $query = $this->db->query($sql);
        return $query->result();
    }



    function composeSolutionQuery($query){
        //die("composeQuery");
        $sql_where = "";
   

        if($query!=null){
            $sql_where = " AND ";
            $search_sql = "(name LIKE '%".$query["search"]."%' ".                
                 "OR introduction LIKE '%".$query["search"]."%'  ".
                 "OR description LIKE '%".$query["search"]."%' ".
                 "OR notes LIKE '%".$query["search"]."%')";

            $sql_where.= "$search_sql";            

            $icampus_category_sql = ""; 
            if(!empty($query["icampus_category"])){

                foreach($query["icampus_category"] as $row){
                    $icampus_category_sql .= "OR icampus_category LIKE '%$row%'";
                }//foreach
                $icampus_category_sql = "(".substr($icampus_category_sql,2).")";
                $sql_where.= " AND $icampus_category_sql";  
            }

            $level_sql = ""; 
            if(!empty($query["level"])){
                foreach($query["level"] as $row){
                    $level_sql .= "OR level LIKE '%$row%'"; 
                }
                $level_sql = "(".substr($level_sql,2).")";
                $sql_where.= " AND $level_sql"; 
            }            
        }
        return $sql_where;
    }

    function composeQuery($query){
        //die("composeQuery");
         $sql_where = "";
            $search_sql = "(name LIKE '%".$query["search"]."%' ".
                 "OR brand LIKE '%".$query["search"]."%' ".
                 "OR model LIKE '%".$query["search"]."%'  ".
                 "OR introduction LIKE '%".$query["search"]."%'  ".
                 "OR description LIKE '%".$query["search"]."%' ".
                 "OR techSpec LIKE '%".$query["search"]."%'  ".
                 "OR youtubeURL LIKE '%".$query["search"]."%' ".
                 "OR homepage LIKE '%".$query["search"]."%'  ".
                 "OR warranty LIKE '%".$query["search"]."%' ". 
                 "OR deployment LIKE '%".$query["search"]."%' ".
                 "OR training LIKE '%".$query["search"]."%' ".  
                 "OR batchPrices LIKE '%".$query["search"]."%' ".
                 "OR pricesDetail LIKE '%".$query["search"]."%' ". 
                 "OR batchPrices_si LIKE '%".$query["search"]."%' ".
                 "OR pricesDetail_si LIKE '%".$query["search"]."%' ".             
                 "OR contact_name LIKE '%".$query["search"]."%' ".
                 "OR company_name LIKE '%".$query["search"]."%' ". 
                 "OR company_site LIKE '%".$query["search"]."%' ".
                 "OR tel LIKE '%".$query["search"]."%' ". 
                 "OR email LIKE '%".$query["search"]."%' ". 
                 "OR memo LIKE '%".$query["search"]."%' ".
                 "OR smallImagefilenames LIKE '%".$query["search"]."%' ".
                 "OR largeImagefilenames LIKE '%".$query["search"]."%' ".
                 "OR docfilenames LIKE '%".$query["search"]."%'";

            $sql_where.= "$search_sql)";            

            $icampus_category_sql = ""; 
            if(!empty($query["icampus_category"])){

                foreach($query["icampus_category"] as $row){
                    $icampus_category_sql .= "OR icampus_category LIKE '%$row%'";
                }//foreach
                $icampus_category_sql = "(".substr($icampus_category_sql,2).")";
                $sql_where.= " AND $icampus_category_sql";  
            }

            $solution_category_sql = ""; 
            if(!empty($query["solution_category"])){
                foreach($query["solution_category"] as $row){
                    $solution_category_sql .= "OR solution_category LIKE '%$row%'";
                }//foreach
                $solution_category_sql = "(".substr($solution_category_sql,2).")";            
                $sql_where.= " AND $solution_category_sql";  
            }

            $level_sql = ""; 
            if(!empty($query["level"])){
                foreach($query["level"] as $row){
                    $level_sql .= "OR level LIKE '%$row%'"; 
                }
                $level_sql = "(".substr($level_sql,2).")";
                $sql_where.= " AND $level_sql"; 
            }

            return $sql_where;
    }
    function composeSearchSQL($query){
        $sql_userArray = 'SELECT userid FROM account WHERE creator_id = "'.$this->session->userdata('account')['userid'].'" OR creator_id IN (SELECT permission_userid FROM si_permission WHERE userid = "'.$this->session->userdata('account')['userid'].'")';

        $sql_where = "WHERE userid IN (".$sql_userArray.") AND isVerified=1 ";
        //print_r($sql_where);
        if($query!=null){       
            $sql_where .= " AND ".$this->composeQuery($query);
        }//query!=null
        return $sql_where;
    }

    function composeRootSearchSQL($query){

        $sql_where = "WHERE isVerified=1 ";
        
        if($query!=null){       
            $sql_where .= " AND ".$this->composeQuery($query);
        }//query!=null


        return $sql_where;
    }

    //搜尋商品
    function searchList($query= null,$offset=0,$limit=10)
    {
        //die(print_r($query));

        if($this->session->userdata('account')["role"] == "root"){
            $sql="SELECT * FROM product ".$this->composeRootSearchSQL($query)." LIMIT ".$offset.",".$limit;   
        }else{      //SI
            $sql="SELECT * FROM product ".$this->composeSearchSQL($query)." LIMIT ".$offset.",".$limit; 
        }
        //die($sql);
        $query = $this->db->query($sql);
        return $query->result();
    }

    //總筆數
    function searchList_total_rows($query= null){ 

        if($this->session->userdata('account')["role"] == "root"){
            $sql="SELECT * FROM product ".$this->composeRootSearchSQL($query);

        }else{      //SI
            $sql="SELECT * FROM product ".$this->composeSearchSQL($query);
        }
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    //
    function solutionList($solutionArr){
        $query = $this->db->where_in('pid', $solutionArr)->get('product');
        //die($this->db->last_query());
        return $query->result();
    }

    //新增一筆
    function add($data)
    {       
        $this->db->insert('solution', (array) $data);   
        return $this->db->insert_id();
    }






    //取得已允許的採購商
    function assign_buyers($sid){

        $query =  $this->db->select('*')->from('account')->join('assign_buyers', 'assign_buyers.userid=account.userid')->where('sid',$sid)->get();
        return $query->result();        
    }   
    //儲存assign_buyers
    function saveAssign_buyers($array){
        //die("saveAssign_buyers");
        $this->db->insert_batch('assign_buyers', $array); 
    }
    //清除assign_buyers
    function clearAssign_buyers($sid){
        $this->db->delete('assign_buyers', array('sid' => $sid)); 
    }    


    function setSolutionData($sid,$data){
        $this->db->where('sid', $sid);
        $this->db->update('solution', $data);         
    }

    //get Solution
    function getSolution($sid)
    {
        $query = $this->db->get_where('solution', array('sid' => $sid));
        if ($query->num_rows() > 0){
            return $query->result()[0];
        }else{
            return null;
        }    
        
    }//findOne

    function modify($data)
    {    
        $this->db->where('sid', $data->sid);   
        $this->db->update('solution', (array) $data);  
    }        
//----------------改到這---------------------




    //移除
    function remove($sid)
    {       
       $this->db->where('sid', $sid);
       $this->db->delete('solution'); 
     
    }

    //列出解決方案
    function viewList($offset=0,$limit=10)
    {

        $query = $this->db->get_where('solution', array('userid' => $this->session->userdata('account')['userid']), $limit, $offset);
       // die($this->db->last_query());
        return $query->result();
    }


     //總筆數
    function viewList_total_rows(){       
        return $this->db->where("userid",$this->session->userdata('account')['userid'])->count_all_results("solution");       
    }


}
?>
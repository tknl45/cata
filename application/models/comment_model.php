<?
class Comment_model extends CI_Model {
   
    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }

    //問問題
    function postComment($data){
    	$this->db->insert('comment', $data);
    }
    //回答問題
    function replyComment($cid, $data){
    	$this->db->where('cid', $cid);
		$this->db->update('comment', $data); 
    }
    //移除問題
    function removeComment($cid){
		$this->db->where('cid', $cid);
		$this->db->delete('comment');     	
    }

    //取得問題列表
    function getComments($sid){
    	//$query = $this->db->get_where('comment', array('sid' => $sid));
    	$sql = "SELECT * FROM COMMENT, account WHERE COMMENT.userid=account.userid AND sid='$sid'";
    	$query =  $this->db->query($sql);
    	return $query->result();
    }

}
?>
<?
class Theme extends CI_Model {

	var $view   = '';
	var $data   = '';
   
    function __construct()
    {
        parent::__construct();
		// $this->load->database();
        if($this->session->userdata('locale')!=null){
            $this->lang->load('locale', $this->session->userdata('locale'));
        }else{
            $lang = $this->server_lang($_SERVER['HTTP_ACCEPT_LANGUAGE']);
            $this->lang->load('locale', $lang);
        }
        
    }
	
	function load($view,$data=array())
    {		
        
        if($view !="welcome"){
            $this->check(); //判斷是否登入
        }
        
        $this->load->view('head');
        $this->load->view($view,$data);
        $this->load->view('foot');       		
    }

    function noAuthLoad($view,$data=array())
    {       
        
        $this->load->view('head');
        $this->load->view($view,$data);
        $this->load->view('foot');              
    }

    function check(){
        if($this->session->userdata('logged_in')!=true ){
            redirect(base_url());
        }
    }

    function server_lang($server){
        preg_match('/^([a-z\-]+)/i',$server, $matches);  
        $lang = strtolower($matches[1]); 
        switch($lang){
            case 'zh-tw':
                $lang='zh-TW';
                return $lang;
                break;
            case 'zh':
                $lang='zh-TW';
                return $lang;
                break;
            case 'zh-cn':
                $lang='zh-CN';
                return $lang;
                break;
            case 'en':
                $lang='english';
                return $lang;
                break;
            case 'en-us':
                $lang='english';
                return $lang;
                break;
        }
    }    
}
?>
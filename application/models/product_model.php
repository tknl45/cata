<?
class Product_model extends CI_Model {
   
    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	
	function add($data)
    {		
     	$this->db->insert('product', $data); 	
    }

    function modify($pid,$data)
    {    
        $this->db->where('pid', $pid);   
        $this->db->update('product', $data);  
    }

    //移除
    function remove($pid)
    {       
       $this->db->where('pid', $pid);
       $this->db->delete('product'); 
     
    }

    //列出新增的商品
    function viewList($offset=0,$limit=10)
    {
        $query = null;
        if($this->session->userdata('account')["role"] == "root"){
            $query = $this->db->get('product', $limit, $offset);
        }elseif ($this->session->userdata('account')["role"] == "si") {
            $this->load->model('account_model');
            $userids = $this->account_model->createdUserids($this->session->userdata('account')["userid"]);
            $query = $this->db->where_in('userid', $userids)->get('product');
            
        }else{
            $query = $this->db->get_where('product', array('userid' => $this->session->userdata('account')['userid']), $limit, $offset);
        }
        
        return $query->result();
    }



    function composeSearchSQL($query){
        $this->load->model('account_model');
        $sql_where = ""; 

        if($this->session->userdata('account')["role"] != "root" && $this->session->userdata('account')["role"] != "si"){
            $sql_where = "WHERE userid = '".$this->session->userdata('account')['userid']."' ";
        }else if($this->session->userdata('account')["role"] == "si"){
            $userids = $this->account_model->createdUserids($this->session->userdata('account')["userid"]);
            if(count($userids)==1){
                $sql_where = "WHERE userid = '".$userids[0]."' ";
                //$sql="SELECT * FROM product WHERE pid =".implode(",",$solutionArr);
            }else{
                $list = "'". implode("', '", $userids) ."'";
                $sql_where="WHERE userid IN ($list) " ;
            }
            //$sql_where = "WHERE userid = '".$this->session->userdata('account')['userid']."' ";
        }


        
        
        if($query!=null){

            $search_sql = " (name LIKE '%".$query["search"]."%' ".
                 "OR brand LIKE '%".$query["search"]."%' ".
                 "OR model LIKE '%".$query["search"]."%'  ".
                 "OR introduction LIKE '%".$query["search"]."%'  ".
                 "OR description LIKE '%".$query["search"]."%' ".
                 "OR techSpec LIKE '%".$query["search"]."%'  ".
                 "OR youtubeURL LIKE '%".$query["search"]."%' ".
                 "OR homepage LIKE '%".$query["search"]."%'  ".
                 "OR warranty LIKE '%".$query["search"]."%' ". 
                 "OR deployment LIKE '%".$query["search"]."%' ".
                 "OR training LIKE '%".$query["search"]."%' ".  
                 "OR batchPrices LIKE '%".$query["search"]."%' ".
                 "OR pricesDetail LIKE '%".$query["search"]."%' ". 
                 "OR batchPrices_si LIKE '%".$query["search"]."%' ".
                 "OR pricesDetail_si LIKE '%".$query["search"]."%' ".              
                 "OR contact_name LIKE '%".$query["search"]."%' ".
                 "OR company_name LIKE '%".$query["search"]."%' ". 
                 "OR company_site LIKE '%".$query["search"]."%' ".
                 "OR tel LIKE '%".$query["search"]."%' ". 
                 "OR email LIKE '%".$query["search"]."%' ". 
                 "OR memo LIKE '%".$query["search"]."%' ".
                 "OR smallImagefilenames LIKE '%".$query["search"]."%' ".
                 "OR largeImagefilenames LIKE '%".$query["search"]."%' ".
                 "OR docfilenames LIKE '%".$query["search"]."%')";
           
            if($sql_where != ""){
                $sql_where .= "AND $search_sql";
            }else{
                $sql_where .= " WHERE $search_sql";
            }

          

            $icampus_category_sql = ""; 
            if(!empty($query["icampus_category"])){

                foreach($query["icampus_category"] as $row){
                    $icampus_category_sql .= "OR icampus_category LIKE '%$row%'";
                }//foreach
                $icampus_category_sql = "(".substr($icampus_category_sql,2).")";
                $sql_where.= " AND $icampus_category_sql";  
            }

            $product_category_sql = ""; 
            if(!empty($query["product_category"])){
                foreach($query["product_category"] as $row){
                    $product_category_sql .= "OR product_category LIKE '%$row%'";
                }//foreach
                $product_category_sql = "(".substr($product_category_sql,2).")";            
                $sql_where.= " AND $product_category_sql";  
            }

            $level_sql = ""; 
            if(!empty($query["level"])){
                foreach($query["level"] as $row){
                    $level_sql .= "OR level LIKE '%$row%'"; 
                }
                $level_sql = "(".substr($level_sql,2).")";
                $sql_where.= " AND $level_sql"; 
            }
        }//query
        return $sql_where;
    }

    //搜尋商品
    function searchList($query,$offset=0,$limit=10)
    {
      
        $sql="SELECT * FROM product ".$this->composeSearchSQL($query)." LIMIT ".$offset.",".$limit;
        //die($sql);
        $query = $this->db->query($sql);
        return $query->result();
    }

    //總筆數
    function searchList_total_rows($query){       
        $sql="SELECT * FROM product ".$this->composeSearchSQL($query);
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
     //總筆數
    function viewList_total_rows(){       

        if($this->session->userdata('account')["role"] == "root"){
            return $this->db->count_all_results("product");
        }elseif ($this->session->userdata('account')["role"] == "si") {
            $this->load->model('account_model');
            $userids = $this->account_model->createdUserids($this->session->userdata('account')["userid"]);
            //$query = $this->db->where_in('userid', $userids)->get('product');
            return $this->db->where_in('userid', $userids)->count_all_results("product");       
        }else{
            return $this->db->where("userid",$this->session->userdata('account')['userid'])->count_all_results("product");       
        }


        //return $this->db->where("userid",$this->session->userdata('account')['userid'])->count_all_results("product");       
    }

    //只取一筆
    function getOne($pid)
    {
        $query = $this->db->get_where('product', array('pid' => $pid));
        if ($query->num_rows() > 0){
            return $query->result()[0];
        }else{
            return null;
        }    
        
    }//findOne

    function verify($pid, $isVerify){
        $this->db->where('pid', $pid);
        $this->db->update('product', array('isVerified' =>$isVerify)); 
    }

    //取出沒有驗證的筆數
    function needVerfyNum(){
        if($this->session->userdata('account')["role"] == "root"){
            $this->db->where('isVerified',0);
            $this->db->from('product');
            return $this->db->count_all_results();
        }elseif ($this->session->userdata('account')["role"] == "si") {
            $this->load->model('account_model');
            $userids = $this->account_model->createdUserids($this->session->userdata('account')["userid"]);
            //$query = $this->db->where_in('userid', $userids)->get('product');
            return $this->db->where_in('userid', $userids)->where('isVerified',0)->count_all_results("product");       
        }else{
            return $this->db->where("userid",$this->session->userdata('account')['userid'])->where('isVerified',0)->count_all_results("product");       
        }
    }

}
?>
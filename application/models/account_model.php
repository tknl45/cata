<?
class Account_model extends CI_Model {
   
    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	
	function add($data)
    {		
     	$this->db->insert('account', $data); 	
    }

    function modify($userid,$data)
    {    
        $this->db->where('userid', $userid);   
        $this->db->update('account', $data);  
    }

    function remove($userid)
    {       
       $this->db->where('userid', $userid);
       $this->db->delete('account'); 
     
    }

    //依照角色列出帳號
    function roleAccountList($role)
    {

        $query = null;
        if($role=="buyer" || $role=="vendor"){
            //如果是root 可以全部看!!
            if($this->session->userdata('account')["role"] == "root"){
                $query = $this->db->get_where('account', array('role' => $role));
            }else{//否則只能看到自己建立的
                $query = $this->db->get_where('account', array('role' => $role,'creator_id' => $this->session->userdata('account')["userid"]));
            }
        }else{ //只有root
            $query = $this->db->get_where('account', array('role' => $role));
        }

        
        return $query->result();
    }

    function findOne($userid)
    {
        $query = $this->db->get_where('account', array('userid' => $userid), 1, 0);
        if ($query->num_rows() > 0){
            $accountData = array(
                'userid'      => $query->result()[0]->userid ,
                'company_name'  => $query->result()[0]->company_name ,
                'company_site'  => $query->result()[0]->company_site ,
                'contact_name'  => $query->result()[0]->contact_name ,
                'tel'           => $query->result()[0]->tel ,
                'email'         => $query->result()[0]->email,
                'role'          => $query->result()[0]->role ,
                'profileImg'    => $query->result()[0]->profileImg ,
                'password'      => $query->result()[0]->password 
                );


            return $accountData;
        }else{
            return null;
        }
    }//findOne

    //取得si 連結的userid陣列
    function permissionSiUserIds($si_userid){
        $query = $this->db->get_where('si_permission', array('userid' => $si_userid));
        return $query->result();
    }

    //取得si userid陣列
    function siUserIds(){
        $query = $this->db->get_where('account', array('role' => 'si'));
        return $query->result();
    }    

    //儲存si persmission
    function savePersmssion($userid,$array){
        $this->db->insert_batch('si_permission', $array); 
    }


    //清除si persmission
    function clearPersmssion($userid){
        $this->db->delete('si_permission', array('userid' => $userid)); 
    }

    //取得所建立的userID Array
    public function createdUserids($userid){
        $query = $this->db->get_where('account', array('creator_id' => $userid));
        $array = array();
        foreach ($query->result() as $row)
        {
            array_push($array, $row->userid);
        }
        return $array;

    }

    //取得viewer userid陣列
    function buyerList(){
        $query = null;
        //如果是root 可以全部看!!
        if($this->session->userdata('account')["role"] == "root"){
            $query = $this->db->get_where('account', array('role' => 'buyer'));
        }else{//否則只能看到自己建立的
            $query = $this->db->get_where('account', array('role' => 'buyer','creator_id' => $this->session->userdata('account')["userid"]));
        }
        return $query->result();
    }      
}
?>
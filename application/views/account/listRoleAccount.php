<script type="text/javascript">
  $( document ).ready(function() {

      $("#saveform").validate({             
            rules:{
              password:{
                required:true
              },
              userid:{
                required:true,
                rangelength:[1,20],
                remote:"<?=base_url()?>account/isUserExist" 
              }
            },//end of rule
            messages:{
              password:{
                required:"<?=$this->lang->line('account/listRoleAccount.js.rule.msg.password.required')?>"
              },
              userid:{
                required:"<?=$this->lang->line('account/listRoleAccount.js.rule.msg.userid.required')?>",
                rangelength:"<?=$this->lang->line('account/listRoleAccount.js.rule.msg.userid.rangelength')?>",
                remote:"<?=$this->lang->line('account/listRoleAccount.js.rule.msg.userid.remote')?>"
              }
            }//end of messages
          
          });//end of validate()




      $('#saveBtn').click(function() {
          var isChecked = $("#saveform").valid();
          console.log(isChecked);
          if(isChecked){
              var options = { 
                  // beforeSubmit:  showRequest,  // pre-submit callback 
                  success:       showResponse  // post-submit callback 
              }; 
            $('#saveform').ajaxSubmit(options); 
          }
      }); //.saveBtn

      $('#addBtn').click(function() {
          $("#saveform").attr("action","<?=base_url()?>account/add");
         clearModal();
      });

      $('.modifyBtn').click(function() {
        clearModal();
        var userid = $(this).attr("userid");
        $.ajax({
           type: 'GET',
           url: "<?=base_url()?>account/getInfo/"+userid,
           success: function(data){
              
              var obj = JSON.parse(data);
              $("#inputUserid").val(obj.userid);
              $('#inputUserid').attr('disabled', true);
              $("#inputPassword").val(obj.password);
              $("#inputTEL").val(obj.tel);
              $("#inputCompanyName").val(obj.company_name);
              $("#inputCompanySite").val(obj.company_site);
              $("#inputEmail").val(obj.email);
              $("#inputContact_name").val(obj.contact_name);

              $("#saveform").attr("action","<?=base_url()?>account/modify/"+userid);
              $("#formModal").modal();

           },
           error: function(jqXHR, textStatus, errorThrown) {
                // report error
                alert("ERROR:"+textStatus);
                alert(errorThrown);
            }
        });//ajax
      }); //.modifyBtn

      $('.removeBtn').click(function() {
        var userid = $(this).attr("userid");
        if(confirm("<?=$this->lang->line('account/listRoleAccount.js.alert.removeCheck')?>\n["+userid+"]"))
        {
          $.ajax({
             type: 'GET',
             url: "<?=base_url()?>account/remove/"+userid,
             success: function(data){
                location.reload(); 
             },
             error: function(jqXHR, textStatus, errorThrown) {
                  // report error
                  alert("ERROR:"+textStatus);
                  alert(errorThrown);
              }
          });//ajax

        }//if(confirm)

        
      }); //.removeBtn
      var userid = "";
      $(".permissionBtn").click(function(){
        userid = $(this).attr("userid");
          //取得全部si 
          $.get( "<?=base_url()?>account/siUserIds?d="+Math.random(), function( data ) {
            var array = JSON.parse(data);
            var optionStr="";
            for(var i=0 ; i<array.length ; i++){
              if(array[i] == userid){continue;}
              optionStr += '<option value="'+array[i]+'">'+array[i]+'</option>';
            }
            $("#left-select").empty().html(optionStr);
            //取得已授權的si 
            $.get( "<?=base_url()?>account/permissionSiUserIds/"+userid+"?d="+Math.random(), function( data ) {
              var array = JSON.parse(data);
              var optionStr="";
              for(var i=0 ; i<array.length ; i++){
                $("option[value='"+array[i]+"']").remove();
                optionStr += '<option value="'+array[i]+'">'+array[i]+'</option>';
              }
              $("#right-select").empty().html(optionStr);
            });//$.get

          });//$.get
      });//$(".permissionBtn")
      $("#savePermissionBtn").click(function(){
          var str='';

          $("#right-select option").each(function () {
            str +=  ','+$(this).val();
          });
          str = str.substr(1); // first coma

          var dataString = {userid:userid ,list:str};
            //alert(str);
            $.ajax({
              type: "POST",
              url: "<?=base_url()?>account/savePersmssion",
              data: dataString,
              cache: false,
              success: function(data){
                console.log(data);
                if(data=="OK"){
                  $('#siPermissionModal').modal('hide');
                }else{
                  alert("something error...");
                }
              }
            });        
      })

  });//ready

// pre-submit callback 
function showRequest(formData, jqForm, options) { 
    var queryString = $.param(formData); 
    alert('About to submit: \n\n' + queryString); 
    return true; 
} 
 
// post-submit callback 
function showResponse(responseText, statusText, xhr, $form)  { 
    location.reload(); 
}

//清除modal訊息
function clearModal(){
  $("#inputUserid").val("");
  $('#inputUserid').attr('disabled', false);
  $("#inputPassword").val("");
  $("#inputTEL").val("");
  $("#inputCompanyName").val("");
  $("#inputCompanySite").val("");
  $("#inputEmail").val("");
  $("#inputContact_name").val("");

}

function go_right(){
  $("#left-select option:selected").each(function () {
        $('#right-select').append('<option value="'+ $(this).val() +'">' + $(this).text() + '</option>');
        $(this).remove();
      });
}

function go_left(){
  $("#right-select option:selected").each(function () {
      $('#left-select').append('<option value="'+ $(this).val() +'">' + $(this).text() + '</option>');
      $(this).remove();
    });
}
</script>

<style type="text/css">
  select {
  width: 220px;
  background-color: #ffffff;
  border: 1px solid #cccccc;
}
</style>


<a id="addBtn" class="btn btn-success pull-right" href="#" role="button" data-toggle="modal" data-target="#formModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span><?=$this->lang->line('account/listRoleAccount.btn.add')?></a>
<?if(empty($accounts)){?>
<div class="alert alert-danger" role="alert"><?=$this->lang->line('account/listRoleAccount.notice.noUsers')?></div>
<?}else{?>
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><?=$this->lang->line('account/listRoleAccount.accountAdmin')?> - <?=($this->lang->line('account/listRoleAccount.role.'.$role))?></div>

  <!-- Table -->
  <table class="table">
    <tr>
    	<th><?=$this->lang->line('account/listRoleAccount.table.column.userid')?></th>
      <th><?=$this->lang->line('account/listRoleAccount.table.column.name')?></th>
    	<th><?=$this->lang->line('account/listRoleAccount.table.column.password')?></th>
    	<th><?=$this->lang->line('account/listRoleAccount.table.column.company')?></th>
    	<th><?=$this->lang->line('account/listRoleAccount.table.column.tel')?></th>
    	<th><?=$this->lang->line('account/listRoleAccount.table.column.email')?></th>
    	<th><?=$this->lang->line('account/listRoleAccount.table.column.actions')?></th>
    </tr>
<?foreach ( $accounts as $account){?>    
    <tr class="user_<?=$account->userid?>">
    	<td><?=$account->userid?></td>
      <td><?=$account->contact_name?></td>
    	<td><?=$account->password?></td>
    	<td><a href="<?=$account->company_site?>"><?=$account->company_name?></a></td>
    	<td><?=$account->tel?></td>
    	<td><?=$account->email?></td>
    	<td>
        <a class="btn btn-warning modifyBtn" href="#" role="button" userid="<?=$account->userid?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><?=$this->lang->line('account/listRoleAccount.btn.modify')?></a>
        <a class="btn btn-danger removeBtn" href="#" role="button" userid="<?=$account->userid?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><?=$this->lang->line('account/listRoleAccount.btn.remove')?></a>
<?if($role=="si" || $role=="root"){?>        
        <a class="btn btn-success permissionBtn" href="#" role="button" userid="<?=$account->userid?>"  data-toggle="modal" data-target="#siPermissionModal"><span class="glyphicon glyphicon glyphicon-list-alt" aria-hidden="true"></span><?=$this->lang->line('account/listRoleAccount.btn.permission')?></a>
<?}?>        
      </td>
    </tr>
<?}?>      
  </table>
</div>
<?}//if empty?>

<!-- /.modal -->
<div  class="modal fade" id="formModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=$this->lang->line('account/listRoleAccount.formModal.title')?></h4>
      </div>
      <div class="modal-body">
          <form id="saveform" class="form-horizontal" action="<?=base_url()?>account/add" method="post">

            <div class="form-group">
              <label for="inputUserid" class="col-sm-2 control-label text-danger"><?=$this->lang->line('account/listRoleAccount.table.column.userid')?>*</label>
              <div class="col-sm-10">
                <input type="text" name="userid" class="form-control" id="inputUserid" placeholder="User id" required>
              </div>
            </div>

            <div class="form-group">
              <label for="inputContact_name" class="col-sm-2 control-label"><?=$this->lang->line('account/listRoleAccount.table.column.name')?></label>
              <div class="col-sm-10">
                <input type="text" name="contact_name" class="form-control" id="inputContact_name" placeholder="Contact name">
              </div>
            </div>

            <div class="form-group">
              <label for="inputPassword" class="col-sm-2 control-label text-danger"><?=$this->lang->line('account/listRoleAccount.table.column.password')?>*</label>
              <div class="col-sm-10">
                <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password" required>
              </div>
            </div>

            <div class="form-group">
              <label for="inputTEL" class="col-sm-2 control-label"><?=$this->lang->line('account/listRoleAccount.table.column.tel')?></label>
              <div class="col-sm-10">
                <input type="text" name="tel" class="form-control" id="inputTEL" placeholder="+886266316755" size="20">
              </div>
            </div>

            <div class="form-group">
              <label for="inputCompanyName" class="col-sm-2 control-label"><?=$this->lang->line('account/listRoleAccount.table.column.company')?></label>
              <div class="col-sm-10">
                <input type="text" name="company_name" class="form-control" id="inputCompanyName" placeholder="company name" size="120">
              </div>
            </div>

            <div class="form-group">
              <label for="inputCompanySite" class="col-sm-2 control-label"><?=$this->lang->line('account/listRoleAccount.table.column.site')?></label>
              <div class="col-sm-10">
                <input type="text" name="company_site" class="form-control" id="inputCompanySite" placeholder="http://..." size="120">
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail" class="col-sm-2 control-label"><?=$this->lang->line('account/listRoleAccount.table.column.email')?></label>
              <div class="col-sm-10">
                <input type="email" name="email" class="form-control" id="inputEmail" placeholder="email@@@@.xxx">
              </div>
            </div>
              <input type="hidden" name="role" id="inputRole" value="<?=$role?>">
          </form>        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('account/listRoleAccount.btn.close')?></button>
        <button type="button" id="saveBtn" class="btn btn-primary"><?=$this->lang->line('account/listRoleAccount.btn.save')?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="siPermissionModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=$this->lang->line('account/listRoleAccount.siPermissionModal.title')?></h4>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-5">
                <h4><?=$this->lang->line('account/listRoleAccount.siPermissionModal.si_list')?></h4>
                <select size="20" multiple id="left-select">
                    <option value="si_1">si_1</option>
                    <option value="saab">Saab</option>
                    <option value="mercedes">Mercedes</option>
                    <option value="audi">Audi</option>                  
                </select>
            </div>
            <div class="col-md-2">
              <br/><br/><br/><br/>
              <button type="button" class="btn btn-success" onClick="go_right();">>></button>
              <button type="button" class="btn btn-success" onClick="go_left();"> <<</button>
            </div>
            <div class="col-md-5">
                <h4><?=$this->lang->line('account/listRoleAccount.siPermissionModal.view_list')?></h4>
                <select size="20" name="viewList" multiple id="right-select"></select>
            </div>
          </div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('account/listRoleAccount.btn.close')?></button>
        <button type="button" id="savePermissionBtn" class="btn btn-primary"><?=$this->lang->line('account/listRoleAccount.btn.save')?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--======Jcrop======-->
<script type="text/javascript" src="<?=base_url()?>js/jcrop/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>js/jcrop/jquery.Jcrop.min.css">
<script type="text/javascript">
$( document ).ready(function() {
  $("#passwordForm").validate({
    errorElement: "div",
    rules: {
      password: {
        required: true,
        maxlength: 50
      },
      check_password: {
        required: true,
        maxlength: 50,
        equalTo: "#new_password"
      },
      new_password: {
        required: true,
        maxlength: 50
      }
    },
    messages: {
      password: {
        required: "Please insert Original password",
        maxlength: "password length is smaller than 50 chars"
      },
      check_password: {
        required: "Please New password again",
        maxlength: "password length is smaller than 50 chars",
        equalTo: "Not Correct"
      },
      new_password: {
        required: "Please insert new password",
        maxlength: "password length is smaller than 50 chars"
      }
    }
  });

      $('#saveInfoBtn').click(function() {
          var options = { 
              // beforeSubmit:  showRequest,  // pre-submit callback 
              success:       showResponse  // post-submit callback 
          }; 
          $('#saveForm').ajaxSubmit(options);

          return false; 
      }); //.saveBtn

      $('#savePwBtn').click(function() {
          var isCheck = $("#passwordForm").valid();
          if(isCheck == false) {return false;}

          var options = { 
              // beforeSubmit:  showRequest,  // pre-submit callback 
              success:       showPwResponse  // post-submit callback 
          }; 
          $('#passwordForm').ajaxSubmit(options);

          return false; 
      }); //.saveBtn

/**--------- img uploader related --------------------------------*/
        var imgUpload=new plupload.Uploader({
            runtimes:'gears,html5,flash,browserplus',
            browse_button:'imgUpload',
            //container:'uploader',
            max_file_size:'1mb',
            url:'<?=base_url()?>uploadFile/uploadProfileImage',
            flash_swf_url:'<?=base_url()?>js/plupload/plupload.flash.swf',
            filters : [
                { title : "Image files", extensions : "jpg,gif,png" }
            ]
        });

        imgUpload.init();
        //上傳完成
        imgUpload.bind('FilesAdded',function(up,files){
            //only one file can be select
            while(up.files.length>1){
                up.removeFile(up.files[0]);
            }
           
            imgUpload.start();
            up.refresh();
            $('#imgModal').modal('show');
            $('#upload_progress').show();
        });

        //上傳前
        imgUpload.bind('BeforeUpload', function(up,file){           
            up.settings.multipart_params={fileName:file.name,fileId:file.id};
        });
        //上傳過程
        imgUpload.bind('UploadProgress',function(up,file){
            $('#upload_progress').html(file.percent+"%");
            $('#upload_progress').attr('aria-valuenow',file.percent);
            $('#upload_progress').attr('style','width:'+file.percent+'%');
        });
        //上傳完成
        imgUpload.bind('UploadComplete',function(up,file,response){
            $('#upload_progress').html("0%");
            $('#upload_progress').hide();          
        });
        imgUpload.bind('FileUploaded',function(up,file,response){
          console.log(response);   
          
          $("#crop-target").attr('src','<?=base_url()?>upload/profile/'+response.response);
          $('#src').attr('value',response.response);  //record image name
          $('#crop-target').Jcrop({
            bgColor:'black',
            bgOpacity:.4,
            aspectRatio: 460/260,
            minSize:[460,260],
            setSelect:[0,0,460,260],
            boxHeight:260,
            onChange:recordCoords
          });

        });
/**---------end img uploader related --------------------------------*/
        //圖片切割
        $("#imgCropBtn").click(function(){
          var options = { 
              // beforeSubmit:  showRequest,  // pre-submit callback 
              success:       showCropResponse  // post-submit callback 
          }; 
          $('#cropForm').ajaxSubmit(options);

          return false; 
        });
});//end of ready

  function recordCoords(c)
  {

    $('input#crop_x').attr('value',c.x);
    $('input#crop_y').attr('value',c.y);
    $('input#crop_width').attr('value',c.w);
    $('input#crop_height').attr('value',c.h);
  };


// post-submit callback 
function showResponse(responseText, statusText, xhr, $form)  { 
   alert("<?=$this->lang->line('account/editForm.alert.saveSuccess')?>");
   location.reload(); 
}       

// post-submit callback 
function showPwResponse(responseText, statusText, xhr, $form)  { 
   if(responseText=="OK"){
      alert("<?=$this->lang->line('account/editForm.alert.saveSuccess')?>");
      location.reload(); 
   }else{
      alert(responseText);
   }
} 

function showCropResponse(responseText, statusText, xhr, $form)  { 
   if(responseText=="OK"){
      alert("<?=$this->lang->line('account/editForm.alert.cropSuccess')?>");


      $.get( "<?=base_url()?>account/savePersonalProfile/"+$('#src').val(), function() {
            alert("<?=$this->lang->line('account/editForm.alert.saveSuccess')?>");
            location.reload();
        })
        .fail(function() {
          
          alert("<?=$this->lang->line('account/editForm.alert.fail')?>");

        });
      // 
   }else{
      alert(responseText);
   }
} 
</script> 

<div class="container">
  <div class="row">
    <div class="col-md-2">
            <ul class="nav nav-pills nav-stacked">
              <li><a data-toggle="modal" href="#myModal"><?=$this->lang->line('account/editForm.nav.personalInfo')?></a></li>
              <li><a data-toggle="modal" href="#myModalpassword" id="pwBtn"><?=$this->lang->line('account/editForm.nav.password')?></a></li>
              <li><a data-toggle="modal" href="#myModalintro" id="imgUpload"><?=$this->lang->line('account/editForm.nav.picture')?></a></li>
            <ul>
    </div>

    <div class="col-md-10">
      <div class="row">
          <div class="col-md-4">
            <img class="thumbnail" src="<?=base_url()?>upload/profile/<?=$profileImg?>" width="300">
          </div>
          <div class="col-md-6">
            <dl class="dl-horizontal">
              <dt><?=$this->lang->line('account/editForm.userid')?></dt>
              <dd><?=$userid?></dd>
              <dt><?=$this->lang->line('account/editForm.name')?></dt>
              <dd><?=$contact_name?></dd>
              <dt><?=$this->lang->line('account/editForm.tel')?></dt>
              <dd><?=$tel?></dd>
              <dt><?=$this->lang->line('account/editForm.company')?></dt>
              <dd><?=$company_name?></dd>
              <dt><?=$this->lang->line('account/editForm.site')?></dt>
              <dd><?=$company_site?></dd>
              <dt><?=$this->lang->line('account/editForm.email')?></dt>
              <dd><?=$email?></dd>                                        
            </dl> 
          </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=$this->lang->line('account/editForm.nav.personalInfo')?></h4>
      </div>
      <div class="modal-body">
<form id="saveForm" class="form-horizontal" action="<?=base_url()?>account/modifyPersonalInfo" method="post">
          

            <div class="form-group">
              <label for="inputUserid" class="col-sm-2 control-label text-danger"><?=$this->lang->line('account/editForm.userid')?>*</label>
              <div class="col-sm-10">
                <input type="text" name="userid" class="form-control" id="inputUserid" placeholder="User id" required value="<?=$userid?>" disabled>
              </div>
            </div>

            <div class="form-group">
              <label for="inputContact_name" class="col-sm-2 control-label"><?=$this->lang->line('account/editForm.name')?></label>
              <div class="col-sm-10">
                <input type="text" name="contact_name" class="form-control" id="inputContact_name" placeholder="Contact name" value="<?=$contact_name?>">
              </div>
            </div>
            <div class="form-group">
              <label for="inputTEL" class="col-sm-2 control-label"><?=$this->lang->line('account/editForm.tel')?></label>
              <div class="col-sm-10">
                <input type="text" name="tel" class="form-control" id="inputTEL" placeholder="+886266316755" size="20" value="<?=$tel?>">
              </div>
            </div>

            <div class="form-group">
              <label for="inputCompanyName" class="col-sm-2 control-label"><?=$this->lang->line('account/editForm.company')?></label>
              <div class="col-sm-10">
                <input type="text" name="company_name" class="form-control" id="inputCompanyName" placeholder="company name" size="120" value="<?=$company_name?>">
              </div>
            </div>

            <div class="form-group">
              <label for="inputCompanySite" class="col-sm-2 control-label"><?=$this->lang->line('account/editForm.site')?></label>
              <div class="col-sm-10">
                <input type="text" name="company_site" class="form-control" id="inputCompanySite" placeholder="http://..." size="120" value="<?=$company_site?>">
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail" class="col-sm-2 control-label"><?=$this->lang->line('account/editForm.email')?></label>
              <div class="col-sm-10">
                <input type="email" name="email" class="form-control" id="inputEmail" placeholder="your email" value="<?=$email?>">
              </div>
            </div>
</form>      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('account/editForm.btn.close')?></button>
        <button type="button" class="btn btn-primary" id="saveInfoBtn"><?=$this->lang->line('account/editForm.btn.save')?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<form id="passwordForm" class="form-horizontal" action="<?=base_url()?>account/modifyPersonalPw" method="post">
<div class="modal fade" id="myModalpassword">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=$this->lang->line('account/editForm.nav.password')?></h4>
      </div>
      <div class="modal-body">

            <div class="form-group">
              <label class="col-sm-2 control-label"><?=$this->lang->line('account/editForm.password.old')?></label>
              <div class="col-sm-10">
                <input type="password" name="password" maxlength="50" id="InputPassword"  class="form-control"/>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label"><?=$this->lang->line('account/editForm.password.new')?></label>
              <div class="col-sm-10">
                <input type="password" name="new_password"  id="InputNew_password"  maxlength="50" class="form-control"/>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"><?=$this->lang->line('account/editForm.password.repeat')?></label>
              <div class="col-sm-10">
                <input type="password" name="check_password" maxlength="50"  id="InputCheck_password" class="form-control"/>
              </div>
            </div>
     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('account/editForm.btn.close')?></button>
        <button type="button" class="btn btn-primary" id="savePwBtn"><?=$this->lang->line('account/editForm.btn.save')?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form> 


<div class="modal fade" id="imgModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=$this->lang->line('account/editForm.picture.process')?></h4>
      </div>
      <div class="modal-body">
        <div id="upload_progress" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
            60%
        </div>
        <div id="imgShowArea">
          <img id="crop-target" style="max-width:inherit" src=""/>
        </div>
    <form id="cropForm" method="post" action="<?=base_url()?>uploadFile/cropImage/profile">
      <input type="hidden" name="src" id="src"/>
      <input type="hidden" name="crop_x" id="crop_x" value=""/>
      <input type="hidden" name="crop_y" id="crop_y" value=""/>
      <input type="hidden" name="crop_width" id="crop_width" value=""/>
      <input type="hidden" name="crop_height" id="crop_height" value=""/>
    </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('account/editForm.btn.close')?></button>
        <button type="button" class="btn btn-primary" id="imgCropBtn"><?=$this->lang->line('account/editForm.btn.save')?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


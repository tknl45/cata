

<div class="row">
  <div class="col-lg-4">

  <h2><?=$this->lang->line('login_view.accountAdmin')?></h2>
    <p><?=$this->lang->line('login_view.accountAdmin.description')?></p>          

      <p><a class="btn btn-primary" role="button" href="<?=base_url()?>account/listRoleAccount?role=vendor"><?=$this->lang->line('login_view.accountAdmin.vender')?> »</a></p>
      <p><a class="btn btn-primary" role="button" href="<?=base_url()?>account/listRoleAccount?role=buyer"><?=$this->lang->line('login_view.accountAdmin.buyer')?> »</a></p>
<?if($this->session->userdata('account')["role"] == "root"){//只有root可以作si以上的管理?>
      <p><a class="btn btn-primary" role="button" href="<?=base_url()?>account/listRoleAccount?role=si"><?=$this->lang->line('login_view.accountAdmin.si')?> »</a></p>
      <p><a class="btn btn-primary" role="button" href="<?=base_url()?>account/listRoleAccount?role=root"><?=$this->lang->line('login_view.accountAdmin.root')?> »</a></p>
<?}?>


  </div>
  <div class="col-lg-4">

    <h2><?=$this->lang->line('login_view.products')?></h2>
    <p><?=$this->lang->line('login_view.products.description')?></p>
 
    <a class="btn btn-primary" role="button" href="<?=base_url()?>product/review"><?=$this->lang->line('login_view.products.review')?> <span class="badge"><?=$this->session->userdata('needVerfyNum')?></a>


  </div>
  <div class="col-lg-4">

    <h2><?=$this->lang->line('login_view.total_solution')?></h2>
    <p><?=$this->lang->line('login_view.total_solution.description')?></p>
    <ul class="list-inline">
      <a class="btn btn-primary" role="button" href="<?=base_url()?>solution/viewList"><?=$this->lang->line('login_view.total_solution.solutions')?> »</a>
    </ul>

  </div>
</div>



      <div class="jumbotron">
        <h1><?=$this->lang->line('welcome.title')?></h1>
        <p class="lead"><?=$this->lang->line('welcome.description')?></p>
          <form action="<?=base_url()?>account/login" method="POST" class="form-inline">
            <div class="form-group">
              <label for="exampleInputEmail1"><?=$this->lang->line('welcome.input.userID.label')?></label>
              <input type="text" placeholder="<?=$this->lang->line('welcome.input.userID.placeholder')?>" name="userid" class="form-control"  size="12">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1"><?=$this->lang->line('welcome.input.password.label')?></label>
              <input type="password" placeholder="<?=$this->lang->line('welcome.input.password.placeholder')?>" name="password" class="form-control" size="12">
            </div>
            <button type="submit" class="btn btn-success"><?=$this->lang->line('welcome.btn.signIn')?></button>
          </form>        
      </div>

      <div class="row">
        <div class="col-lg-4">
          <h2><?=$this->lang->line('welcome.role.si')?></h2>
          <p><?=$this->lang->line('welcome.role.si.description')?></p>          
			<ul>
			  <li><?=$this->lang->line('welcome.role.si.function.accountManage')?></li>
			  <li><?=$this->lang->line('welcome.role.si.function.solutionManage')?></li>
			  <li><?=$this->lang->line('welcome.role.si.function.productReview')?></li>
			</ul>
          
        </div>
        <div class="col-lg-4">
        
          <h2><?=$this->lang->line('welcome.role.vendor')?></h2>
          <p><?=$this->lang->line('welcome.role.vendor.description')?></p>
			<ul>
			  <li><?=$this->lang->line('welcome.role.vendor.function.productUpload')?></li>
			</ul>
          
       </div>
        <div class="col-lg-4">
        
          <h2><?=$this->lang->line('welcome.role.buyer')?></h2>
          <p><?=$this->lang->line('welcome.role.buyer.description')?></p>
			<ul>
			  <li><?=$this->lang->line('welcome.role.buyer.function.solutionReview')?></li>
			</ul>
          
        </div>
      </div>

<script type="text/javascript">
$( document ).ready(function() {
	var product_json= <?=$product_json?>;
	if(product_json!=null){
		$("#pid").val(product_json.pid);
		$("#inputName").text(product_json.name);
      	$("#inputBrand").text(product_json.brand);
      	$("#inputModel").text(product_json.model);
      	$("#inputIntroduction").text(product_json.introduction);
      	$("#inputDescription").text(product_json.description);
      	$("#inputTechSpec").text(product_json.techSpec);
      	$("#inputYoutubeURL").val(product_json.youtubeURL);
      	$("#inputHomepage").text(product_json.homepage);
		$("#inputWarranty").text(product_json.warranty);
      	$("#inputDeployment").text(product_json.deployment);
		$("#inputTraining").text(product_json.training);

      	$("#inputContact_name").text(product_json.contact_name);
      	$("#inputCompany_site").text(product_json.company_site);
      	$("#inputCompany_name").text(product_json.company_name);
		$("#inputTel").text(product_json.tel);
		$("#inputEmail").text(product_json.email);      
		$("#inputMemo").text(product_json.memo);
		$("#docfilenames").val(product_json.docfilenames);
		$("#docfiles").val(product_json.docfiles);
		$("#largeImagefilenames").val(product_json.largeImagefilenames);
		$("#largeImagefiles").val(product_json.largeImagefiles);
		$("#smallImagefilenames").val(product_json.smallImagefilenames);
		$("#smallImagefiles").val(product_json.smallImagefiles);

		$("#inputIcampus_category").text(product_json.icampus_category);
		$("#inputProduct_category").text(product_json.product_category);
		$("#inputLevel").text(product_json.level);
<?if($this->session->userdata('isShowPrice') != 0){?>   		
<?if($this->session->userdata('account')["role"] != "buyer"){?>      	
		$("#inputUnitPrice").val(product_json.unitPrice);
      	$("#inputBatchPrices").text(product_json.batchPrices);
		$("#inputPricesDetail").text(product_json.pricesDetail);
<?}//if?>
<?if($this->session->userdata('account')["role"] != "vendor"){?>  
		$("#inputUnitPrice_si").val(product_json.unitPrice_si);
      	$("#inputBatchPrices_si").text(product_json.batchPrices_si);
		$("#inputPricesDetail_si").text(product_json.pricesDetail_si);
<?}//if?>			
<?}//if isshowPrice?>			
	}


	initYoutubeForm();
    
	  $('#goto_productDetail_btn').click(function(){
	  		$('#newProduct').collapse('hide');
	  		$('#productDetail').collapse('show');
	  		return false;
	  });

	  $('#goto_warranty_btn').click(function(){
	  		$('#productDetail').collapse('hide');
	  		$('#warranty').collapse('show');
	  		return false;
	  });

	  $('#goto_pricing_btn').click(function(){	  	
	  		$('#warranty').collapse('hide');
	  		$('#pricing').collapse('show');
	  		return false;
	  });

	  $('#goto_contact_btn').click(function(){
	  		$('#pricing').collapse('hide');
	  		$('#contact').collapse('show');
	  		return false;
	  });
     
    

//----------判斷圖片是否存在-------------
		if($("#largeImagefilenames").val()!=""){
		   var fileArr = $("#largeImagefiles").val().split("|");
		   var fileNameArr = $("#largeImagefilenames").val().split("|");
		   for(var i=0 ; i<fileArr.length-1 ; i++){

		   		var appendHtml='<div class="col-md-3">'+
		    		'<button type="button" class="btn btn-default btn-xs btn-default">'+trim2Short(fileNameArr[i])+'</button>'+
		    		//'<button type="button" class="largeImageRemoveBtn btn btn-danger btn-xs" file="'+fileArr[i]+'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>remove</button>'+
		    		'<img src="<?=base_url()?>upload/product/'+fileArr[i]+'" class="img-thumbnail" width="200">'+
		    		'</div>'; 
		    	$("#largeImageArea").append(appendHtml);
		   }
		}
		if($("#smallImagefilenames").val()!=""){
		   var fileArr = $("#smallImagefiles").val().split("|");
		   var fileNameArr = $("#smallImagefilenames").val().split("|");
		   for(var i=0 ; i<fileArr.length-1 ; i++){
		   		var appendHtml='<div class="col-md-3">'+
		   			'<button type="button" class="btn btn-default btn-xs btn-default">'+trim2Short(fileNameArr[i])+'</button>'+
		    		//'<button type="button" class="smallImageRemoveBtn btn btn-danger btn-xs" file="'+fileArr[i]+'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>remove</button>'+
		    		'<img src="<?=base_url()?>upload/product/'+fileArr[i]+'" class="img-thumbnail" width="200">'+
		    		'</div>'; 
		    	$("#smallImageArea").append(appendHtml);
		   }
		}
		if($("#docfilenames").val()!=""){
		   	var fileArr = $("#docfiles").val().split("|");
		   	var fileNameArr = $("#docfilenames").val().split("|");
		   	for(var i=0 ; i<fileArr.length-1 ; i++){
	            var appendHtml='<div class="col-md-3">'+
	            			'<button type="button" class="btn btn-default btn-xs btn-default">'+trim2Short(fileNameArr[i])+'</button>'+
				    	//	'<button type="button" class="docRemoveBtn btn btn-danger btn-xs" file="'+fileArr[i]+'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>remove</button>'+
				    		'<a href="<?=base_url()?>upload/product/'+fileArr[i]+'"><img src="<?=base_url()?>images/'+getExtension(fileArr[i])+'-icon.png" class="img-thumbnail" width="200"></a>'+
				    		'</div>';
	            $("#docArea").append(appendHtml);
	        }//for
		}
}); 		
//------------------------------------
function getExtension(filename){
	var fArray = filename.split(".");
	return fArray[fArray.length-1];
}

function trim2Short(filename){
	var shortLength = 15;
	if(filename.length>shortLength){
		filename = filename.substr(0, shortLength)+"...";
	}
	return filename;
}	 

/**-----------------youTube----------------------*/
	function initYoutubeForm(){
		var youtubeURL = $("#inputYoutubeURL").val();		
			if(youtubeURL.indexOf("http://www.youtube.com/watch")!=-1){
				var videoId=youtubeURL.replace("http://www.youtube.com/watch?v=","");
				$('#preview-container').empty().html('<iframe id="previewer" width="100%" height="100%" src="http://www.youtube.com/embed/'+videoId+'?wmode=transparent" frameborder="0" allowfullscreen></iframe>');
			}
			else if(youtubeURL.indexOf("https://www.youtube.com/watch")!=-1){ //141008 45 added
				var videoId=youtubeURL.replace("https://www.youtube.com/watch?v=","");
				$('#preview-container').empty().html('<iframe id="previewer" width="100%" height="100%" src="http://www.youtube.com/embed/'+videoId+'?wmode=transparent" frameborder="0" allowfullscreen></iframe>');
			}
			else{
				$('input[name="content"]').val("");
				$(this).next('#preview-container').empty();
				$(this).next('#preview-container').html("<h2>Video Prievew</h2>"); 
			}
	}
</script>
<?if( 
	$this->session->userdata('account')["role"] != "buyer"
){?>
<a id="listBtn" class="btn btn-default" href="<?=base_url()?>product/viewList" role="button"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><?=$this->lang->line('product/view.btn.view_list')?></a>
<?}?>
<?if( 
	$this->session->userdata('account')["role"] == "si"  ||
    $this->session->userdata('account')["role"] == "root"
){?>
<a class="btn btn-default modifyBtn" href="<?=base_url()?>product/modifyForm/<?=$pid?>" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><?=$this->lang->line('product/view.btn.modify')?></a>
<?}?>
<input id="pid" name="pid" type="hidden">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="step1">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#newProduct" aria-expanded="true" aria-controls="newProduct">
          <?=$this->lang->line('Product.info')?>
        </a>
      </h4>
    </div>
	<div id="newProduct" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="step1">
		<div class="panel-body">
			  <div class="form-group">
			    <label for="inputName" class="col-sm-2 control-label"><?=$this->lang->line('Product.info.name')?></label>
			    <div class="col-sm-10">
			      <pre id="inputName"></pre>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputBrand" class="col-sm-2 control-label"><?=$this->lang->line('Product.info.name')?></label>
			    <div class="col-sm-10">
			      <pre id="inputBrand"></pre>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputModel" class="col-sm-2 control-label"><?=$this->lang->line('Product.info.model')?></label>
			    <div class="col-sm-10">
			      <pre id="inputModel"></pre>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputBrand" class="col-sm-2 control-label"><?=$this->lang->line('Product.info.icampus_category')?></label>
			    <div class="col-sm-10">
			    	<pre id="inputIcampus_category"></pre>
					    
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputBrand" class="col-sm-2 control-label"><?=$this->lang->line('Product.info.product_category')?></label>
			    <div class="col-sm-10">
			    	<pre id="inputProduct_category"></pre>
					
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputBrand" class="col-sm-2 control-label"><?=$this->lang->line('Product.info.eduLevel')?></label>
			    <div class="col-sm-10">
			    	<pre id="inputLevel"></pre>
					
			    </div>
			  </div>
			  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button id="goto_productDetail_btn" class="btn btn-default pull-right"><?=$this->lang->line('product/view.btn.next')?></button>
				    </div>
			  </div>			  			  			  			  
      </div>
    </div><!--<div id="newProduct"-->
  </div>
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="step2">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#productDetail" aria-expanded="false" aria-controls="productDetail">
          <?=$this->lang->line('Product.detail')?>
        </a>
      </h4>
    </div>
    <div id="productDetail" class="panel-collapse collapse" role="tabpanel" aria-labelledby="step2">
      <div class="panel-body">      
			  <div class="form-group">
			    <label for="inputIntroduction" class="col-sm-2 control-label"><?=$this->lang->line('Product.detail.introduction')?></label>
			    <div class="col-sm-10">
			      <pre id="inputIntroduction"></pre>
			    </div>
			  </div> 
			  <div class="form-group">
			    <label for="inputDescription" class="col-sm-2 control-label"><?=$this->lang->line('Product.detail.description')?></label>
			    <div class="col-sm-10">
			      <pre id="inputDescription"></pre>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputTechSpec" class="col-sm-2 control-label"><?=$this->lang->line('Product.detail.techSpec')?></label>
			    <div class="col-sm-10">
			      <pre id="inputTechSpec"></pre>
			    </div>
			  </div>			  
			  <div class="form-group">
			    <label  class="col-sm-2 control-label">
			    	<?=$this->lang->line('Product.detail.largeImage')?>
			    			    	
			    	<input id="largeImagefiles" name="largeImagefiles" type="hidden" required>
			    	<input id="largeImagefilenames" name="largeImagefilenames" type="hidden">
			    </label>
			    <div class="col-sm-10" id="largeImageArea">
			    	<!--	
			    		<div class="col-md-3">	
			    				<button type="button" class="btn btn-danger btn-xs largeImageRemoveBtn" file="xxx.jpg"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>remove</button>
			    				<img src="<?=base_url()?>images/800x300.jpg" class="img-thumbnail" width="200">
			    		</div>
			    	-->			    					    					      
			    </div>
			  </div>
			  <div class="form-group">
			    <label  class="col-sm-2 control-label">
			    	<?=$this->lang->line('Product.detail.smallImage')?>			
			    	<input id="smallImagefiles" name="smallImagefiles" type="hidden" required>
			    	<input id="smallImagefilenames" name="smallImagefilenames" type="hidden">
			    </label>
			    <div class="col-sm-10" id="smallImageArea">
			      
			    </div>
			  </div>
			  <div class="form-group">
			    <label  class="col-sm-2 control-label">
			    	<?=$this->lang->line('Product.detail.doc')?>
			    	<input id="docfiles" name="docfiles" type="hidden" required>
			    	<input id="docfilenames" name="docfilenames"type="hidden">
			    </label>
			    <div class="col-sm-10" id="docArea">
			      
			    </div>
			  </div>			  
			  <div class="form-group">
			    <label for="inputYoutubeURL" class="col-sm-2 control-label"><?=$this->lang->line('Product.detail.youtubeURL')?></label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="youtubeURL" id="inputYoutubeURL" placeholder="youtube url" disabled>
			      <div id="preview-container" class="col-sm-2"><h2>Video Prievew</h2></div>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputHomepage" class="col-sm-2 control-label"><?=$this->lang->line('Product.detail.homepage')?></label>
			    <div class="col-sm-10">
			      <pre id="inputHomepage"></pre>
			    </div>
			  </div> 
			  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button id="goto_warranty_btn" class="btn btn-default pull-right"><?=$this->lang->line('product/view.btn.next')?></button>
				    </div>
			  </div>			  			  			          
      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="step3">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#warranty" aria-expanded="false" aria-controls="warranty">
          <?=$this->lang->line('Product.warranty')?>
        </a>
      </h4>
    </div>
    <div id="warranty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="step3">
      <div class="panel-body">
			  <div class="form-group">
			    <label for="inputWarranty" class="col-sm-2 control-label"><?=$this->lang->line('Product.warranty.warranty')?></label>
			    <div class="col-sm-10">
			      <pre id="inputWarranty"></pre>
			    </div>
			  </div> 
			  <div class="form-group">
			    <label for="inputDeployment" class="col-sm-2 control-label"><?=$this->lang->line('Product.warranty.deployment')?></label>
			    <div class="col-sm-10">
			      <pre id="inputDeployment"></pre>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputTraining" class="col-sm-2 control-label"><?=$this->lang->line('Product.warranty.training')?></label>
			    <div class="col-sm-10">
			      <pre id="inputTraining"></pre>
			    </div>
			  </div>
			  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button id="goto_pricing_btn" class="btn btn-default pull-right"><?=$this->lang->line('product/view.btn.next')?></button>
				    </div>
			  </div>			  
      </div>
    </div>
  </div>
<?if($this->session->userdata('isShowPrice') != 0){?>  
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="step4">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#pricing" aria-expanded="false" aria-controls="pricing">
			<?=$this->lang->line('Product.pricing')?>
        </a>
      </h4>
    </div>
    <div id="pricing" class="panel-collapse collapse" role="tabpanel" aria-labelledby="step4">
      <div class="panel-body">
<?if($this->session->userdata('account')["role"] != "buyer"){?>      
			  <div class="form-group">
			    <label for="inputName" class="col-sm-2 control-label"><?=$this->lang->line('Product.pricing.unit_price')?></label>
			    <div class="col-sm-10">
			    	<div class="input-group">
				      	<div class="input-group-addon">$</div>
				      	<input type="text" class="form-control" name="unitPrice" id="inputUnitPrice" placeholder="required" disabled>
				      	<div class="input-group-addon">USD</div>
			        </div>
			    </div>
			  </div>			  
			  <div class="form-group">
			    <label for="inputBatchPrices" class="col-sm-2 control-label"><?=$this->lang->line('Product.pricing.batch_prices')?></label>
			    <div class="col-sm-10">
			      <pre id="inputBatchPrices"></pre>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputPricesDetail" class="col-sm-2 control-label"><?=$this->lang->line('Product.pricing.detail')?></label>
			    <div class="col-sm-10">
			      <pre id="inputPricesDetail"></pre>
			    </div>
			  </div>
<?}//if?>
<?if($this->session->userdata('account')["role"] != "vendor"){?>  
			  <div class="form-group">
			    <label for="inputName" class="col-sm-2 control-label"><?=$this->lang->line('Product.pricing.si.unit_price')?></label>
			    <div class="col-sm-10">
			    	<div class="input-group">
				      	<div class="input-group-addon">$</div>
				      	<input type="text" class="form-control" name="unitPrice_si" id="inputUnitPrice_si" placeholder="required" disabled>
				      	<div class="input-group-addon">USD</div>
			        </div>
			    </div>
			  </div>			  
			  <div class="form-group">
			    <label for="inputBatchPrices_si" class="col-sm-2 control-label"><?=$this->lang->line('Product.pricing.si.batch_prices')?></label>
			    <div class="col-sm-10">
			      <pre id="inputBatchPrices_si"  ></pre>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputPricesDetail_si" class="col-sm-2 control-label"><?=$this->lang->line('Product.pricing.si.detail')?></label>
			    <div class="col-sm-10">
			      <pre id="inputPricesDetail_si" ></pre>
			    </div>
			  </div>
<?}//if?>
			  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button id="goto_contact_btn" class="btn btn-default pull-right"><?=$this->lang->line('product/view.btn.next')?></button>
				    </div>
			  </div>			  
      </div>
    </div>
  </div>  
<?}//isShowPrice?>
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="step5">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#contact" aria-expanded="false" aria-controls="contact">
          <?=$this->lang->line('Product.contact')?>          
        </a>
      </h4>
    </div>
    <div id="contact" class="panel-collapse collapse" role="tabpanel" aria-labelledby="step5">
      <div class="panel-body">
			  <div class="form-group">
			    <label for="inputContact_name" class="col-sm-2 control-label"><?=$this->lang->line('Product.contact.contact_name')?></label>
			    <div class="col-sm-10">
			      <pre id="inputContact_name"></pre>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputCompany_name" class="col-sm-2 control-label"><?=$this->lang->line('Product.contact.company_name')?></label>
			    <div class="col-sm-10">
			      <pre id="inputCompany_name"></pre>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputCompany_site" class="col-sm-2 control-label"><?=$this->lang->line('Product.contact.company_site')?></label>
			    <div class="col-sm-10">
			      <pre id="inputCompany_site"></pre>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputTel" class="col-sm-2 control-label"><?=$this->lang->line('Product.contact.tel')?></label>
			    <div class="col-sm-10">
			      <pre id="inputTel"></pre>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputEmail" class="col-sm-2 control-label"><?=$this->lang->line('Product.contact.email')?></label>
			    <div class="col-sm-10">
			      <pre id="inputEmail"></pre>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputMemo" class="col-sm-2 control-label"><?=$this->lang->line('Product.contact.other')?></label>
			    <div class="col-sm-10">
			     <pre id="inputMemo"></pre>
			    </div>
			  </div>			  			  
      </div>
    </div>
  </div>  
</div>


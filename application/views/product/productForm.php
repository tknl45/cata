<!--======Jcrop======-->
<script type="text/javascript" src="<?=base_url()?>js/jcrop/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>js/jcrop/jquery.Jcrop.min.css">

<!--======Main======-->
<script type="text/javascript">
$( document ).ready(function() {
	var product_json= <?=$product_json?>;
	if(product_json!=null){
		$("#productForm").attr("action","<?=base_url()?>product/modify");
		$("#pid").val(product_json.pid);
		$("#userid").val(product_json.userid);
		$("#inputName").val(product_json.name);
      	$("#inputBrand").val(product_json.brand);
      	$("#inputModel").val(product_json.model);
      	$("#inputIntroduction").val(product_json.introduction);
      	$("#inputDescription").val(product_json.description);
      	$("#inputTechSpec").val(product_json.techSpec);
      	$("#inputYoutubeURL").val(product_json.youtubeURL);
      	$("#inputHomepage").val(product_json.homepage);
		$("#inputWarranty").val(product_json.warranty);
      	$("#inputDeployment").val(product_json.deployment);
		$("#inputTraining").val(product_json.training);
		$("#inputUnitPrice").val(product_json.unitPrice);
      	$("#inputBatchPrices").val(product_json.batchPrices);
		$("#inputPricesDetail").val(product_json.pricesDetail);
<?if( 
	$this->session->userdata('account')["role"] == "si"  ||
    $this->session->userdata('account')["role"] == "root"
){?>
		$("#inputUnitPrice_si").val(product_json.unitPrice_si);
      	$("#inputBatchPrices_si").val(product_json.batchPrices_si);
		$("#inputPricesDetail_si").val(product_json.pricesDetail_si);
<?}?>
      	$("#inputContact_name").val(product_json.contact_name);
      	$("#inputCompany_site").val(product_json.company_site);
      	$("#inputCompany_name").val(product_json.company_name);
		$("#inputTel").val(product_json.tel);
		$("#inputEmail").val(product_json.email);      
		$("#inputMemo").val(product_json.memo);
		$("#docfilenames").val(product_json.docfilenames);
		$("#docfiles").val(product_json.docfiles);
		$("#largeImagefilenames").val(product_json.largeImagefilenames);
		$("#largeImagefiles").val(product_json.largeImagefiles);
		$("#smallImagefilenames").val(product_json.smallImagefilenames);
		$("#smallImagefiles").val(product_json.smallImagefiles);
		//checkbox menu
      	var icampusArr = product_json.icampus_category.split(",");
      	for(var i =0 ;i< icampusArr.length ; i++){
      		var item = $("input[name='icampus_category[]'][value='"+icampusArr[i]+"']");
      		item.attr('checked', 1);
      		item.parent().addClass("active");
      	}
      	var productArr = product_json.product_category.split(",");
      	for(var i =0 ;i< productArr.length ; i++){
      		var item = $("input[name='product_category[]'][value='"+productArr[i]+"']");
      		item.attr('checked', 1);
      		item.parent().addClass("active");
      	}
      	var levelArr = product_json.level.split(",");
      	for(var i =0 ;i< levelArr.length ; i++){
      		var item = $("input[name='level[]'][value='"+levelArr[i]+"']");
      		item.attr('checked', 1);
      		item.parent().addClass("active");
      	}      	
		
	}

	initYoutubeForm();
      $("#productForm").validate({ 
      		errorElement: 'div',            
            rules:{
              name:{
                required:true
              },
              brand:{
                required:true
              },
              'icampus_category[]':{
                required:true
              },
              'product_category[]':{
                required:true
              },
              'level[]':{
                required:true
              },
              introduction:{
                required:true
              },
			  description:{
                required:true
			  },
		      techSpec:{
                required:true
		      },
		      largeImagefilenames:{
                required:true
		      },
		      smallImagefilenames:{
                required:true
		      },
		      docfilenames:{
                required:true
		      },
		      unitPrice:{
                required:true,
                number: true
		      },
		      unitPrice_si:{
                required:true,
                number: true
		      },
		      contact_name:{
		      	required:true
		      },
			  company_name:{
			  	required:true
			  },
			  tel:{
			  	required:true
			  },
			  email:{
			  	required:true,
			  	email: true
			  }

            },//end of rule
            messages:{
              name:{
                required:"This field is required."
              },
              brand:{
                required:"This field is required."
              },
              'icampus_category[]':{
                required:"Please select something!"
              },
              'product_category[]':{
                required:"Please select something!"
              },
              'level[]':{
                required:"Please select something!"
              },
              introduction:{
                required:"This field is required."
              },
			  description:{
                required:"This field is required."
			  },
		      techSpec:{
                required:"This field is required."
		      },
		      largeImagefilenames:{
                required:"This field is required.One Picture as least"
		      },
		      smallImagefilenames:{
                required:"This field is required.One Picture as least"
		      },
		      docfilenames:{
                required:"This field is required.One Document as least"
		      },
		      unitPrice:{
                required:"This field is required.",
                number: "Please insert One number"
		      },
		      unitPrice_si:{
                required:"This field is required.",
                number: "Please insert One number"
		      },		      
		      contact_name:{
		      	required:"This field is required."
		      },
			  company_name:{
			  	required:"This field is required."
			  },
			  tel:{
			  	required:"This field is required."
			  },
			  email:{
			  	required:"This field is required.",
			  	email: "Please enter a valid email address"
			  }
            }//end of messages
          
          });//end of validate()

      $('#newProduct').on('hide.bs.collapse', function () {
		   var isOK =  $("#productForm").valid();
		   console.log(isOK);
		   if(!isOK){ return false;}
	  });
      $('#productDetail').on('hide.bs.collapse', function () {
		   var isOK =  $("#productForm").valid();
		   var appendHtml = '<div class="alert alert-danger" role="alert">'+
		   					'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
  							'<a href="#" class="alert-link"><strong>Oh snap!</strong>Please insert A file.</a>'+
							'</div>';
		   if($("#largeImagefilenames").val()==""){
		   		$("#largeImageArea").append(appendHtml);
		   		isOK = false; 
		   }
		   if($("#smallImagefilenames").val()==""){
		   		$("#smallImageArea").append(appendHtml); 
		   		isOK = false;
		   }
		   if($("#docfilenames").val()==""){
		   		$("#docArea").append(appendHtml); 
		   		isOK = false;
		   }

		   console.log(isOK);
		   if(!isOK){ return false;}
	  });

//----------判斷圖片是否存在-------------
		if($("#largeImagefilenames").val()!=""){
		   var fileArr = $("#largeImagefiles").val().split("|");
		   var fileNameArr = $("#largeImagefilenames").val().split("|");
		   for(var i=0 ; i<fileArr.length-1 ; i++){
		   		var appendHtml='<div class="col-md-3">'+
		    		'<button type="button" class="largeImageRemoveBtn btn btn-danger btn-xs" file="'+fileArr[i]+'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'+trim2Short(fileNameArr[i])+'</button>'+
		    		'<img src="<?=base_url()?>upload/product/'+fileArr[i]+'" class="img-thumbnail" width="200">'+
		    		'</div>'; 
		    	$("#largeImageArea").append(appendHtml);
		   }
		}
		if($("#smallImagefilenames").val()!=""){
		   var fileArr = $("#smallImagefiles").val().split("|");
		   var fileNameArr = $("#smallImagefilenames").val().split("|");
		   for(var i=0 ; i<fileArr.length-1 ; i++){
		   		var appendHtml='<div class="col-md-3">'+
		    		'<button type="button" class="smallImageRemoveBtn btn btn-danger btn-xs" file="'+fileArr[i]+'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'+trim2Short(fileNameArr[i])+'</button>'+
		    		'<img src="<?=base_url()?>upload/product/'+fileArr[i]+'" class="img-thumbnail" width="200">'+
		    		'</div>'; 
		    	$("#smallImageArea").append(appendHtml);
		   }
		}
		if($("#docfilenames").val()!=""){
		   	var fileArr = $("#docfiles").val().split("|");
		   	var fileNameArr = $("#docfilenames").val().split("|");
		   	for(var i=0 ; i<fileArr.length-1 ; i++){
	            var appendHtml='<div class="col-md-3">'+
				    		'<button type="button" class="docRemoveBtn btn btn-danger btn-xs" file="'+fileArr[i]+'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'+trim2Short(fileNameArr[i])+'</button>'+
				    		'<a href="<?=base_url()?>upload/product/'+fileArr[i]+'"><img src="<?=base_url()?>images/'+getExtension(fileArr[i])+'-icon.png" class="img-thumbnail" width="200"></a>'+
				    		'</div>';
	            $("#docArea").append(appendHtml);
	        }//for
		}
//------------------------------------
	  $('#goto_productDetail_btn').click(function(){
	  	var isOK =  $("#productForm").valid();
	  	console.log(isOK);
	  	if(isOK){ 
	  		$('#newProduct').collapse('hide');
	  		$('#productDetail').collapse('show');

	  	}
	  	return false;
	  });

	  $('#goto_warranty_btn').click(function(){
	  	var isOK =  $("#productForm").valid();
	  	console.log(isOK);
	  	if(isOK){ 
	  		$('#productDetail').collapse('hide');
	  		$('#warranty').collapse('show');
	  	}
	  	return false;
	  });

	  $('#goto_pricing_btn').click(function(){	  	
	  		$('#warranty').collapse('hide');
	  		$('#pricing').collapse('show');
	  	return false;
	  });

	  $('#goto_contact_btn').click(function(){
	  	var isOK =  $("#productForm").valid();
	  	console.log(isOK);
	  	if(isOK){ 
	  		$('#pricing').collapse('hide');
	  		$('#contact').collapse('show');
	  	}
	  	return false;
	  });

	  $('#save_btn').click(function(){	  	
	  	var isOK =  $("#productForm").valid();
	  	console.log(isOK);
	  	var needToAlert = ($("#productForm").attr("action").indexOf("product/add")>-1);

	  	if(isOK){
	  		var options = { 
	                  //beforeSubmit:  showRequest  // pre-submit callback 
	                  success:       showResponse  // post-submit callback 
	              	}; 
	  		if(needToAlert){ //vendor add
				var r = confirm("<?=$this->lang->line('product/productForm.confirm.save')?>");
				if (r == true) {					
	            	$('#productForm').ajaxSubmit(options); 
				} 	  			
	  		}else{ // si/root modify
	  			$('#productForm').ajaxSubmit(options); 
	  		}
	  		
	  	}

	  	return false;
	  });

	  //移除圖片	
	  $(document.body).on("click",'.largeImageRemoveBtn',function(){
	  		var removefile = $(this).attr("file");
	  		
	  		if($("#largeImagefiles").val()!=""){
	  			var fileArr = $("#largeImagefiles").val().split("|");
	  			var fileNameArr = $("#largeImagefilenames").val().split("|");
	  			var removeIndex = fileArr.indexOf(removefile); 	//要刪除的index
	  			fileArr.splice(removeIndex, 1);					//刪除
	  			fileNameArr.splice(removeIndex, 1);		
	  			$("#largeImagefiles").val(fileArr.join("|")); 	//變回來
	  			$("#largeImagefilenames").val(fileNameArr.join("|"));
	  			$(this).parent().remove();
	  			console.log("remove file:"+removefile);
	  		}else{
	  			console.log(removefile+" can not found!");
	  		}
	  });
	  //移除圖片	
	  $(document.body).on("click",'.smallImageRemoveBtn',function(){
	  		var removefile = $(this).attr("file");
	  		
	  		if($("#smallImagefiles").val()!=""){
	  			var fileArr = $("#smallImagefiles").val().split("|");
	  			var fileNameArr = $("#smallImagefilenames").val().split("|");
	  			var removeIndex = fileArr.indexOf(removefile); 	//要刪除的index
	  			fileArr.splice(removeIndex, 1);			//刪除
	  			fileNameArr.splice(removeIndex, 1);		
	  			$("#smallImagefiles").val(fileArr.join("|")); //變回來
	  			$("#smallImagefilenames").val(fileNameArr.join("|"));
	  			$(this).parent().remove();
	  			console.log("remove file:"+removefile);
	  		}else{
	  			console.log(removefile+" can not found!");
	  		}
	  });
	  //移除文件
	  $(document.body).on("click",'.docRemoveBtn',function(){
	  		var removefile = $(this).attr("file");
	  		
	  		if($("#docfiles").val()!=""){
	  			var fileArr = $("#docfiles").val().split("|");
	  			var fileNameArr = $("#docfilenames").val().split("|");
	  			var removeIndex = fileArr.indexOf(removefile); 	//要刪除的index
	  			fileArr.splice(removeIndex, 1);			//刪除
	  			fileNameArr.splice(removeIndex, 1);		
	  			$("#docfiles").val(fileArr.join("|")); //變回來
	  			$("#docfilenames").val(fileNameArr.join("|"));
	  			$(this).parent().remove();
	  			console.log("remove file:"+removefile);
	  		}else{
	  			console.log(removefile+" can not found!");
	  		}
	  });	  
/**--------- Large uploader related --------------------------------*/
        var largeImageUpload=new plupload.Uploader({
            runtimes:'gears,html5,flash,browserplus',
            browse_button:'largeImageUpload',
            //container:'uploader',
            max_file_size:'1mb',
            url:'<?=base_url()?>uploadFile/uploadProduct',
            flash_swf_url:'<?=base_url()?>js/plupload/plupload.flash.swf',
            filters : [
                { title : "Image files", extensions : "jpg,gif,png" }
            ]
        });

        largeImageUpload.init();
        //上傳完成
        largeImageUpload.bind('FilesAdded',function(up,files){
            //only one file can be select
            while(up.files.length>1){
                up.removeFile(up.files[0]);
            }
           	console.log(files[0].name);
            largeImageUpload.start();
            up.refresh();
            $('#imgModal').modal('show');
            $('#upload_progress').show();
        });

        //上傳前
        largeImageUpload.bind('BeforeUpload', function(up,file){           
            up.settings.multipart_params={fileName:file.name,fileId:file.id};

        });
        //上傳過程
        largeImageUpload.bind('UploadProgress',function(up,file){
            $('#upload_progress').html(file.percent+"%");
            $('#upload_progress').attr('aria-valuenow',file.percent);
            $('#upload_progress').attr('style','width:'+file.percent+'%');
        });
        //上傳完成
        largeImageUpload.bind('UploadComplete',function(up,file,response){
            $('#upload_progress').html("0%");
 			$('#upload_progress').hide();
        });
      
        largeImageUpload.bind('FileUploaded',function(up,file,response){
 			console.log(response);

          $("#crop-target").attr('src','<?=base_url()?>upload/product/'+response.response);

          $('.jcrop-holder img').attr('src', '<?=base_url()?>upload/product/'+response.response);
          $('#src').val(response.response);  //record image name
          $('#size').val("large");
          $("#filename").val(file.name);
          $('#crop-target').Jcrop({
            bgColor:'black',
            bgOpacity:.4,
            aspectRatio: 800/300,
            minSize:[800,300],
            setSelect:[0,0,800,300],
            boxHeight:300,
            onChange:recordCoords
          });


 			// var appendHtml='<div class="col-md-3">'+
		  //   		'<button type="button" class="largeImageRemoveBtn btn btn-danger btn-xs" file="'+response.response+'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>remove</button>'+
		  //   		'<img src="<?=base_url()?>upload/product/'+response.response+'" class="img-thumbnail" width="200">'+
		  //   		'</div>';  
    //          $("#largeImagefiles").val($("#largeImagefiles").val()+response.response+"|");
    //          $("#largeImagefilenames").val($("#largeImagefilenames").val()+file.name+"|");

    //         $("#largeImageArea").append(appendHtml);        
        });

/**--------- Small uploader related --------------------------------*/
        var smallImageUpload=new plupload.Uploader({
            runtimes:'gears,html5,flash,browserplus',
            browse_button:'smallImageUpload',
            //container:'uploader',
            max_file_size:'1mb',
            url:'<?=base_url()?>uploadFile/uploadProduct',
            flash_swf_url:'<?=base_url()?>js/plupload/plupload.flash.swf',
            filters : [
                { title : "Image files", extensions : "jpg,gif,png" }
            ]
        });

        smallImageUpload.init();
        //上傳完成
        smallImageUpload.bind('FilesAdded',function(up,files){
            //only one file can be select
            while(up.files.length>1){
                up.removeFile(up.files[0]);
            }
           
            smallImageUpload.start();
            up.refresh();
            $('#imgModal').modal('show');
            $('#upload_progress').show();
        });

        //上傳前
        smallImageUpload.bind('BeforeUpload', function(up,file){           
            up.settings.multipart_params={fileName:file.name,fileId:file.id};
        });
        //上傳過程
        smallImageUpload.bind('UploadProgress',function(up,file){
            $('#upload_progress').html(file.percent+"%");
            $('#upload_progress').attr('aria-valuenow',file.percent);
            $('#upload_progress').attr('style','width:'+file.percent+'%');
        });
        //上傳完成
        smallImageUpload.bind('UploadComplete',function(up,file,response){
            $('#upload_progress').html("0%");
            $('#upload_progress').hide();
        });
        smallImageUpload.bind('FileUploaded',function(up,file,response){
 			
          $("#crop-target").attr('src','<?=base_url()?>upload/product/'+response.response);
          $('.jcrop-holder img').attr('src', '<?=base_url()?>upload/product/'+response.response);
          console.log(response);
          $('#src').val(response.response);  //record image name
          $('#size').val("small");
          $("#filename").val(file.name);
          $('#crop-target').Jcrop({
            bgColor:'black',
            bgOpacity:.4,
            aspectRatio: 200/200,
            minSize:[200,200],
            setSelect:[0,0,200,200],
            boxHeight:200,
            onChange:recordCoords
          });
        });

/**--------- document uploader related --------------------------------*/
        var docUpload=new plupload.Uploader({
            runtimes:'gears,html5,flash,browserplus',
            browse_button:'docUpload',
            //container:'uploader',
            max_file_size:'5mb',
            url:'<?=base_url()?>uploadFile/uploadProduct',
            flash_swf_url:'<?=base_url()?>js/plupload/plupload.flash.swf',
            filters : [
                { title : "document files", extensions : "doc,pdf" }
            ]
        });

        docUpload.init();
        //上傳完成
        docUpload.bind('FilesAdded',function(up,files){
            //only one file can be select
            while(up.files.length>1){
                up.removeFile(up.files[0]);
            }
           
            docUpload.start();
            up.refresh();

        });

        //上傳前
        docUpload.bind('BeforeUpload', function(up,file){           
            up.settings.multipart_params={fileName:file.name,fileId:file.id};
        });
        //上傳過程
        docUpload.bind('UploadProgress',function(up,file){
            $('#upload_progress').html(file.percent+"%");
            $('#upload_progress').attr('aria-valuenow',file.percent);
            $('#upload_progress').attr('style','width:'+file.percent+'%');
        });
        //上傳完成
        docUpload.bind('UploadComplete',function(up,file,response){
            $('#upload_progress').html("0%");
           
        });
        docUpload.bind('FileUploaded',function(up,file,response){
 			console.log(response);
             $("#docfiles").val($("#docfiles").val()+response.response+"|");
             $("#docfilenames").val($("#docfilenames").val()+file.name+"|");
            var appendHtml='<div class="col-md-3">'+
			    		'<button type="button" class="docRemoveBtn btn btn-danger btn-xs" file="'+response.response+'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>remove</button>'+
			    		'<a href="<?=base_url()?>upload/product/'+response.response+'"><img src="<?=base_url()?>images/'+getExtension(response.response)+'-icon.png" class="img-thumbnail" width="200"></a>'+
			    		'</div>';
            $("#docArea").append(appendHtml);        
        });

        //圖片切割
        $("#imgCropBtn").click(function(){
          var options = { 
              // beforeSubmit:  showRequest,  // pre-submit callback 
              success:       showCropResponse  // post-submit callback 
          }; 
          $('#cropForm').ajaxSubmit(options);




          return false; 
        });        

});


  function recordCoords(c)
  {

    $('input#crop_x').attr('value',c.x);
    $('input#crop_y').attr('value',c.y);
    $('input#crop_width').attr('value',c.w);
    $('input#crop_height').attr('value',c.h);
  };
/**-----------------youTube----------------------*/
	function initYoutubeForm(){
		//register url input field change event listener
		$('#inputYoutubeURL').keyup(function(event){
			console.log($(this).val());
			if($(this).val().indexOf("http://www.youtube.com/watch")!=-1){
				var videoId=$(this).attr('value').replace("http://www.youtube.com/watch?v=","");
				if(videoId===$('input[name="content"]').val()&&$('#preview-container iframe').length!=0) return;	//如果video url一樣所觸發的事件就忽略
				$('input[name="content"]').val(videoId);
				$(this).next('#preview-container').empty().html('<iframe id="previewer" width="100%" height="100%" src="http://www.youtube.com/embed/'+videoId+'?wmode=transparent" frameborder="0" allowfullscreen></iframe>');
			}
			else if($(this).val().indexOf("https://www.youtube.com/watch")!=-1){ //141008 45 added
				var videoId=$(this).val().replace("https://www.youtube.com/watch?v=","");
				if(videoId===$('input[name="content"]').val()&&$('#preview-container iframe').length!=0) return;	//如果video url一樣所觸發的事件就忽略
				$('input[name="content"]').val(videoId);
				$(this).next('#preview-container').empty().html('<iframe id="previewer" width="100%" height="100%" src="http://www.youtube.com/embed/'+videoId+'?wmode=transparent" frameborder="0" allowfullscreen></iframe>');
			}
			else{
				$('input[name="content"]').val("");
				$(this).next('#preview-container').empty();
				$(this).next('#preview-container').html("<h2>Video Prievew</h2>");
                
			}
		});
		$('#url-field').mouseleave(function(){$(this).trigger('keyup')});
	}

function getExtension(filename){
	var fArray = filename.split(".");
	return fArray[fArray.length-1];
}

function trim2Short(filename){
	var shortLength = 15;
	if(filename.length>shortLength){
		filename = filename.substr(0, shortLength)+"...";
	}
	return filename;
}	

function hideAllCollapse(){
	$('#newProduct').collapse('hide');
	$('#productDetail').collapse('hide');
	$('#warranty').collapse('hide');
	$('#pricing').collapse('hide');
	$('#contact').collapse('hide');
}

// pre-submit callback 
function showRequest(formData, jqForm, options) { 
    var queryString = $.param(formData); 
    alert('About to submit: \n\n' + queryString); 
    return true; 
} 
 
// post-submit callback 
function showResponse(responseText, statusText, xhr, $form)  { 
    location.href="<?=base_url()?>product/viewList"; 
}	

//切圖成功
function showCropResponse(responseText, statusText, xhr, $form)  { 
   if(responseText=="OK"){
          var imgfile = $("#src").val();
          var sizeTarget = $("#size").val();
          var filename = $("#filename").val();
           $("#"+sizeTarget+"Imagefiles").val($("#"+sizeTarget+"Imagefiles").val()+imgfile+"|");
           $("#"+sizeTarget+"Imagefilenames").val($("#"+sizeTarget+"Imagefilenames").val()+filename+"|");
           var appendHtml='<div class="col-md-3">'+
			    		'<button type="button" class="'+sizeTarget+'ImageRemoveBtn btn btn-danger btn-xs" file="'+imgfile+'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'+filename+'</button>'+
			    		'<img src="<?=base_url()?>upload/product/'+imgfile+'?d='+Math.random()+'" class="img-thumbnail" width="200">'+
			    		'</div>';
            $("#"+sizeTarget+"ImageArea").append(appendHtml);     	
            $('#imgModal').modal('hide');
   }else{
      alert(responseText);
   }
}  
</script>


<a id="listBtn" class="btn btn-default" href="<?=base_url()?>product/viewList" role="button"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><?=$this->lang->line('product/view.btn.view_list')?></a>
<form id="productForm" class="form-horizontal" method="POST" action="<?=base_url()?>product/add">
<input id="pid" name="pid" type="hidden">
<input id="userid" name="userid" type="hidden">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="step1">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#newProduct" aria-expanded="true" aria-controls="newProduct">
          <?=$this->lang->line('Product.info')?>
        </a>
      </h4>
    </div>
	<div id="newProduct" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="step1">
		<div class="panel-body">
			  <div class="form-group">
			    <label for="inputName" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.info.name')?>*</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="name" id="inputName" placeholder="required" required>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputBrand" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.info.name')?>*</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="brand" id="inputBrand" placeholder="required" required>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputModel" class="col-sm-2 control-label"><?=$this->lang->line('Product.info.model')?></label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="model" id="inputModel" >
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputBrand" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.info.icampus_category')?>*</label>
			    <div class="col-sm-10">
					<div class="btn-group" data-toggle="buttons">
					  <label class="btn btn-default">
					    <input type="checkbox" name="icampus_category[]" value="iLearning" autocomplete="off"><?=$this->lang->line('Category.iCampus.iLearning')?>
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="icampus_category[]" value="iManagement" autocomplete="off"><?=$this->lang->line('Category.iCampus.iManagement')?>
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="icampus_category[]" value="iGovernance" autocomplete="off"><?=$this->lang->line('Category.iCampus.iGovernance')?> 
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="icampus_category[]" value="iSocial" autocomplete="off"><?=$this->lang->line('Category.iCampus.iSocial')?> 
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="icampus_category[]" value="iHealth" autocomplete="off"><?=$this->lang->line('Category.iCampus.iHealth')?> 
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="icampus_category[]" value="iGreen" autocomplete="off"><?=$this->lang->line('Category.iCampus.iGreen')?>
					  </label>	
					  <label class="btn btn-default">
					    <input type="checkbox" name="icampus_category[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.iCampus.others')?>
					  </label>					  				  					  					  
					</div>			      
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputBrand" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.info.product_category')?>*</label>
			    <div class="col-sm-10">
					<div class="btn-group" data-toggle="buttons">
					  <label class="btn btn-default">
					    <input type="checkbox" name="product_category[]" value="Total Solution" autocomplete="off"><?=$this->lang->line('Category.product.total_solution')?>
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="product_category[]" value="Service" autocomplete="off"><?=$this->lang->line('Category.product.service')?>
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="product_category[]" value="Software" autocomplete="off"><?=$this->lang->line('Category.product.software')?> 
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="product_category[]" value="Hardware" autocomplete="off"><?=$this->lang->line('Category.product.hardware')?> 
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="product_category[]" value="Digital Content" autocomplete="off"><?=$this->lang->line('Category.product.digital_content')?>
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="product_category[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.product.others')?>
					  </label>					  				  					  					  
					</div>	
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputBrand" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.info.eduLevel')?>*</label>
			    <div class="col-sm-10">
					<div class="btn-group" data-toggle="buttons">
					  <label class="btn btn-default">
					    <input type="checkbox" name="level[]" value="Kindergarden" autocomplete="off"><?=$this->lang->line('Category.eduLevel.kindergarden')?>
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="level[]" value="Elementary School" autocomplete="off"><?=$this->lang->line('Category.eduLevel.elementary')?>
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="level[]" value="High School" autocomplete="off"><?=$this->lang->line('Category.eduLevel.high_school')?> 
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="level[]" value="Higher Education" autocomplete="off"><?=$this->lang->line('Category.eduLevel.higher_education')?>
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="level[]" value="Vocational Training" autocomplete="off"><?=$this->lang->line('Category.eduLevel.vocational')?>
					  </label>
					  <label class="btn btn-default">
					    <input type="checkbox" name="level[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.eduLevel.others')?>
					  </label>					  				  					  					  
					</div>
			    </div>
			  </div>
			  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button id="goto_productDetail_btn" class="btn btn-default pull-right"><?=$this->lang->line('product/productForm.btn.next')?></button>
				    </div>
			  </div>			  			  			  			  
      </div>
    </div><!--<div id="newProduct"-->
  </div>
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="step2">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#productDetail" aria-expanded="false" aria-controls="productDetail">
          <?=$this->lang->line('Product.detail')?>
        </a>
      </h4>
    </div>
    <div id="productDetail" class="panel-collapse collapse" role="tabpanel" aria-labelledby="step2">
      <div class="panel-body">      
			  <div class="form-group">
			    <label for="inputIntroduction" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.detail.introduction')?>*</label>
			    <div class="col-sm-10">
			      <textarea rows="3" class="form-control" name="introduction" id="inputIntroduction" placeholder="required" required></textarea>
			    </div>
			  </div> 
			  <div class="form-group">
			    <label for="inputDescription" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.detail.description')?>*</label>
			    <div class="col-sm-10">
			      <textarea rows="3" class="form-control" name="description" id="inputDescription" placeholder="required" required></textarea>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputTechSpec" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.detail.techSpec')?>*</label>
			    <div class="col-sm-10">
			      <textarea rows="3" class="form-control" name="techSpec" id="inputTechSpec" placeholder="required" required></textarea>
			    </div>
			  </div>			  
			  <div class="form-group">
			    <label  class="col-sm-2 control-label text-danger">
			    	<?=$this->lang->line('Product.detail.largeImage')?>*
			    	<button type="button" class="btn btn-default btn-xs" id="largeImageUpload"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add</button>
			    	<input id="largeImagefiles" name="largeImagefiles" type="hidden" required>
			    	<input id="largeImagefilenames" name="largeImagefilenames" type="hidden">
			    </label>
			    <div class="col-sm-10" id="largeImageArea">
			    	<!--	
			    		<div class="col-md-3">	
			    				<button type="button" class="btn btn-danger btn-xs largeImageRemoveBtn" file="xxx.jpg"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>remove</button>
			    				<img src="<?=base_url()?>images/800x300.jpg" class="img-thumbnail" width="200">
			    		</div>
			    	-->			    					    					      
			    </div>
			  </div>
			  <div class="form-group">
			    <label  class="col-sm-2 control-label text-danger">
			    	<?=$this->lang->line('Product.detail.smallImage')?>*
					<button type="button" class="btn btn-default btn-xs" id="smallImageUpload"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add</button>
			    	<input id="smallImagefiles" name="smallImagefiles" type="hidden" required>
			    	<input id="smallImagefilenames" name="smallImagefilenames" type="hidden">
			    </label>
			    <div class="col-sm-10" id="smallImageArea">
			      
			    </div>
			  </div>
			  <div class="form-group">
			    <label  class="col-sm-2 control-label text-danger">
			    	<?=$this->lang->line('Product.detail.document')?>*
			    	<button type="button" class="btn btn-default btn-xs" id="docUpload"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add</button>
			    	<input id="docfiles" name="docfiles" type="hidden" required>
			    	<input id="docfilenames" name="docfilenames"type="hidden">
			    </label>
			    <div class="col-sm-10" id="docArea">
			      
			    </div>
			  </div>			  
			  <div class="form-group">
			    <label for="inputYoutubeURL" class="col-sm-2 control-label"><?=$this->lang->line('Product.detail.youtubeURL')?></label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="youtubeURL" id="inputYoutubeURL" placeholder="youtube url">
			      <div id="preview-container" class="col-sm-2"><h2>Video Prievew</h2></div>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputHomepage" class="col-sm-2 control-label"><?=$this->lang->line('Product.detail.homepage')?></label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="homepage" id="inputHomepage" placeholder="http://...">
			    </div>
			  </div> 
			  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button id="goto_warranty_btn" class="btn btn-default pull-right"><?=$this->lang->line('product/productForm.btn.next')?></button>
				    </div>
			  </div>			  			  			          
      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="step3">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#warranty" aria-expanded="false" aria-controls="warranty">
          <?=$this->lang->line('Product.warranty')?>
        </a>
      </h4>
    </div>
    <div id="warranty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="step3">
      <div class="panel-body">
			  <div class="form-group">
			    <label for="inputWarranty" class="col-sm-2 control-label"><?=$this->lang->line('Product.warranty.warranty')?></label>
			    <div class="col-sm-10">
			      <textarea rows="3" class="form-control" name="warranty" id="inputWarranty" placeholder="required" ></textarea>
			    </div>
			  </div> 
			  <div class="form-group">
			    <label for="inputDeployment" class="col-sm-2 control-label"><?=$this->lang->line('Product.warranty.deployment')?></label>
			    <div class="col-sm-10">
			      <textarea rows="3" class="form-control" name="deployment" id="inputDeployment" placeholder="required" ></textarea>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputTraining" class="col-sm-2 control-label"><?=$this->lang->line('Product.warranty.training')?></label>
			    <div class="col-sm-10">
			      <textarea rows="3" class="form-control" name="training" id="inputTraining" placeholder="required" ></textarea>
			    </div>
			  </div>
			  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button id="goto_pricing_btn" class="btn btn-default pull-right"><?=$this->lang->line('product/productForm.btn.next')?></button>
				    </div>
			  </div>			  
      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="step4">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#pricing" aria-expanded="false" aria-controls="pricing">
			<?=$this->lang->line('Product.pricing')?>
        </a>
      </h4>
    </div>
    <div id="pricing" class="panel-collapse collapse" role="tabpanel" aria-labelledby="step4">
      <div class="panel-body">
			  <div class="form-group">
			    <label for="inputName" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.pricing.unit_price')?>*</label>
			    <div class="col-sm-10">
			    	<div class="input-group">
				      	<div class="input-group-addon">$</div>
				      	<input type="text" class="form-control" name="unitPrice" id="inputUnitPrice" placeholder="required" required>
				      	<div class="input-group-addon">USD</div>
			        </div>
			    </div>
			  </div>			  
			  <div class="form-group">
			    <label for="inputBatchPrices" class="col-sm-2 control-label"><?=$this->lang->line('Product.pricing.batch_prices')?></label>
			    <div class="col-sm-10">
			      <textarea rows="3" class="form-control" name="batchPrices" id="inputBatchPrices"></textarea>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputPricesDetail" class="col-sm-2 control-label"><?=$this->lang->line('Product.pricing.detail')?></label>
			    <div class="col-sm-10">
			      <textarea rows="3" class="form-control" name="pricesDetail" id="inputPricesDetail"></textarea>
			    </div>
			  </div>
<?if( 
	$this->session->userdata('account')["role"] == "si"  ||
    $this->session->userdata('account')["role"] == "root"
){?>			  
			  <div class="form-group has-error">
			    <label for="inputName" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.pricing.si.unit_price')?>*</label>
			    <div class="col-sm-10">
			    	<div class="input-group">
				      	<div class="input-group-addon">$</div>
				      	<input type="text" class="form-control" name="unitPrice_si" id="inputUnitPrice_si" placeholder="required" required>
				      	<div class="input-group-addon">USD</div>
			        </div>
			    </div>
			  </div>			  
			  <div class="form-group has-error">
			    <label for="inputBatchPrices_si" class="col-sm-2 control-label"><?=$this->lang->line('Product.pricing.si.batch_prices')?></label>
			    <div class="col-sm-10">
			      <textarea rows="3" class="form-control" name="batchPrices_si" id="inputBatchPrices_si"  ></textarea>
			    </div>
			  </div>
			  <div class="form-group has-error">
			    <label for="inputPricesDetail_si" class="col-sm-2 control-label"><?=$this->lang->line('Product.pricing.si.detail')?></label>
			    <div class="col-sm-10">
			      <textarea rows="3" class="form-control" name="pricesDetail_si" id="inputPricesDetail_si" ></textarea>
			    </div>
			  </div>
<?}//if?>
			  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button id="goto_contact_btn" class="btn btn-default pull-right"><?=$this->lang->line('product/productForm.btn.next')?></button>
				    </div>
			  </div>
      </div>
    </div>
  </div>  

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="step5">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#contact" aria-expanded="false" aria-controls="contact">
          <?=$this->lang->line('Product.contact')?> 
        </a>
      </h4>
    </div>
    <div id="contact" class="panel-collapse collapse" role="tabpanel" aria-labelledby="step5">
      <div class="panel-body">
			  <div class="form-group">
			    <label for="inputContact_name" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.contact.contact_name')?>*</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="contact_name" id="inputContact_name" placeholder="required" required value="<?=$this->session->userdata('account')['contact_name']?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputCompany_name" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.contact.company_name')?>*</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="company_name" id="inputCompany_name" placeholder="required" required value="<?=$this->session->userdata('account')['company_name']?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputCompany_site" class="col-sm-2 control-label"><?=$this->lang->line('Product.contact.company_site')?></label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="company_site" id="inputCompany_site" value="<?=$this->session->userdata('account')['company_site']?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputTel" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.contact.tel')?>*</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="tel" id="inputTel" placeholder="required" required value="<?=$this->session->userdata('account')['tel']?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputEmail" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Product.contact.email')?>*</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="email" id="inputEmail" placeholder="required" required value="<?=$this->session->userdata('account')['email']?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputMemo" class="col-sm-2 control-label"><?=$this->lang->line('Product.contact.other')?></label>
			    <div class="col-sm-10">
			      <textarea rows="3" class="form-control" name="memo" id="inputMemo"></textarea>
			    </div>
			  </div>
			  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button id="save_btn" class="btn btn-default pull-right"><?=$this->lang->line('product/productForm.btn.save')?></button>
				    </div>
			  </div>			  			  
      </div>
    </div>
  </div>  
</div>
</form>

<div class="modal fade" id="imgModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=$this->lang->line('product/productForm.modal.imgProcess')?></h4>
      </div>
      <div class="modal-body">
        <div id="upload_progress" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
            60%
        </div>
        <div id="imgShowArea">
          <img id="crop-target" style="max-width:inherit" src=""/>
        </div>
    <form id="cropForm" method="post" action="<?=base_url()?>uploadFile/cropImage/product">
      <input type="hidden" name="size" id="size"/>
      <input type="hidden" name="filename" id="filename"/>	
      <input type="hidden" name="src" id="src"/>
      <input type="hidden" name="crop_x" id="crop_x" value=""/>
      <input type="hidden" name="crop_y" id="crop_y" value=""/>
      <input type="hidden" name="crop_width" id="crop_width" value=""/>
      <input type="hidden" name="crop_height" id="crop_height" value=""/>
    </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('product/productForm.btn.close')?></button>
        <button type="button" class="btn btn-default" id="imgCropBtn"><?=$this->lang->line('product/productForm.btn.saveChanges')?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
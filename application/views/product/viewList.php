<script type="text/javascript">
$( document ).ready(function() {
	var search_json = <?=(empty($search_json)?"null":$search_json)?>;
	if(search_json!=null){
		$("#searchInput").val(search_json.search);
		var icampusArr = search_json.icampus_category;
		var productArr = search_json.product_category;
		var levelArr = search_json.level;
      	for(var i =0 ;i< icampusArr.length ; i++){
      		var item = $("input[name='icampus_category[]'][value='"+icampusArr[i]+"']");
      		item.attr('checked', 1);
      		item.parent().addClass("active");
      	}

      	for(var i =0 ;i< productArr.length ; i++){
      		var item = $("input[name='product_category[]'][value='"+productArr[i]+"']");
      		item.attr('checked', 1);
      		item.parent().addClass("active");
      	}

      	for(var i =0 ;i< levelArr.length ; i++){
      		var item = $("input[name='level[]'][value='"+levelArr[i]+"']");
      		item.attr('checked', 1);
      		item.parent().addClass("active");
      	} 

	    // if(levelArr!=false || productArr!=false || icampusArr!=false){
	    // 	$("#search_cata_area").show();
	    // }
	}



      $('.removeBtn').click(function() {
        var pid = $(this).attr("pid");
        if(confirm("<?=$this->lang->line('product/viewList.alert.remove')?>"))
        {
          $.ajax({
             type: 'GET',
             url: "<?=base_url()?>product/remove/"+pid,
             success: function(data){
                location.reload(); 
             },
             error: function(jqXHR, textStatus, errorThrown) {
                  // report error
                  alert("ERROR:"+textStatus);
                  alert(errorThrown);
              }
          });//ajax
        }//if(confirm)
      }); //.removeBtn

      $('#showCataArea').click(function() {
      	$("#search_cata_area").slideToggle('slow');
      });	

      $(".verifyBtn").click(function() {
      	var pid = $(this).attr("pid");
      	var btn = $(this);
      	var isVerify = ($(this).text() == "<?=$this->lang->line('product/viewList.isVerified.yes')?>")?"0":"1";
	      var dataString = {pid:pid ,isVerify:isVerify};
	        console.log(dataString);
	        $.ajax({
	          type: "POST",
	          url: "<?=base_url()?>product/verify",
	          data: dataString,
	          cache: false,
	          success: function(data){
	            console.log(data);
	            if(data=="OK"){
	              alert("<?=$this->lang->line('product/viewList.alert.changed')?>");
			 		if(btn.text() == "<?=$this->lang->line('product/viewList.isVerified.yes')?>"){
			      		btn.text("<?=$this->lang->line('product/viewList.isVerified.no')?>");
			      		btn.removeClass("btn-success");
			      		btn.addClass("btn-danger");
			      	}else{
			      		btn.text("<?=$this->lang->line('product/viewList.isVerified.yes')?>");
			      		btn.removeClass("btn-danger");
			      		btn.addClass("btn-success");
			      	}	              
	            }else{
	              alert("<?=$this->lang->line('product/viewList.alert.error')?>");
	            }
	          }
	        });      	

      	console.log($(this).text());
     
      	return false;
      });
});	

</script>



<a id="addBtn" class="btn btn-success pull-right" href="<?=base_url()?>product/productForm" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span><?=$this->lang->line('product/viewList.btn.add')?></a>
<div class="panel panel-info">
  <!-- Default panel contents -->
  <div class="panel-heading"><?=$this->lang->line('product/viewList.title')?></div>
  <div class="panel-body bg-info">
  <form action="<?=base_url()?>product/search" method="POST">
	  	<!-- search text-->
		<div class="input-group">
		  <input id="searchInput" type="text" class="form-control" name="search" placeholder="<?=$this->lang->line('product/viewList.search.placeholder')?>" required>
		  <span class="input-group-btn">
		    <button class="btn btn-default" type="submit" href="#"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
		    <a class="btn btn-default dropdown-toggle" type="button" href="#" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
	        <ul class="dropdown-menu dropdown-menu-right" role="menu">
	          <li><a href="#" id="showCataArea"><?=$this->lang->line('product/viewList.btn.showCataArea')?></a></li>
	        </ul>	    
		  </span>
		</div><!-- /input-group -->
		<!-- search catagory-->
		<div id="search_cata_area">
			<div class="form-group">
			   <label for="inputBrand" class="col-sm-2 control-label"><?=$this->lang->line('Category.iCampus')?></label>
			   <div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iLearning" autocomplete="off"><?=$this->lang->line('Category.iCampus.iLearning')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iManagement" autocomplete="off"><?=$this->lang->line('Category.iCampus.iManagement')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iGovernance" autocomplete="off"><?=$this->lang->line('Category.iCampus.iGovernance')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iSocial" autocomplete="off"><?=$this->lang->line('Category.iCampus.iSocial')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iHealth" autocomplete="off"><?=$this->lang->line('Category.iCampus.iHealth')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iGreen" autocomplete="off"><?=$this->lang->line('Category.iCampus.iGreen')?>
				  </label>	
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.iCampus.others')?>
				  </label>					  				  					  					  
				</div>			      
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputBrand" class="col-sm-2 control-label"><?=$this->lang->line('Category.product')?></label>
		    <div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
				  <label class="btn btn-default">
				    <input type="checkbox" name="product_category[]" value="Total Solution" autocomplete="off"><?=$this->lang->line('Category.product.total_solution')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="product_category[]" value="Service" autocomplete="off"><?=$this->lang->line('Category.product.service')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="product_category[]" value="Software" autocomplete="off"><?=$this->lang->line('Category.product.software')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="product_category[]" value="Hardware" autocomplete="off"><?=$this->lang->line('Category.product.hardware')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="product_category[]" value="Digital Content" autocomplete="off"><?=$this->lang->line('Category.product.digital_content')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="product_category[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.product.others')?>
				  </label>					  				  					  					  
				</div>	
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputBrand" class="col-sm-2 control-label"><?=$this->lang->line('Category.eduLevel')?></label>
		    <div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Kindergarden" autocomplete="off"><?=$this->lang->line('Category.eduLevel.kindergarden')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Elementary School" autocomplete="off"><?=$this->lang->line('Category.eduLevel.elementary')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="High School" autocomplete="off"><?=$this->lang->line('Category.eduLevel.high_school')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Higher Education" autocomplete="off"><?=$this->lang->line('Category.eduLevel.higher_education')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Vocational Training" autocomplete="off"><?=$this->lang->line('Category.eduLevel.vocational')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.eduLevel.others')?>
				  </label>					  				  					  					  
				</div>
		    </div>
		  </div>
		</div>
	</form>	
  </div>
<?
if(empty($list)){
?>
	<div class="alert alert-warning" role="alert"><?=$this->lang->line('product/viewList.notice.noProducts')?></div>
<?
}else{
?>  
  <!-- Table -->
 	<table class="table">
      <thead>
        <tr>
          <th><?=$this->lang->line('product/viewList.table.column.order')?></th>
          <th><?=$this->lang->line('product/viewList.table.column.img')?></th>
          <th><?=$this->lang->line('product/viewList.table.column.name')?></th>
          <th><?=$this->lang->line('product/viewList.table.column.brand')?></th>
          <th><?=$this->lang->line('product/viewList.table.column.description')?></th>
          <th><?=$this->lang->line('product/viewList.table.column.isVerified')?></th>
          <th><?=$this->lang->line('product/viewList.table.column.uploader')?></th>
          <th><?=$this->lang->line('product/viewList.table.column.actions')?></th>
        </tr>
      </thead>
      <tbody>
<?
$i=0;
foreach ($list as $row) {
	$arrayImgs = explode("|", $row->smallImagefiles);
	$img = $arrayImgs[0];
?> 
 
        <tr>
          <th scope="row"><?=$offset+(++$i)?></th>
          <td><img src="<?=base_url()?>upload/product/<?=$img?>" width="100"></td>
          <td><?=$row->name?></td>
          <td><?=$row->brand?></td>
          <td><?=character_limiter($row->description, 20)?></td>
          <th>
<?if( 
	$this->session->userdata('account')["role"] == "si"  ||
    $this->session->userdata('account')["role"] == "root"
){?>          
			<a  pid="<?=$row->pid?>" class="btn btn-<?=($row->isVerified?"success":"danger")?> verifyBtn" href="#" role="button"><?=($row->isVerified?$this->lang->line('product/viewList.isVerified.yes'):$this->lang->line('product/viewList.isVerified.no'))?></a>
<?}else{?>
          	<span class="label label-<?=($row->isVerified?"success":"danger")?>"><?=($row->isVerified?$this->lang->line('product/viewList.isVerified.yes'):$this->lang->line('product/viewList.isVerified.no'))?></span>
<?}?>          	
          </th>
          <td><?=$row->userid?></td>
          <td>
 
          	<a class="btn btn-primary modifyBtn" href="<?=base_url()?>product/view/<?=$row->pid?>" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><?=$this->lang->line('product/viewList.btn.detail')?></a>
<?if( 
	$this->session->userdata('account')["role"] == "si"  ||
    $this->session->userdata('account')["role"] == "root"
){?> 			
 			<a class="btn btn-warning modifyBtn" href="<?=base_url()?>product/modifyForm/<?=$row->pid?>" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><?=$this->lang->line('product/viewList.btn.modify')?></a>
        	<a class="btn btn-danger removeBtn" href="#" role="button" pid="<?=$row->pid?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><?=$this->lang->line('product/viewList.btn.remove')?></a>          
<?}?>
          </td>           
        </tr>
<?}//foreach
}//if empty
?>
      </tbody>
    </table>
<?if(!empty($links)){?>     
    <div class="panel-footer text-center"><?=$links?></div>
<?}//empty links?>    
</div>
    
<!DOCTYPE html>
<html>
<head>
	<title><?=$this->lang->line('head.title')?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="<?=base_url()?>bootstrap/css/bootstrap.css" rel="stylesheet">
  	<link href="<?=base_url()?>css/custom.css" rel="stylesheet">
	<script src="<?=base_url()?>js/jquery-1.11.2.js"></script>
	<script src="<?=base_url()?>js/jquery.validate.min.js"></script>
	<script src="<?=base_url()?>js/jquery.form.min.js"></script>
	<script src="<?=base_url()?>bootstrap/js/bootstrap.min.js"></script>
  <script src="<?=base_url();?>js/plupload/plupload.full.js"></script>
</head>
<body>


    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=base_url()?>"><?=$this->lang->line('head.title')?></a>
        </div>
<?if(
	$this->session->userdata('logged_in')== true &&
	($this->session->userdata('account')["role"] == "root" || $this->session->userdata('account')["role"] == "si")
){?>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <?=$this->lang->line('head.accountAdmin')?> <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">             
                <li><?=anchor('account/listRoleAccount?role=vendor', $this->lang->line('head.accountAdmin.vender'), '');?></li>
                <li><?=anchor('account/listRoleAccount?role=buyer', $this->lang->line('head.accountAdmin.buyer'), '');?></li>
<?if($this->session->userdata('account')["role"] == "root"){//只有root可以作si以上的管理?>                
                <li><?=anchor('account/listRoleAccount?role=si', $this->lang->line('head.accountAdmin.si'), '');?></li>
                <li><?=anchor('account/listRoleAccount?role=root', $this->lang->line('head.accountAdmin.root'), '');?></li> 
<?}?>                               
              </ul>
            </li>
          </ul>
<?}?>

<?//si 商品審核權限
if(
  $this->session->userdata('logged_in')== true && 
    (
      $this->session->userdata('account')["role"] == "si"  ||
      $this->session->userdata('account')["role"] == "root"
    )
  )
{?> 
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <?=$this->lang->line('head.products')?> <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu"> 
                <li><a href="<?=base_url()?>product/review"><?=$this->lang->line('head.products.review')?> <span class="badge"><?=$this->session->userdata('needVerfyNum')?></span></a></li>                                                                  
              </ul>
            </li>
          </ul>
<?}?>

<?//ventor權限
if(
  $this->session->userdata('logged_in')== true && 
  $this->session->userdata('account')["role"] == "vendor"
	)
{?> 
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <?=$this->lang->line('head.actions')?> <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu"> 
                <li><?=anchor('product/productForm', $this->lang->line('head.actions.add'), '');?></li>                          
                <li><?=anchor('product/viewList', $this->lang->line('head.actions.products'), '');?></li>                                        
              </ul>
            </li>
          </ul>
<?}?>

<?//buyer權限
if(
  $this->session->userdata('logged_in')== true && 
  $this->session->userdata('account')["role"] == "buyer"
  )
{?> 
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <?=$this->lang->line('head.actions')?> <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">                   
                <li><?=anchor('buyer/viewList', $this->lang->line('head.actions.solutions'), '');?></li>                                        
              </ul>
            </li>
          </ul>
<?}?>
<?if($this->session->userdata('logged_in')== true &&
  ($this->session->userdata('account')["role"] == "root" || $this->session->userdata('account')["role"] == "si")
  )
{?> 
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <?=$this->lang->line('head.total_solution')?> <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu"> 
                <li><?=anchor('solution/form', $this->lang->line('head.total_solution.add_new'), '');?></li>                                                         
                <li><?=anchor('solution/viewList', $this->lang->line('head.total_solution.list'), '');?></li>                              
              </ul>
            </li>
          </ul>
<?}?>


 <div id="navbar" class="collapse navbar-collapse navbar-right">
           <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <span class="glyphicon glyphicon-globe" aria-hidden="true"></span><?=$this->lang->line('language_setting')?><span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?=base_url()?>account/locale/zh-TW"><?=$this->lang->line('language_zhtw')?></a></li>
                <li><a href="<?=base_url()?>account/locale/english"><?=$this->lang->line('language_en')?></a></li>            
              </ul>
            </li>
          </ul>
<?if($this->session->userdata('logged_in')==""){?>
<!--
          <form class="navbar-form navbar-right" action="<?=base_url()?>account/login" method="POST">
            <div class="form-group">
              <input type="text" placeholder="userid" name="userid" class="form-control"  size="12">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" name="password" class="form-control" size="12">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
-->
<?}else{?>
       
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
              <?=$this->session->userdata('account')["contact_name"]?> <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?=base_url()?>account/editForm"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><?=$this->lang->line('head.account.edit')?></a></li>
                <li><a href="<?=base_url()?>account/logout"> <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span><?=$this->lang->line('head.account.logout')?></a></li>            
              </ul>
            </li>
          </ul>
<?}?>


        </div><!--/.navbar-collapse navbar-right-->
      </div>
    </nav>
<div class="container">




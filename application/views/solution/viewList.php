<script type="text/javascript">
$( document ).ready(function() {
	var search_json = <?=(empty($search_json)?"null":$search_json)?>;
	if(search_json!=null){
		$("#searchInput").val(search_json.search);
		var icampusArr = search_json.icampus_category;
		var levelArr = search_json.level;
      	for(var i =0 ;i< icampusArr.length ; i++){
      		var item = $("input[name='icampus_category[]'][value='"+icampusArr[i]+"']");
      		item.attr('checked', 1);
      		item.parent().addClass("active");
      	}


      	for(var i =0 ;i< levelArr.length ; i++){
      		var item = $("input[name='level[]'][value='"+levelArr[i]+"']");
      		item.attr('checked', 1);
      		item.parent().addClass("active");
      	} 

	    // if(levelArr!=false || icampusArr!=false){
	    // 	$("#search_cata_area").show();
	    // }
	}



      $('.removeBtn').click(function() {
        var sid = $(this).attr("sid");
        if(confirm("<?=$this->lang->line('solution/viewList.alert.remove')?>"))
        {
          $.ajax({
             type: 'GET',
             url: "<?=base_url()?>solution/remove/"+sid,
             success: function(data){
                location.reload(); 
             },
             error: function(jqXHR, textStatus, errorThrown) {
                  // report error
                  alert("ERROR:"+textStatus);
                  alert(errorThrown);
              }
          });//ajax
        }//if(confirm)
      }); //.removeBtn

      $('#showCataArea').click(function() {
      	$("#search_cata_area").slideToggle('slow');
      });	

      $(".verifyBtn").click(function() {
      	var sid = $(this).attr("sid");
      	var btn = $(this);
      	var isVerify = ($(this).text() == "Yes")?"0":"1";
	      var dataString = {sid:sid ,isVerify:isVerify};
	        console.log(dataString);
	        $.ajax({
	          type: "POST",
	          url: "<?=base_url()?>solution/verify",
	          data: dataString,
	          cache: false,
	          success: function(data){
	            console.log(data);
	            if(data=="OK"){
	              alert("changed");
			 		if(btn.text() == "Yes"){
			      		btn.text("No");
			      		btn.removeClass("btn-success");
			      		btn.addClass("btn-danger");
			      	}else{
			      		btn.text("Yes");
			      		btn.removeClass("btn-danger");
			      		btn.addClass("btn-success");
			      	}	              
	            }else{
	              alert("something error...");
	            }
	          }
	        });      	

      	console.log($(this).text());
     
      	return false;
      });
});	

</script>



<a id="addBtn" class="btn btn-success pull-right" href="<?=base_url()?>solution/form" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span><?=$this->lang->line('solution/viewList.btn.add')?></a>
<div class="panel panel-info">
  <!-- Default panel contents -->
  <div class="panel-heading"><?=$this->lang->line('solution/viewList.heading')?></div>
  <div class="panel-body bg-info">
  <form action="<?=base_url()?>solution/search" method="POST">
	  	<!-- search text-->
		<div class="input-group">
		  <input id="searchInput" type="text" class="form-control" name="search" placeholder="<?=$this->lang->line('solution/viewList.search.placeholder')?>">
		  <span class="input-group-btn">
		    <button class="btn btn-default" type="submit" href="#"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
		    <a class="btn btn-default dropdown-toggle" type="button" href="#" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
	        <ul class="dropdown-menu dropdown-menu-right" role="menu">
	          <li><a href="#" id="showCataArea"><?=$this->lang->line('solution/viewList.btn.showCataArea')?></a></li>
	          <li><a href="<?=base_url().'solution/viewList'?>" ><?=$this->lang->line('solution/viewList.btn.list')?></a></li>
	        </ul>	    
		  </span>
		</div><!-- /input-group -->
		<!-- search catagory-->
		<div id="search_cata_area">
			<div class="form-group">
			   <label for="inputBrand" class="col-sm-2 control-label"><?=$this->lang->line('Category.iCampus')?></label>
			   <div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iLearning" autocomplete="off"><?=$this->lang->line('Category.iCampus.iLearning')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iManagement" autocomplete="off"><?=$this->lang->line('Category.iCampus.iManagement')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iGovernance" autocomplete="off"><?=$this->lang->line('Category.iCampus.iGovernance')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iSocial" autocomplete="off"><?=$this->lang->line('Category.iCampus.iSocial')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iHealth" autocomplete="off"><?=$this->lang->line('Category.iCampus.iHealth')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iGreen" autocomplete="off"><?=$this->lang->line('Category.iCampus.iGreen')?>
				  </label>	
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.iCampus.others')?>
				  </label>					  				  					  					  
				</div>			      
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputBrand" class="col-sm-2 control-label"><?=$this->lang->line('Category.eduLevel')?></label>
		    <div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Kindergarden" autocomplete="off"><?=$this->lang->line('Category.eduLevel.kindergarden')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Elementary School" autocomplete="off"><?=$this->lang->line('Category.eduLevel.elementary')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="High School" autocomplete="off"><?=$this->lang->line('Category.eduLevel.high_school')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Higher Education" autocomplete="off"><?=$this->lang->line('Category.eduLevel.higher_education')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Vocational Training" autocomplete="off"><?=$this->lang->line('Category.eduLevel.vocational')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.eduLevel.others')?>
				  </label>					  				  					  					  
				</div>
		    </div>
		  </div>
		</div>
	</form>	
  </div>
<?
if(empty($list)){
?>
	<div class="alert alert-warning" role="alert">You have no Solutions!</div>
<?
}else{
?>  
  <!-- Table -->
 	<table class="table">
      <thead>
        <tr>
          <th><?=$this->lang->line('solution/viewList.table.column.order')?></th>
          <th><?=$this->lang->line('Solution.name')?></th>
          <th><?=$this->lang->line('Solution.introduction')?></th>
          <th><?=$this->lang->line('Solution.description')?></th>
<?if($this->session->userdata('account')["role"] == "root"){?>          
          <th><?=$this->lang->line('solution/viewList.table.column.creator')?></th>
<?}?>          
          <th><?=$this->lang->line('solution/viewList.table.column.actions')?></th>
        </tr>
      </thead>
      <tbody>
<?
$i=0;
foreach ($list as $row) {
?> 
 
        <tr>
          <th scope="row"><?=$offset+(++$i)?></th>
          <td><?=$row->name?></td>
          <td><?=character_limiter($row->introduction,20)?></td>
          <td><?=character_limiter($row->description, 20)?></td>

<?if($this->session->userdata('account')["role"] == "root"){?>
			<td><?=$row->userid?></td>
<?}?>
        <td>
          	<a class="btn btn-primary detailBtn" href="<?=base_url()?>solution/detail/<?=$row->sid?>" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><?=$this->lang->line('solution/viewList.btn.detail')?></a> 			 			
          	<a class="btn btn-warning settingBtn" href="<?=base_url()?>solution/gotoAuthority?sid=<?=$row->sid?>" role="button"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span><?=$this->lang->line('solution/viewList.btn.setting')?></a>
 			<a class="btn btn-warning modifyBtn" href="<?=base_url()?>solution/modifyForm/<?=$row->sid?>" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><?=$this->lang->line('solution/viewList.btn.modify')?></a>
 			<a class="btn btn-default pdfBtn" href="<?=base_url()?>pdfPrint/pdf/<?=$row->sid?>" role="button"><span class="glyphicon glyphicon-print" aria-hidden="true"></span><?=$this->lang->line('solution/viewList.btn.pdf')?></a>
        	<a class="btn btn-danger removeBtn" href="#" role="button" sid="<?=$row->sid?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><?=$this->lang->line('solution/viewList.btn.remove')?></a>          
          </td>           
        </tr>
<?}//foreach
}//if empty
?>
      </tbody>
    </table>
<?if(!empty($links)){?>     
    <div class="panel-footer text-center"><?=$links?></div>
<?}//empty links?>    
</div>
    
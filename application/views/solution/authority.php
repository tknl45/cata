<script type="text/javascript">
$( document ).ready(function() {
<?
  if(empty($solution->isActive)!=true){
?>
$("input[name='isActive']").attr('checked', 0);
$("input[name='isActive']").parent().removeClass("active");

$("input[name='isActive'][value=<?=$solution->isActive?>]").attr('checked', 1);
$("input[name='isActive'][value=<?=$solution->isActive?>]").parent().addClass("active");
<?    
  }//if($solution->isActive!=null)
?>
<?
  if(empty($solution->isShowPrice)!=true){
?>
$("input[name='isShowPrice']").attr('checked', 0);
$("input[name='isShowPrice']").parent().removeClass("active");

$("input[name='isShowPrice'][value=<?=$solution->isShowPrice?>]").attr('checked', 1);
$("input[name='isShowPrice'][value=<?=$solution->isShowPrice?>]").parent().addClass("active");
<?    
  }//if($solution->isActive!=null)
?>
      $("#save").click(function(){
          var str='';

          $("#right-select option").each(function () {
            str +=  ','+$(this).val();
          });
          str = str.substr(1); // first coma

          var dataString = {sid:<?=$sid?> ,list:str, isActive:$('input[name=isActive]:checked').val(), isShowPrice:$('input[name=isShowPrice]:checked').val()};
            //alert(str);
            $.ajax({
              type: "POST",
              url: "<?=base_url()?>solution/saveAuthority",
              data: dataString,
              cache: false,
              success: function(data){
                console.log(data);
                if(data=="OK"){
                  alert("<?=$this->lang->line('solution/gotoAuthority.alert.saveOK')?>");
                }else{
                  alert("<?=$this->lang->line('solution/gotoAuthority.alert.saveFail')?>");
                }
              }
            });

            return false;        
      });

});//ready


function go_right(){
  $("#left-select option:selected").each(function () {
        $('#right-select').append('<option value="'+ $(this).val() +'">' + $(this).text() + '</option>');
        $(this).remove();
      });
}

function go_left(){
  $("#right-select option:selected").each(function () {
      $('#left-select').append('<option value="'+ $(this).val() +'">' + $(this).text() + '</option>');
      $(this).remove();
    });
}
</script>
<style type="text/css">
  select {
  width: 220px;
  background-color: #ffffff;
  border: 1px solid #cccccc;
}
</style>


<h3 class="text-center">
	<span class="label label-default"><?=$this->lang->line('Solution.step.new')?></span>▶
	<span class="label label-default"><?=$this->lang->line('Solution.step.add')?></span>▶
	<span class="label label-default"><?=$this->lang->line('Solution.step.view')?></span>▶
	<span class="label label-primary"><?=$this->lang->line('Solution.step.auth')?></span>
</h3>
<div class="panel panel-info">
  <!-- Default panel contents -->
  <div class="panel-heading"><?=$this->lang->line('solution/gotoAuthority.heading')?></div>
  <div class="panel-body">
  		<form>
		  <div class="form-group">
		    <label for="inputName" class="col-sm-2 control-label"><?=$this->lang->line('solution/gotoAuthority.isActive')?></label>
		    <div class="col-sm-10">
          <div class="btn-group" data-toggle="buttons">
            <label class="btn btn-default">
              <input type="radio" name="isActive" value="1" autocomplete="off"><?=$this->lang->line('solution/gotoAuthority.btn.yes')?>
            </label>
            <label class="btn btn-default active">
              <input type="radio" name="isActive" value="0" autocomplete="off" checked><?=$this->lang->line('solution/gotoAuthority.btn.no')?>
            </label>                                            
          </div>		      
		    </div>
		  </div>
      <div class="form-group">
        <label for="inputName" class="col-sm-2 control-label"><?=$this->lang->line('solution/gotoAuthority.isShowPrice')?></label>
        <div class="col-sm-10">
          <div class="btn-group" data-toggle="buttons">
            <label class="btn btn-default">
              <input type="radio" name="isShowPrice" value="1" autocomplete="off"><?=$this->lang->line('solution/gotoAuthority.btn.yes')?>
            </label>
            <label class="btn btn-default active">
              <input type="radio" name="isShowPrice" value="0" autocomplete="off" checked><?=$this->lang->line('solution/gotoAuthority.btn.no')?>
            </label>                                            
          </div>          
        </div>
      </div>      
		  <div class="form-group">
		    <label for="inputNotes" class="col-sm-2 control-label"><?=$this->lang->line('solution/gotoAuthority.customerAuth')?></label>
		    <div class="col-sm-10">
            <div class="col-md-3">
                <h4><?=$this->lang->line('solution/gotoAuthority.customerAuth.customerList')?></h4>
                <select size="20" multiple id="left-select">
<?
$result=$buyerList;
if($assign_buyers!=null){
  $result=array_diff($buyerList,$assign_buyers);
}//if($assign_buyers!=null)
foreach($result as $buyer){?>                
                    <option value="<?=$buyer->userid?>"><?=$buyer->contact_name?></option>
<?}//foreach

?>                                 
                </select>
            </div>
            <div class="col-md-1">
              <br/><br/><br/><br/>
              <button type="button" class="btn btn-default" onClick="go_right();">>></button>
              <button type="button" class="btn btn-default" onClick="go_left();"> <<</button>
            </div>
            <div class="col-md-3">
                <h4><?=$this->lang->line('solution/gotoAuthority.customerAuth.viewerList')?></h4>
                <select size="20" name="viewList" multiple id="right-select">
<?
if($assign_buyers != null){

foreach($assign_buyers as $assign_buyer){?>                
                    <option value="<?=$assign_buyer->userid?>"><?=$assign_buyer->contact_name?></option>
<?}//foreach  
}//if
?>                  
                </select>
            </div>		       
		    </div>
		  </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <a href="<?=base_url()?>solution/viewList" class="btn btn-default"><?=$this->lang->line('solution/gotoAuthority.btn.list')?></a>
          <button type="submit" id="save" class="btn btn-success"><?=$this->lang->line('solution/gotoAuthority.btn.save')?></button>
        </div>
      </div>      
		</form>		   
  </div>
</div>
    
<script type="text/javascript">
$( document ).ready(function() {
	var selectNum = <?if($this->session->userdata("solutionArr")==null){echo "0";}else{echo sizeof($this->session->userdata("solutionArr"));}?>; //選擇多少數量


	var search_json = <?=(empty($search_json)?"null":$search_json)?>;
	if(search_json!=null){
		$("#searchInput").val(search_json.search);
		var icampusArr = search_json.icampus_category;
		var productArr = search_json.product_category;
		var levelArr = search_json.level;
		if(icampusArr!=false){
	      	for(var i =0 ;i< icampusArr.length ; i++){
	      		var item = $("input[name='icampus_category[]'][value='"+icampusArr[i]+"']");
	      		item.attr('checked', 1);
	      		item.parent().addClass("active");
	      	}			
		}

		if(productArr!=false){
	      	for(var i =0 ;i< productArr.length ; i++){
	      		var item = $("input[name='product_category[]'][value='"+productArr[i]+"']");
	      		item.attr('checked', 1);
	      		item.parent().addClass("active");
	      	}
	    }
		if(levelArr!=false){	    
	      	for(var i =0 ;i< levelArr.length ; i++){
	      		var item = $("input[name='level[]'][value='"+levelArr[i]+"']");
	      		item.attr('checked', 1);
	      		item.parent().addClass("active");
	      	} 
	    }
	    // if(levelArr!=false || productArr!=false || icampusArr!=false){
	    // 	$("#search_cata_area").show();
	    // }
	    	
	}



      $('#showCataArea').click(function() {
      	$("#search_cata_area").slideToggle('slow');
      });	
      $(".toogleProductBtn").click(function() {
      		var pid = $(this).attr("pid");
      		console.log(pid);
      		var btn=$(this);
      		//call session


      		if($(this).text()=="<?=$this->lang->line('solution/addProduct.btn.addInto')?>"){
				$.get( "<?=base_url()?>solution/toogleProductPid/add/"+pid+"?d="+(new Date()).getTime(), function(data) {
					console.log(data);
					if(data==1){
		      			showSelectNum(1);
		      			btn.html('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span><?=$this->lang->line('solution/addProduct.btn.delFrom')?>');
		      			btn.removeClass("btn-success");
		      			btn.addClass("btn-danger");						
					}else{
						alert( "ERROR:pid is exist" );
					}
				})
				.fail(function() {
					alert( "ERROR:connection fail" );
				})


      		}else{
				$.get( "<?=base_url()?>solution/toogleProductPid/remove/"+pid+"?d="+(new Date()).getTime(), function(data) {
					console.log(data);
					if(data==1){
		      			showSelectNum(-1);
		      			btn.html('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span><?=$this->lang->line('solution/addProduct.btn.addInto')?>');
		      			btn.removeClass("btn-danger");
		      			btn.addClass("btn-success");   					
					}else{
						alert( "ERROR:pid not exist" );
					}
				})
				.fail(function() {
					alert( "ERROR:connection fail" );
				})
   			
      		}

      });	


	function showSelectNum(offset){
		selectNum += offset;
		$("code").text(selectNum);
	} 

	$("#sendBtn").click(function(){
		if($("#searchInput").val()==""){
			location.href="addProduct";
			return false;
		}
	});     
});	



</script>



<h3 class="text-center">
	<span class="label label-default"><?=$this->lang->line('Solution.step.new')?></span>▶
	<span class="label label-primary"><?=$this->lang->line('Solution.step.add')?></span>▶
	<span class="label label-default"><?=$this->lang->line('Solution.step.view')?></span>▶
	<span class="label label-default"><?=$this->lang->line('Solution.step.auth')?></span>
</h3>
<div class="panel panel-info">
  <!-- Default panel contents -->
  <div class="panel-heading"><?=$this->lang->line('solution/addProduct.searchHeading_start')?><code><?if($this->session->userdata("solutionArr")==null){echo "0";}else{echo sizeof($this->session->userdata("solutionArr"));}?></code> <?=$this->lang->line('solution/addProduct.searchHeading_end')?>
	<a class="btn btn-success pull-right" href="<?=base_url()?>solution/viewSolution" role="button"><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span><?=$this->lang->line('solution/addProduct.btn.preview')?></a>
  </div>
  <div class="panel-body bg-info">
  <form action="<?=base_url()?>solution/product_search" method="POST">
	  	<!-- search text-->
		<div class="input-group">
		  <input id="searchInput" type="text" class="form-control" name="search" placeholder="<?=$this->lang->line('solution/addProduct.search.placeholder')?>">
		  <span class="input-group-btn">
		    <button class="btn btn-default" type="submit" href="#" id="sendBtn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
		    <a class="btn btn-default dropdown-toggle" type="button" href="#" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
	        <ul class="dropdown-menu dropdown-menu-right" role="menu">
	          <li><a href="#" id="showCataArea"><?=$this->lang->line('solution/addProduct.btn.showCataArea')?></a></li>
	          <li><a href="<?=base_url()?>solution/addProduct"><?=$this->lang->line('solution/addProduct.btn.list')?></a></li>
	        </ul>	    
		  </span>
		</div><!-- /input-group -->
		<!-- search catagory-->
		<div id="search_cata_area">
			<div class="form-group">
			   <label for="inputBrand" class="col-sm-2 control-label"><?=$this->lang->line('Category.iCampus')?></label>
			   <div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iLearning" autocomplete="off"><?=$this->lang->line('Category.iCampus.iLearning')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iManagement" autocomplete="off"><?=$this->lang->line('Category.iCampus.iManagement')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iGovernance" autocomplete="off"><?=$this->lang->line('Category.iCampus.iGovernance')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iSocial" autocomplete="off"><?=$this->lang->line('Category.iCampus.iSocial')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iHealth" autocomplete="off"><?=$this->lang->line('Category.iCampus.iHealth')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iGreen" autocomplete="off"><?=$this->lang->line('Category.iCampus.iGreen')?>
				  </label>	
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.iCampus.others')?>
				  </label>					  				  					  					  
				</div>			      
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputBrand" class="col-sm-2 control-label"><?=$this->lang->line('Category.product')?></label>
		    <div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
				  <label class="btn btn-default">
				    <input type="checkbox" name="product_category[]" value="Total Solution" autocomplete="off"><?=$this->lang->line('Category.product.total_solution')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="product_category[]" value="Service" autocomplete="off"><?=$this->lang->line('Category.product.service')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="product_category[]" value="Software" autocomplete="off"><?=$this->lang->line('Category.product.software')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="product_category[]" value="Hardware" autocomplete="off"><?=$this->lang->line('Category.product.hardware')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="product_category[]" value="Digital Content" autocomplete="off"><?=$this->lang->line('Category.product.digital_content')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="product_category[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.product.others')?>
				  </label>					  				  					  					  
				</div>	
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputBrand" class="col-sm-2 control-label"><?=$this->lang->line('Category.eduLevel')?></label>
		    <div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Kindergarden" autocomplete="off"><?=$this->lang->line('Category.eduLevel.kindergarden')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Elementary School" autocomplete="off"><?=$this->lang->line('Category.eduLevel.elementary')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="High School" autocomplete="off"><?=$this->lang->line('Category.eduLevel.high_school')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Higher Education" autocomplete="off"><?=$this->lang->line('Category.eduLevel.higher_education')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Vocational Training" autocomplete="off"><?=$this->lang->line('Category.eduLevel.vocational')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.eduLevel.others')?>
				  </label>					  				  					  					  
				</div>
		    </div>
		  </div>
		</div>
	</form>	
  </div>

<?
if(empty($list)){
?>
	<div class="alert alert-warning" role="alert">Please search products and add to total souction!</div>
<?
}else{
?>  
  <!-- Table -->
 	<table class="table">
      <thead>
        <tr>
          <th><?=$this->lang->line('solution/addProduct.table.column.order')?></th>
          <th><?=$this->lang->line('solution/addProduct.table.column.img')?></th>
          <th><?=$this->lang->line('solution/addProduct.table.column.name')?></th>
          <th><?=$this->lang->line('solution/addProduct.table.column.brand')?></th>
          <th><?=$this->lang->line('solution/addProduct.table.column.description')?></th>
          <th><?=$this->lang->line('solution/addProduct.table.column.isVerified')?></th>
          <th><?=$this->lang->line('solution/addProduct.table.column.uploader')?></th>
          <th><?=$this->lang->line('solution/addProduct.table.column.actions')?></th>
        </tr>
      </thead>
      <tbody>
<?
$i=0;
foreach ($list as $row) {
	$arrayImgs = explode("|", $row->smallImagefiles);
	$img = $arrayImgs[0];
?> 
 
        <tr>
          <th scope="row"><?=$offset+(++$i)?></th>
          <td><img src="<?=base_url()?>upload/product/<?=$img?>" width="100"></td>
          <td><?=$row->name?></td>
          <td><?=$row->brand?></td>
          <td><?=character_limiter($row->description, 20)?></td>
          <th><span class="label label-<?=($row->isVerified?"success":"danger")?>"><?=($row->isVerified?$this->lang->line('solution/addProduct.isVerified.yes'):$this->lang->line('solution/addProduct.isVerified.no'))?></span></th>
          <td><?=$row->userid?></td>
          <td>          	
          	<a class="btn btn-primary viewBtn" target="_blank" href="<?=base_url()?>product/view/<?=$row->pid?>" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><?=$this->lang->line('solution/addProduct.btn.detail')?></a>
          	<?
          	if($this->session->userdata("solutionArr")!="" && in_array($row->pid,$this->session->userdata("solutionArr"))){?>	
          		<a class="btn btn-danger toogleProductBtn" href="#" pid="<?=$row->pid?>" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><?=$this->lang->line('solution/addProduct.btn.delFrom')?></a>	
          	<?}else{?>
          		<a class="btn btn-success toogleProductBtn" href="#" pid="<?=$row->pid?>" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span><?=$this->lang->line('solution/addProduct.btn.addInto')?></a>
          	<?}?>
          </td>           
        </tr>
<?}//foreach
}//if empty
?>
      </tbody>
    </table>
<?if(!empty($links)){?>  
    <div class="panel-footer text-center"><?=$links?></div>
<?}?>    
</div>
    
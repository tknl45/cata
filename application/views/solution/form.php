<script type="text/javascript">
$( document ).ready(function() {
<?
if($this->session->userdata("solution_add")!=null){
?>
		var json = <?=json_encode($this->session->userdata('solution_add'),JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)?>
		//var icampus_category="<?=$this->session->userdata('solution_add')->icampus_category?>";
      	var icampusArr = json.icampus_category.split(",");
      	for(var i =0 ;i< icampusArr.length ; i++){
      		var item = $("input[name='icampus_category[]'][value='"+icampusArr[i]+"']");
      		item.attr('checked', 1);
      		item.parent().addClass("active");
      	}
      	//var level="<?=$this->session->userdata('solution_add')->level?>";
      	var levelArr = json.level.split(",");
      	for(var i =0 ;i< levelArr.length ; i++){
      		var item = $("input[name='level[]'][value='"+levelArr[i]+"']");
      		item.attr('checked', 1);
      		item.parent().addClass("active");
      	}

      	$("#inputName").val(json.name); 
      	$("#inputIntroduction").text(json.introduction); 
      	$("#inputDescription").text(json.description); 
      	$("#inputNotes").text(json.notes); 
<?
}//if
?>
      $("#productForm").validate({ 
      		errorElement: 'div',            
            rules:{
              name:{
                required:true
              },
              'icampus_category[]':{
                required:true
              },
              'level[]':{
                required:true
              },
              introduction:{
                required:true
              },
			  description:{
                required:true
			  }
            },//end of rule
            messages:{
              name:{
                required:"This field is required."
              },
              'icampus_category[]':{
                required:"Please select something!"
              },
              'level[]':{
                required:"Please select something!"
              },
              introduction:{
                required:"This field is required."
              },
			  description:{
                required:"This field is required."
			  }
            }//end of messages
          
          });//end of validate()

});
</script>


<a id="listBtn" class="btn btn-default" href="<?=base_url()?>solution/viewList" role="button"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><?=$this->lang->line('solution/form.btn.list')?></a>
<h3 class="text-center">
	<span class="label label-primary"><?=$this->lang->line('Solution.step.new')?></span>▶
	<span class="label label-default"><?=$this->lang->line('Solution.step.add')?></span>▶
	<span class="label label-default"><?=$this->lang->line('Solution.step.view')?></span>▶
	<span class="label label-default"><?=$this->lang->line('Solution.step.auth')?></span>
</h3>
<div class="panel panel-default">
	<div class="panel-heading"><?=$this->lang->line('solution/form.heading')?></div>
  	<div class="panel-body">
		<form id="productForm" class="form-horizontal" method="POST" action="<?=base_url()?>solution/addProduct">
		  <div class="form-group">
		    <label for="inputBrand" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Category.iCampus')?>*</label>
		    <div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iLearning" autocomplete="off"><?=$this->lang->line('Category.iCampus.iLearning')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iManagement" autocomplete="off"><?=$this->lang->line('Category.iCampus.iManagement')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iGovernance" autocomplete="off"><?=$this->lang->line('Category.iCampus.iGovernance')?>  
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iSocial" autocomplete="off"><?=$this->lang->line('Category.iCampus.iSocial')?>  
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iHealth" autocomplete="off"><?=$this->lang->line('Category.iCampus.iHealth')?>  
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="iGreen" autocomplete="off"><?=$this->lang->line('Category.iCampus.iGreen')?>
				  </label>	
				  <label class="btn btn-default">
				    <input type="checkbox" name="icampus_category[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.iCampus.others')?>
				  </label>					  				  					  					  
				</div>			      
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputBrand" class="col-sm-2 control-label text-danger"><?=$this->lang->line('Category.eduLevel')?>*</label>
		    <div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Kindergarden" autocomplete="off"><?=$this->lang->line('Category.eduLevel.kindergarden')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Elementary School" autocomplete="off"><?=$this->lang->line('Category.eduLevel.elementary')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="High School" autocomplete="off"><?=$this->lang->line('Category.eduLevel.high_school')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Higher Education" autocomplete="off"><?=$this->lang->line('Category.eduLevel.higher_education')?> 
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Vocational Training" autocomplete="off"><?=$this->lang->line('Category.eduLevel.vocational')?>
				  </label>
				  <label class="btn btn-default">
				    <input type="checkbox" name="level[]" value="Others" autocomplete="off"><?=$this->lang->line('Category.eduLevel.others')?>
				  </label>					  				  					  					  
				</div>
		    </div>
		  </div>		  		  
		  <div class="form-group">
		    <label for="inputName" class="col-sm-2 control-label text-danger"><?=$this->lang->line('solution/form.name')?>*</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="name" id="inputName" placeholder="" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputIntroduction" class="col-sm-2 control-label text-danger"><?=$this->lang->line('solution/form.introduction')?>*</label>
		    <div class="col-sm-10">
		      <textarea rows="3" class="form-control" name="introduction" id="inputIntroduction" placeholder="" required></textarea>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputDescription" class="col-sm-2 control-label text-danger"><?=$this->lang->line('solution/form.description')?>*</label>
		    <div class="col-sm-10">
		      <textarea rows="3" class="form-control" name="description" id="inputDescription" placeholder="" required></textarea>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputNotes" class="col-sm-2 control-label"><?=$this->lang->line('solution/form.notes')?></label>
		    <div class="col-sm-10">
		      <textarea rows="3" class="form-control" name="notes" id="inputNotes"></textarea>
		    </div>
		  </div>		  
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span><?=$this->lang->line('solution/form.btn.create')?></button>
		    </div>
		  </div>
		</form>
  	</div>
</div>

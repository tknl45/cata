<script type="text/javascript">
$( document ).ready(function() {
	$(".replyBtn").click(function(){
		console.log("replyBtn");

		var cid=$(this).attr("cid");
		$.post( "<?=base_url()?>comment/replyComment/"+cid,{feedback: $(".feedback[cid="+cid+"]").val()} , function() {
		  	alert( "success" );
		  	//location.reload();
		})
		.fail(function() {
		    alert( "error" );
		});		
	});//$(".replyBtn")
});	//ready



</script>



<a class="btn btn-default" href="<?=base_url()?>solution/viewList" role="button"><span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span><?=$this->lang->line('solution/detail.btn.list')?></a>
<div class="panel panel-info">
  <!-- Default panel contents -->
  <div class="panel-heading"><?=$this->lang->line('solution/detail.heading')?></div>
  <div class="panel-body">
  		<form>
		  <div class="form-group">
		    <label for="inputName" class="col-sm-2 control-label"><?=$this->lang->line('Category.iCampus')?></label>
		    <div class="col-sm-10">
		      <pre><?=$solution->icampus_category?></pre>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputName" class="col-sm-2 control-label"><?=$this->lang->line('Category.eduLevel')?></label>
		    <div class="col-sm-10">
		      <pre><?=$solution->level?></pre>
		    </div>
		  </div>		    		
		  <div class="form-group">
		    <label for="inputName" class="col-sm-2 control-label"><?=$this->lang->line('Solution.name')?></label>
		    <div class="col-sm-10">
		      <pre><?=$solution->name?></pre>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputIntroduction" class="col-sm-2 control-label"><?=$this->lang->line('Solution.introduction')?></label>
		    <div class="col-sm-10">
		       <pre><?=$solution->introduction?></pre>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputDescription" class="col-sm-2 control-label"><?=$this->lang->line('Solution.description')?></label>
		    <div class="col-sm-10">
		       <pre><?=$solution->description?></pre>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputNotes" class="col-sm-2 control-label"><?=$this->lang->line('Solution.notes')?></label>
		    <div class="col-sm-10">
		       <pre><?=$solution->notes?></pre>
		    </div>
		  </div>
		</form>		   
  </div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#products" aria-controls="products" role="tab" data-toggle="tab"><?=$this->lang->line('solution/detail.tab.product')?></a></li>
    <li role="presentation"><a href="#comment" aria-controls="comment" role="tab" data-toggle="tab"><?=$this->lang->line('solution/detail.tab.comment')?></a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="products">
<?
if(empty($list)){
?>
	<div class="alert alert-warning" role="alert"><?=$this->lang->line('solution/detail.noProducts')?></div>
<?
}else{
?>  
  <!-- Table -->
 	<table class="table">
      <thead>
        <tr>
          <th><?=$this->lang->line('solution/detail.table.column.order')?></th>
          <th><?=$this->lang->line('solution/detail.table.column.img')?></th>
          <th><?=$this->lang->line('solution/detail.table.column.name')?></th>
          <th><?=$this->lang->line('solution/detail.table.column.brand')?></th>
          <th><?=$this->lang->line('solution/detail.table.column.description')?></th>
          <th><?=$this->lang->line('solution/detail.table.column.actions')?></th>
        </tr>
      </thead>
      <tbody>
<?
$i=0;
foreach ($list as $row) {
	$arrayImgs = explode("|", $row->smallImagefiles);
	$img = $arrayImgs[0];
?> 
 
        <tr>
          <input type="hidden" value="<?=$row->pid?>">
          <th scope="row"><?=(++$i)?></th>
          <td><img src="<?=base_url()?>upload/product/<?=$img?>" width="100"></td>
          <td><?=$row->name?></td>
          <td><?=$row->brand?></td>
          <td><?=character_limiter($row->description, 20)?></td>         
          <td>
          	<a class="btn btn-primary viewBtn" target="_blank" href="<?=base_url()?>product/view/<?=$row->pid?>" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><?=$this->lang->line('solution/detail.btn.detail')?></a>
          </td>           
        </tr>
<?}//foreach
}//if empty
?>
      </tbody>
    </table>

	</div><!--<div role="tabpanel" class="tab-pane active" id="products"> -->
    <div role="tabpanel" class="tab-pane" id="comment">
        

	<div class="actionBox">
<?
if(empty($comments)){
?>
	<div class="alert alert-warning" role="alert"><?=$this->lang->line('solution/detail.noComments')?></div>
<?
}else{
foreach ($comments as $row) {
	if(empty($row->profileImg)){
		$row->profileImg = "defaultProfile.jpg";
	}
?>	
        <ul class="commentList">
            <li>
                <div class="commenterImage">                
                  <img src="<?=base_url()?>upload/profile/<?=$row->profileImg?>" />
                </div>
                <div class="commentText">
                    <p class=""><?=$this->typography->auto_typography($row->content)?></p> <span class="date sub-text">on <?=$row->timestamp?></span>

                </div>               
            </li>

        </ul>
	            <div class="form-group">
				    <div class="input-group">
				      <textarea class="form-control feedback" rows="1" cid="<?=$row->cid?>"><?=$row->feedback?></textarea>
				      <span class="input-group-btn">
				        <button class="btn btn-success replyBtn" cid="<?=$row->cid?>">Reply</button>
				      </span>
				    </div><!-- /input-group --> 
	            </div>         
<?}//foreach
}//if empty
?>              
    </div><!--<div role="tabpanel" class="tab-pane" id="comment">-->
  </div><!-- <div class="tab-content">-->

</div><!-- <div role="tabpanel">-->



      </tbody>
    </table>
 

  
</div>
    
<script type="text/javascript">
$( document ).ready(function() {
    //往上移動
    $(".upBtn").click(function(){
    	var row = $(this).closest('tr');
    	var prev = row.prev();
    	console.log(prev);
    	if(prev.length == 0){
    		alert("<?=$this->lang->line('solution/viewSolution.alert.top')?>");
    	}else{
    		row.prev().insertAfter(row);
    	}
    	return false;
    	
    });

    //往下移動
    $(".dnBtn").click(function(){
     	var row = $(this).closest('tr');
    	var next = row.next();
    	if(next.length == 0){
    		alert("<?=$this->lang->line('solution/viewSolution.alert.bottom')?>");
    	}else{
    		row.next().insertBefore(row);
    	}
    	return false;
    });

    //確認完成
    $("#nextBtn").click(function(){
    	var pidArr = [];
		$( "input" ).each(function( index ) {
		  //console.log( index + ": " + $( this ).val() );
		  pidArr.push($( this ).val());
		});   	
		var pids = pidArr.join(",");

		$.post( "<?=base_url()?>solution/saveData",{ "pids": pids }, function(data) {
			//if(data == 1){
				alert( "<?=$this->lang->line('solution/viewSolution.alert.saveOK')?>" );
				location.href="<?=base_url()?>solution/gotoAuthority?sid="+data;
			//}
		  
		  //
		})
		.fail(function() {
		    alert( "<?=$this->lang->line('solution/viewSolution.alert.saveFail')?>" );
		})
	

    	return false;
    });
});	



</script>



<h3 class="text-center">
  <span class="label label-default"><?=$this->lang->line('Solution.step.new')?></span>▶
  <span class="label label-default"><?=$this->lang->line('Solution.step.add')?></span>▶
  <span class="label label-primary"><?=$this->lang->line('Solution.step.view')?></span>▶
  <span class="label label-default"><?=$this->lang->line('Solution.step.auth')?></span>
</h3>
<a class="btn btn-default" href="<?=base_url()?>solution/search" role="button"><span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span><?=$this->lang->line('solution/viewSolution.btn.addMore')?></a>
<a class="btn btn-success pull-right" id="nextBtn" href="#" role="button"><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span><?=$this->lang->line('solution/viewSolution.btn.finish')?></a>
<div class="panel panel-info">
  <!-- Default panel contents -->
  <div class="panel-heading"><?=$this->lang->line('solution/viewSolution.heading')?>
	
  </div>
  <div class="panel-body">
  		<form>
		  <div class="form-group">
		    <label for="inputName" class="col-sm-2 control-label"><?=$this->lang->line('Category.iCampus')?></label>
		    <div class="col-sm-10">
		      <pre><?=$this->session->userdata('solution_add')->icampus_category?></pre>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputName" class="col-sm-2 control-label"><?=$this->lang->line('Category.eduLevel')?></label>
		    <div class="col-sm-10">
		      <pre><?=$this->session->userdata('solution_add')->level?></pre>
		    </div>
		  </div>		    		
		  <div class="form-group">
		    <label for="inputName" class="col-sm-2 control-label"><?=$this->lang->line('Solution.name')?></label>
		    <div class="col-sm-10">
		      <pre><?=$this->session->userdata('solution_add')->name?></pre>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputIntroduction" class="col-sm-2 control-label"><?=$this->lang->line('Solution.introduction')?></label>
		    <div class="col-sm-10">
		       <pre><?=$this->session->userdata('solution_add')->introduction?></pre>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputDescription" class="col-sm-2 control-label"><?=$this->lang->line('Solution.description')?></label>
		    <div class="col-sm-10">
		       <pre><?=$this->session->userdata('solution_add')->description?></pre>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputNotes" class="col-sm-2 control-label"><?=$this->lang->line('Solution.notes')?></label>
		    <div class="col-sm-10">
		       <pre><?=$this->session->userdata('solution_add')->notes?></pre>
		    </div>
		  </div>
		</form>		   
  </div>

<?
if(empty($list)){
?>
	<div class="alert alert-warning" role="alert">Please search products and add to total souction!</div>
<?
}else{
?>  
  <!-- Table -->
 	<table class="table">
      <thead>
        <tr>
          <th><?=$this->lang->line('solution/addProduct.table.column.order')?></th>
          <th><?=$this->lang->line('solution/addProduct.table.column.img')?></th>
          <th><?=$this->lang->line('solution/addProduct.table.column.name')?></th>
          <th><?=$this->lang->line('solution/addProduct.table.column.brand')?></th>
          <th><?=$this->lang->line('solution/addProduct.table.column.description')?></th>
          <th><?=$this->lang->line('solution/addProduct.table.column.isVerified')?></th>
          <th>UP/DOWN</th>
          <th><?=$this->lang->line('solution/addProduct.table.column.actions')?></th>
        </tr>
      </thead>
      <tbody>
<?
$i=0;
foreach ($list as $row) {
	$arrayImgs = explode("|", $row->smallImagefiles);
	$img = $arrayImgs[0];
?> 
 
        <tr>
          <input type="hidden" value="<?=$row->pid?>">
          <th scope="row"><?=(++$i)?></th>
          <td><img src="<?=base_url()?>upload/product/<?=$img?>" width="100"></td>
          <td><?=$row->name?></td>
          <td><?=$row->brand?></td>
          <td><?=character_limiter($row->description, 20)?></td>
          <th><span class="label label-<?=($row->isVerified?"success":"danger")?>"><?=($row->isVerified?"Yes":"No")?></span></th>
          <td>
          	<a class="btn btn-default upBtn" href="#" role="button"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
          	<BR>
          	<a class="btn btn-default dnBtn" href="#" role="button"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
          </td>
          <td>
          	<a class="btn btn-primary viewBtn" target="_blank" href="<?=base_url()?>product/view/<?=$row->pid?>" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><?=$this->lang->line('solution/addProduct.btn.detail')?></a>
          	<a class="btn btn-danger toogleProductBtn" href="#" pid="<?=$row->pid?>" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><?=$this->lang->line('solution/addProduct.btn.delFrom')?></a>	
          </td>           
        </tr>
<?}//foreach
}//if empty
?>
      </tbody>
    </table>
 

  
</div>
    
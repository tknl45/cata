 <?php
class PdfPrint extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->model('buyer_model');
		$this->load->model('account_model');
		$this->load->model('solution_model');
		$this->load->model('comment_model');        
        ob_end_clean();
    }
    
    function pdf($sid){
    	$solution = $this->solution_model->getSolution($sid);
    	if($solution == null){
    		die("No find!");
    	}

    	$solutionArr = explode(",",$solution->pids);
    	$list = $this->solution_model->solutionList($solutionArr);

    	//init page
    	$this->load->library('Pdf');
    	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetFont('cid0jp', '', 9); // 可以顯示中文(繁、簡)、日文、韓文。
		$pdf->setHeaderFont(array('cid0jp', '', 10));
		$contactInfo = "Contact name:".$this->session->userdata('account')["contact_name"];
		$contactInfo .= " / Tel:".$this->session->userdata('account')["tel"];
		$contactInfo .= " / Email:".$this->session->userdata('account')["email"];
		$pdf->SetHeaderData(null, null,"Total Soulution:".$solution->name, $contactInfo);
		$pdf->setFooterMargin(10); 
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));

    	$msg  = "<h1>$solution->name</h1>";
    	$msg .= "<p><strong>Description:</strong>$solution->description</p>";
    	$msg .= "<p><strong>Introduction:</strong>$solution->introduction</p>";
    	$msg .= "<p><strong>Notes:</strong>$solution->notes</p>";

    	
		$pdf->AddPage();
		//$pdf->writeHTML($msg, true, 0, true, 0);

		$msg .="<H3>Products List:</H3>";
    	if(empty($list)){
    		$msg = "No solutions";
    	}else{
    		 	$msg  .= '<table cellspacing="0" cellpadding="1" border="1">';
			       $msg  .= "<tr>";
			          $msg  .= '<th width="15">#</th>';
			          //$msg  .= "<th>Images</th>";
			          $msg  .= '<th width="100">Name</th>';
			          $msg  .= '<th width="100">Brands</th>';
			          $msg  .= '<th width="300">Description</th>';
			        $msg  .= "</tr>";
  			$i=0;
			foreach ($list as $row) {
				$arrayImgs = explode("|", $row->smallImagefiles);
				$img = $arrayImgs[0];				
		        $msg  .= "<tr>";
		          $msg  .= "<th scope='row'>".(++$i)."</th>";
		          //$msg  .= '<td><img src="'.base_url().'upload/product/'.$img.'" width="50"></td>';
		          $msg  .= "<td>$row->name</td>";
		          $msg  .= "<td>$row->brand</td>";
		          $msg  .= "<td>".$row->description."</td>";             
		        $msg  .= "</tr>";			
			}
			$msg.="</table>";
    	}


    	
    	$pdf->writeHTML($msg, true, false, false, false, '');


//列印各產品目錄

    	foreach ($list as $row) {
    		$html="";
    		$html .= '<h1>Product Name:'.$row->name.'</h1>';
			$arrayImgs = explode("|", $row->smallImagefiles);
			//$img = $arrayImgs[0];				
			foreach ($arrayImgs as $img) {
				$html  .= ' <img src="'.base_url().'upload/product/'.$img.'" width="80" height="80">';
			}
    		
    		$html  .= ' <br>';

    		$arrayLargerImgs = explode("|", $row->largeImagefiles);
    		//foreach ($arrayLargerImgs as $img) {
			//	$html  .= ' <img src="'.base_url().'upload/product/'.$img.'" width="800" height="300">';
			//}
			$html  .= ' <img src="'.base_url().'upload/product/'.$arrayLargerImgs[0].'" width="800" height="300">';
			$html  .= ' <br>';

    		$html .= '<table cellspacing="0" cellpadding="1" border="1">';
	    		$html .= '<tr><th width="150">Brands / Manufacturer</th><td width="380">'.$row->brand.'</td></tr>';
	    		$html .= '<tr><th width="150">Product Model</th><td width="380">'.$row->model.'</td></tr>';
	    		$html .= '<tr><th width="150">iCampus Category</th><td width="380">'.$row->icampus_category.'</td></tr>';
	    		$html .= '<tr><th width="150">Product Category</th><td width="380">'.$row->product_category.'</td></tr>';
	    		$html .= '<tr><th width="150">Level</th><td width="380">'.$row->level.'</td></tr>';
	    		$html .= '<tr><th width="150">Product Introduction</th><td width="380">'.$row->introduction.'</td></tr>';
	    		$html .= '<tr><th width="150">Function Description</th><td width="380">'.$row->description.'</td></tr>';
	    		$html .= '<tr><th width="150">Technological Spec</th><td width="380">'.$row->techSpec.'</td></tr>';
	    		$html .= '<tr><th width="150">Product Homepage</th><td width="380">'.$row->homepage.'</td></tr>';
	    		$html .= '<tr><th width="150">Warranty and After-Service</th><td width="380">'.$row->warranty.'</td></tr>';
	    		$html .= '<tr><th width="150">Installation and Deployment</th><td width="380">'.$row->deployment.'</td></tr>';
	    		$html .= '<tr><th width="150">Training Service</th><td width="380">'.$row->training.'</td></tr>';
	    		$html .= '<tr><th width="150">Unit Price</th><td width="380">'.$row->unitPrice_si.'</td></tr>';
	    		$html .= '<tr><th width="150">Batch Prices</th><td width="380">'.$row->batchPrices_si.'</td></tr>';
	    		$html .= '<tr><th width="150">Prices Detail and Notes</th><td width="380">'.$row->pricesDetail_si.'</td></tr>';
	    		$html .= '<tr><th width="150">Contact</th><td width="380">'.$row->contact_name.'</td></tr>';
	    		$html .= '<tr><th width="150">Company Name</th><td width="380">'.$row->company_name.'</td></tr>';
	    		$html .= '<tr><th width="150">Company Website</th><td width="380">'.$row->company_site.'</td></tr>';
	    		$html .= '<tr><th width="150">Tel</th><td width="380">'.$row->tel.'</td></tr>';
	    		$html .= '<tr><th width="150">Email</th><td width="380">'.$row->email.'</td></tr>';
	    		$html .= '<tr><th width="150">Other Product Information / Memo</th><td width="380">'.$row->memo.'</td></tr>';
    		$html .= '</table>';

    		$pdf->AddPage();

    		//die($html);
    		$pdf->writeHTML($html, true, 0, true, 0);
    	}

    	$pdf->Output('example_001.pdf', 'I');


	}




    function test()
    {
$this->load->library('Pdf'); 
$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false); 
$pdf->SetTitle('Pdf Example'); 
//$pdf->SetHeaderMargin(30); 
//$pdf->SetTopMargin(20); 
$pdf->setFooterMargin(20); 
$pdf->SetAutoPageBreak(true); 
$pdf->SetAuthor('Author'); 
$pdf->SetDisplayMode('real', 'default'); 
$pdf->Write(5, 'CodeIgniter TCPDF Integration'); 
$pdf->Output('pdfexample.pdf', 'I');
    }
}
?> 
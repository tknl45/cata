<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class buyer extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('buyer_model');
		$this->load->model('account_model');
		$this->load->model('solution_model');
		$this->load->model('comment_model');
	}



	//搜尋
	public function search($offset=0){

        $query = null;
        if($this->input->post('search') != null ){

        	$query = array(
	        	'search' => $this->input->post('search'), 
	      		'icampus_category' => $this->input->post('icampus_category'),
	      		'level' => $this->input->post('level')
        	);        	
    		$newdata = array('buyer_search_query'  => $query);
           	$this->session->set_userdata($newdata);
        }else{
        	$query = $this->session->userdata('buyer_search_query');
        }

       
        $data['search_json']  = json_encode($query,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);;       
		
		//分頁
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] =  base_url() .'buyer/viewList';
        $config['per_page'] = 10;
        $config['total_rows'] = $this->buyer_model->viewList_search_total_rows($query) ;
        $this->pagination->initialize($config);

        $data["offset"] = $offset;
        $data["links"] = $this->pagination->create_links();	
        $this->session->set_userdata(array('config'  => $config));

        $list = $this->buyer_model->viewList_search($query,$offset,$config['per_page']);
		$data['list']  = $list;	

		$this->theme->load('buyer/viewList',$data);                
	}





	//觀看頁面
	public function detail($sid){
		$solution = $this->solution_model->getSolution($sid);
		$data["sid"] = $sid;
		
		$this->session->set_userdata(array('isShowPrice'  =>$solution->isShowPrice));


		$data["solution"] = $solution;
		$data["comments"] = $this->comment_model->getComments($sid);

		//die($buyer->pids);
		$solutionArr = explode(",",$solution->pids);
		//die(print_r($buyerArr));
		$list = $this->solution_model->solutionList($solutionArr);
		$data['list']  = $list;	
		$this->theme->load('buyer/detail',$data);
	}

	
	


	//show list data
	public function viewList($offset=0){
        //分頁
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] =  base_url() .'buyer/viewList';
        $config['total_rows'] = $this->buyer_model->viewList_search_total_rows(null);        
        $config['per_page'] = 10 ;
      

        $this->pagination->initialize($config);
        $data["offset"] = $offset;
        $data["links"] = $this->pagination->create_links();

		$list = $this->buyer_model->viewList_search(null,$offset,$config['per_page']);
		$data['list']  = $list;        
	  	$this->theme->load('buyer/viewList',$data);
	}



}

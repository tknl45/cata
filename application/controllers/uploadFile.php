<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UploadFile extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();		
		
	}
	//上傳
	public function uploadImages()
	{
		$uploadDir=APPPATH."../upload/images/";	//define upload dir
			//generate unique file name to upload
			$fullName=$this->input->post('fileName');	//e.g. boo1.xls
			$fileId=$this->input->post('fileId');		//unique id generated by pluploader
			$path=pathinfo($fullName);
			$file=$fileId.'.'.$path['extension'];  //e.g. book1_id.xls
			$name=$_FILES['file']['name'];
			$uploadFile=$uploadDir.$file;
			
			//check directory existance
			if(!file_exists(dirname($uploadFile)))
				mkdir(dirname($uploadFile), 0777, true);
				
			//move uploaded file
			move_uploaded_file($_FILES['file']['tmp_name'], $uploadFile);

			echo $file;	
	}
	//上傳到product 文件夾
	public function uploadProduct()
	{
		$uploadDir=APPPATH."../upload/product/";	//define upload dir
			//generate unique file name to upload
			$fullName=$this->input->post('fileName');	//e.g. boo1.xls
			$fileId=$this->input->post('fileId');		//unique id generated by pluploader
			$path=pathinfo($fullName);
			$file=$fileId.'.'.$path['extension'];  //e.g. book1_id.xls
			$name=$_FILES['file']['name'];
			$uploadFile=$uploadDir.$file;
			
			//check directory existance
			if(!file_exists(dirname($uploadFile)))
				mkdir(dirname($uploadFile), 0777, true);
				
			//move uploaded file
			move_uploaded_file($_FILES['file']['tmp_name'], $uploadFile);

			echo $file;	
	}


	//上傳到profile文件夾
	public function uploadProfileImage()
	{
		$uploadDir=APPPATH."../upload/profile/";	//define upload dir
			//generate unique file name to upload
			$fullName=$this->input->post('fileName');	//e.g. boo1.xls
			$fileId=$this->input->post('fileId');		//unique id generated by pluploader
			$path=pathinfo($fullName);
			$file=$fileId.'.'.$path['extension'];  //e.g. book1_id.xls
			$name=$_FILES['file']['name'];
			$uploadFile=$uploadDir.$file;
			
			//check directory existance
			if(!file_exists(dirname($uploadFile)))
				mkdir(dirname($uploadFile), 0777, true);
				
			//move uploaded file
			move_uploaded_file($_FILES['file']['tmp_name'], $uploadFile);

			echo $file;	
	}
	function cropImage($dir){
		//fetch input
		$src=$this->security->xss_clean($this->input->post('src'));
		$crop_x=$this->security->xss_clean($this->input->post('crop_x'));
		$crop_y=$this->security->xss_clean($this->input->post('crop_y'));
		$crop_width=$this->security->xss_clean($this->input->post('crop_width'));
		$crop_height=$this->security->xss_clean($this->input->post('crop_height'));

		//sourse image
		$fullPath=APPPATH."../upload/$dir/".$src;

		//die($fullPath);
		//check image extension
		$ext=pathinfo($src,PATHINFO_EXTENSION);
		//write_file('c:/124.txt',$ext);
		$sourceImg;
		switch(strtolower($ext)){
			case "jpg":
				$sourceImg=imagecreatefromjpeg($fullPath);
				break;
			case "jpeg":
				$sourceImg=imagecreatefromjpeg($fullPath);
				break;
			case "gif":
				$sourceImg=imagecreatefromgif($fullPath);
				break;
			case "png":
				$sourceImg=imagecreatefrompng($fullPath);
				break;
		}
		//add cropped image
		$croppedImg=imagecreatetruecolor($crop_width, $crop_height);
		imagecopyresampled($croppedImg, $sourceImg, 0, 0, $crop_x, $crop_y, $crop_width, $crop_height, $crop_width, $crop_height);
		
		switch(strtolower($ext)){
			case "jpg":
				imagejpeg($croppedImg, $fullPath, 90);
				break;
			case "jpeg":
				imagejpeg($croppedImg, $fullPath, 90);
				break;
			case "gif":
				imagegif($croppedImg, $fullPath);
				break;
			case "png":
				imagepng($croppedImg, $fullPath, 9);
				break;
		}
		echo "OK";
	}	
}

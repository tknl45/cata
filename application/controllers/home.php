<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$data = array('a' => 123 );
		//die("123");
		if($this->session->userdata('account')==null){
			$this->theme->load('welcome',$data);

		}else if($this->session->userdata('account')['role']=="root"){
       		$this->theme->load('login_view');
       	}else if($this->session->userdata('account')['role']=="si"){
       		$this->theme->load('login_view');
       	}else if($this->session->userdata('account')['role']=="vendor"){
       		redirect('product/viewList');
       	}else{
       		redirect('buyer/viewList');
       	}		
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Solution extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('solution_model');
		$this->load->model('account_model');
		$this->load->model('comment_model');
	}
	//新增頁面
	public function form(){
		$this->session->unset_userdata('solution_add');
		$this->session->unset_userdata('solutionArr');
		
		$this->theme->load('solution/form');
	}

	//商品搜尋
	public function product_search($offset=0){

        $query = null;
        if($this->input->post('search') != null ){

        	$query = array(
	        	'search' => $this->input->post('search'), 
	        	'product_category' => $this->input->post('product_category'),
	      		'icampus_category' => $this->input->post('icampus_category'),
	      		'level' => $this->input->post('level')
        	);        	
    		$newdata = array('solution_search_query'  => $query);
           	$this->session->set_userdata($newdata);
        }else{
        	$query = $this->session->userdata('solution_search_query');
        }

       
        $data['search_json']  = json_encode($query,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);;       
		
		//分頁
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] =  base_url() .'solution/product_search';
        $config['per_page'] = 10;
        $config['total_rows'] = $this->solution_model->searchList_total_rows($query) ;
        $this->pagination->initialize($config);

        $data["offset"] = $offset;
        $data["links"] = $this->pagination->create_links();	
        $this->session->set_userdata(array('config'  => $config));

        $list = $this->solution_model->searchList($query,$offset,$config['per_page']);
		$data['list']  = $list;	

		$this->theme->load('solution/addProduct',$data);                
	}
	//搜尋
	public function search($offset=0){

        $query = null;
        if($this->input->post('search') != null ){

        	$query = array(
	        	'search' => $this->input->post('search'), 
	      		'icampus_category' => $this->input->post('icampus_category'),
	      		'level' => $this->input->post('level')
        	);        	
    		$newdata = array('solution_search_query'  => $query);
           	$this->session->set_userdata($newdata);
        }else{
        	$query = $this->session->userdata('solution_search_query');
        }

       
        $data['search_json']  = json_encode($query,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);;       
		
		//分頁
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] =  base_url() .'solution/viewList';
        $config['per_page'] = 10;
        $config['total_rows'] = $this->solution_model->viewList_search_total_rows($query) ;
        $this->pagination->initialize($config);

        $data["offset"] = $offset;
        $data["links"] = $this->pagination->create_links();	
        $this->session->set_userdata(array('config'  => $config));

        $list = $this->solution_model->viewList_search($query,$offset,$config['per_page']);
		$data['list']  = $list;	

		$this->theme->load('solution/viewList',$data);                
	}





	//進入產品搜尋
	public function addProduct($offset=0){
		if($this->input->post('name') == ""){

		}else{
			

			if($this->session->userdata("solution_add")!=null){
				$this->session->userdata("solution_add")->name = $this->input->post('name');
				$this->session->userdata("solution_add")->icampus_category = $this->input->post('icampus_category');
				$this->session->userdata("solution_add")->introduction = $this->input->post('introduction');
				$this->session->userdata("solution_add")->description = $this->input->post('description');
				$this->session->userdata("solution_add")->notes = $this->input->post('notes');
				$this->session->userdata("solution_add")->icampus_category = implode (",", $this->input->post('icampus_category'));
				$this->session->userdata("solution_add")->level = implode (",", $this->input->post('level'));
			}else{
				$solution_add = array(
				'name' => $this->input->post('name'),
				'icampus_category' => implode (",", $this->input->post('icampus_category')),
	      		'level' => implode (",", $this->input->post('level')),
	      		'introduction' => $this->input->post('introduction'),
	      		'description' => $this->input->post('description'),
	      		'notes' => $this->input->post('notes')
				);
				//temp to session
    			$newdata = array('solution_add'  => (object)$solution_add);
           		$this->session->set_userdata($newdata);					
			}


					
		}



	//分頁
	        $this->load->library('pagination');
	        $config = array();
	        $config['base_url'] =  base_url() .'solution/search';
	        $config['per_page'] = 10;
	        $config['total_rows'] = $this->solution_model->searchList_total_rows(null) ;
	        $this->pagination->initialize($config);

	        $data["offset"] = $offset;
	        $data["links"] = $this->pagination->create_links();	
	        $this->session->set_userdata(array('config'  => $config));

	        $list = $this->solution_model->searchList(null,$offset,$config['per_page']);
			$data['list']  = $list;	

			$this->theme->load('solution/addProduct',$data); 


			//$this->theme->load('solution/addProduct');
	}

	//寫入產品選擇
	public function toogleProductPid($method,$pid){
		$result = 0;
		$solutionArr = $this->session->userdata("solutionArr");
		if(empty($solutionArr)){
			$this->session->set_userdata(array('solutionArr'  => array()));
		}

		$solutionArr = $this->session->userdata("solutionArr");
		
		$isExist = in_array($pid, $solutionArr);
		if($isExist && $method == "remove"){
			$solutionArr = array_diff($solutionArr, array($pid));
			$this->session->set_userdata(array('solutionArr'  => $solutionArr));
			$result = 1;			
		}else if(!$isExist && $method == "add"){
			array_push($solutionArr,$pid);
			$this->session->set_userdata(array('solutionArr'  => $solutionArr));
			$result = 1;	
		}
		echo $result;
	}

	//檢視解決方案
	public function viewSolution(){
		$solutionArr = $this->session->userdata("solutionArr");
        $list = $this->solution_model->solutionList($solutionArr);
		$data['list']  = $list;	
		$this->theme->load('solution/viewSolution',$data);		
	}

	//儲存資料
	public function saveData(){
		$solution = $this->session->userdata('solution_add');
		$pids = $this->input->post('pids');
		$solution->pids = $pids;
		$solution->userid = $this->session->userdata('account')["userid"];
		if(empty($solution->sid)){
			$id = $this->solution_model->add($solution);
			echo $id;
		}else{
			$this->solution_model->modify($solution);
			echo $solution->sid;
		}
		
		

		//echo "1";
		//print_r($solution);
		//die();
	}

	//權限設定
	public function gotoAuthority(){
		$sid = $this->input->get('sid');
		$data["sid"] = $sid; 
		if($sid != null){
			$data["assign_buyers"] =$this->solution_model->assign_buyers($sid);
		}
		$data["solution"] = $this->solution_model->getSolution($sid);
		$data["buyerList"] = $this->account_model->buyerList();
		

		$this->theme->load('solution/authority',$data);
	}

	//儲存可觀看之採購商
	public function saveAuthority(){
		$sid =  $this->input->post('sid');
		$isActive =  $this->input->post('isActive');
		$isShowPrice =  $this->input->post('isShowPrice');
        $data = array(
                       'isActive' => $isActive,
                       'isShowPrice' => $isShowPrice
                    );		
        $this->solution_model->setSolutionData($sid,$data);
		//清除
		$this->solution_model->clearAssign_buyers($sid);

		$list =  $this->input->post('list');
		if($list==""){

			echo "OK"; 
			return;
		}

		$array= explode(',', $list);
		$datas = array();
		foreach ($array as $item) {
			if($item==""){continue;}
			$data = array(
			'sid' => $sid,
			'userid' => $item
			);

			array_push($datas, $data);
		}
		//die(print_r($datas));
		//die(sizeof($datas));

		$this->solution_model->saveAssign_buyers($datas);
		echo "OK";
	}//savePersmssion

	//觀看頁面
	public function detail($sid){
		$solution = $this->solution_model->getSolution($sid);
		$data["sid"] = $sid;
		$data["solution"] = $solution;
		$data["comments"] = $this->comment_model->getComments($sid);
		//die($solution->pids);
		$solutionArr = explode(",",$solution->pids);
		//die(print_r($solutionArr));
		$list = $this->solution_model->solutionList($solutionArr);
		$data['list']  = $list;	
		$this->theme->load('solution/detail',$data);
	}

	//刪除
	public function remove($sid){
		$this->solution_model->remove($sid);
	}	

	//修改頁面
	public function modifyForm($sid){
		$data["solution"] =  $this->solution_model->getSolution($sid);
		$this->session->set_userdata(array('solution_add'  => $data["solution"]));
		$this->session->set_userdata(array('solutionArr'  => explode( ",",$data["solution"]->pids)));
		$this->theme->load('solution/form',$data);
	}

	//-------------改到這-------------

	//修改帳號
	public function modify($sid){
			date_default_timezone_set('Asia/Taipei');
			$datestring = "%Y-%m-%d %H:%i:%s";
			$time = time();
			$createDate = mdate($datestring, $time);

		$sid = $this->input->post('pid');
		$data = array(
			'name' => $this->input->post('name'),
      		'brand' => $this->input->post('brand'),
      		'model' => $this->input->post('model'),
      		'icampus_category' => implode (",", $this->input->post('icampus_category')),
			'solution_category' => implode (",", $this->input->post('solution_category')),
      		'level' => implode (",", $this->input->post('level')),
      		'introduction' => $this->input->post('introduction'),
      		'description' => $this->input->post('description'),
      		'techSpec' => $this->input->post('techSpec'),
      		'youtubeURL' => $this->input->post('youtubeURL'),
      		'homepage' => $this->input->post('homepage'),
			'warranty' => $this->input->post('warranty'),
      		'deployment' => $this->input->post('deployment'),
			'training' => $this->input->post('training'),
			'unitPrice' => $this->input->post('unitPrice'),
      		'batchPrices' => $this->input->post('batchPrices'),
			'pricesDetail' => $this->input->post('pricesDetail'),
      		'contact_name' => $this->input->post('contact_name'),
      		'company_site' => $this->input->post('company_site'),
      		'company_name' => $this->input->post('company_name'),
			'tel' => $this->input->post('tel'),
			'email' => $this->input->post('email'),      
			'memo' => $this->input->post('memo'),
			'userid' => $this->session->userdata('account')['userid'],
			'createDate' => $createDate,
			'docfilenames' => $this->input->post('docfilenames'),
			'docfiles' => $this->input->post('docfiles'),
			'largeImagefilenames' => $this->input->post('largeImagefilenames'),
			'largeImagefiles' => $this->input->post('largeImagefiles'),
			'smallImagefilenames' => $this->input->post('smallImagefilenames'),
			'smallImagefiles' => $this->input->post('smallImagefiles')
			);

	
		
		$this->solution_model->modify($sid,$data);
	}





	//show list data
	public function viewList($offset=0){
        //分頁
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] =  base_url() .'solution/viewList';
        $config['total_rows'] = $this->solution_model->viewList_search_total_rows(null);        
        $config['per_page'] = 10 ;
      

        $this->pagination->initialize($config);
        $data["offset"] = $offset;
        $data["links"] = $this->pagination->create_links();

		$list = $this->solution_model->viewList_search(null,$offset,$config['per_page']);
		$data['list']  = $list;        
	  	$this->theme->load('solution/viewList',$data);
	}



}

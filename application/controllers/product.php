<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('product_model');

		
	}
	//新增頁面
	public function productForm(){
		$data["product_json"] = "null";
		$this->theme->load('product/productForm',$data);
	}
	//修改頁面
	public function modifyForm($pid){
		$product = $this->product_model->getOne($pid);
		$data["product_json"] = json_encode($product,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		$this->theme->load('product/productForm',$data);
	}
	//觀看頁面
	public function view($pid){
		$product = $this->product_model->getOne($pid);

		if($this->session->userdata('account')["role"] != "buyer"){
			$this->session->set_userdata(array('isShowPrice'  =>1));
		}
		//判斷是否秀價錢
		if($this->session->userdata('isShowPrice')==0){
			$product->unitPrice="";
			$product->batchPrices="";
			$product->pricesDetail="";
			$product->unitPrice_si="";
			$product->batchPrices_si="";
			$product->pricesDetail_si="";
		}

		$data["pid"] = $pid;
		$data["product_json"] = json_encode($product,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		$this->theme->load('product/view',$data);
	}





	//新增
	public function add(){
			date_default_timezone_set('Asia/Taipei');
			$datestring = "%Y-%m-%d %H:%i:%s";
			$time = time();
			$createDate = mdate($datestring, $time);


		$data = array(
			'name' => $this->input->post('name'),
      		'brand' => $this->input->post('brand'),
      		'model' => $this->input->post('model'),
      		'icampus_category' => implode (",", $this->input->post('icampus_category')),
      		'batchPrices' => $this->input->post('batchPrices'),
			'product_category' => implode (",", $this->input->post('product_category')),
      		'level' => implode (",", $this->input->post('level')),
      		'introduction' => $this->input->post('introduction'),
      		'description' => $this->input->post('description'),
      		'techSpec' => $this->input->post('techSpec'),
      		'youtubeURL' => $this->input->post('youtubeURL'),
      		'homepage' => $this->input->post('homepage'),
			'warranty' => $this->input->post('warranty'),
      		'deployment' => $this->input->post('deployment'),
			'training' => $this->input->post('training'),
			'unitPrice' => $this->input->post('unitPrice'),
      		'batchPrices' => $this->input->post('batchPrices'),
			'pricesDetail' => $this->input->post('pricesDetail'),
			'unitPrice_si' => $this->input->post('unitPrice'),
      		'batchPrices_si' => $this->input->post('batchPrices'),
			'pricesDetail_si' => $this->input->post('pricesDetail'),			
      		'contact_name' => $this->input->post('contact_name'),
      		'company_site' => $this->input->post('company_site'),
      		'company_name' => $this->input->post('company_name'),
			'tel' => $this->input->post('tel'),
			'email' => $this->input->post('email'),      
			'memo' => $this->input->post('memo'),
			'userid' => $this->session->userdata('account')['userid'],
			'createDate' => $createDate,
			'docfilenames' => $this->input->post('docfilenames'),
			'docfiles' => $this->input->post('docfiles'),
			'largeImagefilenames' => $this->input->post('largeImagefilenames'),
			'largeImagefiles' => $this->input->post('largeImagefiles'),
			'smallImagefilenames' => $this->input->post('smallImagefilenames'),
			'smallImagefiles' => $this->input->post('smallImagefiles')
			);

		
			$this->product_model->add($data);
	}

	//修改帳號
	public function modify($userid){
			date_default_timezone_set('Asia/Taipei');
			$datestring = "%Y-%m-%d %H:%i:%s";
			$time = time();
			$createDate = mdate($datestring, $time);

		$pid = $this->input->post('pid');
		$userid = $this->input->post('userid');
		$data = array(
			'name' => $this->input->post('name'),
      		'brand' => $this->input->post('brand'),
      		'model' => $this->input->post('model'),
      		'icampus_category' => implode (",", $this->input->post('icampus_category')),
			'product_category' => implode (",", $this->input->post('product_category')),
      		'level' => implode (",", $this->input->post('level')),
      		'introduction' => $this->input->post('introduction'),
      		'description' => $this->input->post('description'),
      		'techSpec' => $this->input->post('techSpec'),
      		'youtubeURL' => $this->input->post('youtubeURL'),
      		'homepage' => $this->input->post('homepage'),
			'warranty' => $this->input->post('warranty'),
      		'deployment' => $this->input->post('deployment'),
			'training' => $this->input->post('training'),
			'unitPrice' => $this->input->post('unitPrice'),
      		'batchPrices' => $this->input->post('batchPrices'),
			'pricesDetail' => $this->input->post('pricesDetail'),
			'unitPrice_si' => $this->input->post('unitPrice_si'),
      		'batchPrices_si' => $this->input->post('batchPrices_si'),
			'pricesDetail_si' => $this->input->post('pricesDetail_si'),			
      		'contact_name' => $this->input->post('contact_name'),
      		'company_site' => $this->input->post('company_site'),
      		'company_name' => $this->input->post('company_name'),
			'tel' => $this->input->post('tel'),
			'email' => $this->input->post('email'),      
			'memo' => $this->input->post('memo'),
			'createDate' => $createDate,
			'docfilenames' => $this->input->post('docfilenames'),
			'docfiles' => $this->input->post('docfiles'),
			'largeImagefilenames' => $this->input->post('largeImagefilenames'),
			'largeImagefiles' => $this->input->post('largeImagefiles'),
			'smallImagefilenames' => $this->input->post('smallImagefilenames'),
			'smallImagefiles' => $this->input->post('smallImagefiles')
			);

	
		
		$this->product_model->modify($pid,$data);
	}

	//刪除
	public function remove($pid){
		$this->product_model->remove($pid);
	}

	//搜尋
	public function search($offset=0){

        $query = null;
        if($this->input->post('search') != null ){
        	$query = array(
	        	'search' => $this->input->post('search'), 
	      		'icampus_category' => $this->input->post('icampus_category'),
				'product_category' => $this->input->post('product_category'),
	      		'level' => $this->input->post('level')
        	);        	
    		$newdata = array('product_search_query'  => $query);
           	$this->session->set_userdata($newdata);
        }else{
        	$query = $this->session->userdata('product_search_query');
        }

       
        $data['search_json']  = json_encode($query,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);;       
		
		//分頁
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] =  base_url() .'product/search';
        $config['per_page'] = 10;
        $config['total_rows'] = $this->product_model->searchList_total_rows($query) ;
        $this->pagination->initialize($config);

        $data["offset"] = $offset;
        $data["links"] = $this->pagination->create_links();	
        $this->session->set_userdata(array('config'  => $config));

        $list = $this->product_model->searchList($query,$offset,$config['per_page']);
		$data['list']  = $list;	

		$this->theme->load('product/viewList',$data);                
	}

	//show list data
	public function viewList($offset=0){
        //分頁
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] =  base_url() .'product/viewList';
        $config['total_rows'] = $this->product_model->viewList_total_rows();        
        $config['per_page'] = 10 ;     

        $this->pagination->initialize($config);
        $data["offset"] = $offset;
        $data["links"] = $this->pagination->create_links();

		$list = $this->product_model->viewList($offset,$config['per_page']);
		$data['list']  = $list;        
	  	$this->theme->load('product/viewList',$data);
	}

	public function review($offset=0){
        $this->viewList($offset);
	}

	function verify(){
		$pid = $this->input->post('pid');
		$isVerify = $this->input->post('isVerify');
		$this->product_model->verify($pid, $isVerify);
		echo "OK";
	}

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('account_model');
		$this->load->model('product_model');
		
	}
	//列表
	public function listRoleAccount()
	{
		$roleArray = array("root", "si", "buyer", "vendor");
		$role =  $this->input->get('role');
		if (!in_array($role, $roleArray)) { //非預期角色
		    $data  = array('error_msg' => 'role error happened');
			$this->theme->noAuthLoad('error',$data);
		}


		$accounts = $this->account_model->roleAccountList($role);
	  	$data['accounts']  = $accounts;
	  	$data['role']  = $role;
        
	  	$this->theme->load('account/listRoleAccount',$data);
	}
	//登出
	public function logout(){
		$this->session->unset_userdata('account');
		$this->session->unset_userdata('isShowPrice');
		$this->session->unset_userdata('buyer_search_query');
		$this->session->unset_userdata('product_search_query');
		$this->session->unset_userdata('solution_search_query');
		$this->session->unset_userdata('solution_add');
		$this->session->unset_userdata('solutionArr');


		$newdata = array(
                   'logged_in' => FALSE
               	);
        $this->session->set_userdata($newdata);
        redirect('home');
	}

	//新增帳號
	public function add(){
		$data = array(
			'userid' => $this->input->post('userid'),
			'company_name' => $this->input->post('company_name'),
			'company_site' => $this->input->post('company_site'),
			'contact_name' => $this->input->post('contact_name'),
			'tel' => $this->input->post('tel'),
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password'),
			'role' => $this->input->post('role'),
			'creator_id' => $this->session->userdata('account')["userid"]
			);

		
			$this->account_model->add($data);
	}

	//修改帳號
	public function modify($userid){
		$data = array(
			'company_name' => $this->input->post('company_name'),
			'company_site' => $this->input->post('company_site'),
			'contact_name' => $this->input->post('contact_name'),
			'tel' => $this->input->post('tel'),
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password'),
			'role' => $this->input->post('role')
			);

		
		$this->account_model->modify($userid,$data);
	}

	//刪除帳號
	public function remove($userid){
		$this->account_model->remove($userid);
	}


	//檢查userId 是否存在
	public function isUserExist(){
		$userid = 	$this->input->get('userid');
		$accountData = $this->account_model->findOne($userid);

        if($accountData!=null){
            echo "false";
        }else{
        	echo "true";
        }
	}


	public function getInfo($userid){
		$accountData = $this->account_model->findOne($userid);
		echo json_encode($accountData,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
	}
	//登入
	public function login()
	{

		$this->load->library('form_validation');

		$this->form_validation->set_rules('userid', 'userid', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$data  = array('error_msg' => 'userid or password is required!');
			$this->theme->noAuthLoad('error',$data);
		}else{
			//登入
			$userid =  $this->input->post('userid');
			$password =  $this->input->post('password');

	        $accountData = $this->account_model->findOne($userid);

	        if($accountData!=null && $accountData['password'] == $password){
	            //寫入session
	        	
	        	$newdata = array(
                   'account'  => $accountData,
                   'logged_in' => TRUE
               	);
               	$this->session->set_userdata($newdata);
	            //redirect('/');
               	$this->session->set_userdata(array('needVerfyNum'  => $this->product_model->needVerfyNum()));



               	if($this->session->userdata('account')['role']=="root"){
               		$this->theme->load('login_view');
               	}else if($this->session->userdata('account')['role']=="si"){
               		$this->theme->load('login_view');
               	}else if($this->session->userdata('account')['role']=="vendor"){
               		redirect('product/viewList');
               	}else{
               		redirect('buyer/viewList');
               	}


	        }else{
	        	$data  = array('error_msg' => 'userid or password is wrong!');
				$this->theme->noAuthLoad('error',$data);
	        }
		}//if ($this->form_validation->run() == FALSE)
		
	}//login()

	public function permissionSiUserIds($userid){
		$result = $this->account_model->permissionSiUserIds($userid);
		$array = array();
		foreach ($result as $row) {
			array_push($array, $row->permission_userid);
		}
		echo json_encode($array,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);      
	}//permissionSiUserIds

	public function siUserIds(){
		$result = $this->account_model->siUserIds();
		$array = array();
		foreach ($result as $row) {
			array_push($array, $row->userid);
		}
		echo json_encode($array,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);      
	}//siUserIds	


	

	public function savePersmssion(){


		$userid =  $this->input->post('userid');
		//清除
		$this->account_model->clearPersmssion($userid);

		$list =  $this->input->post('list');
		if($list==""){

			echo "OK"; 
			return;
		}

		$array= explode(',', $list);
		$datas = array();
		foreach ($array as $item) {
			if($item==""){continue;}
			$data = array(
			'userid' => $userid,
			'permission_userid' => $item
			);

			array_push($datas, $data);
		}
		//die(print_r($datas));
		//die(sizeof($datas));

		//
		$this->account_model->savePersmssion($userid,$datas);
		echo "OK";
	}//savePersmssion	

	public function editForm(){
		$userid = $this->session->userdata('account')["userid"];
		$account = $this->account_model->findOne($userid);

		if(empty($account["profileImg"])){
			$account["profileImg"] = "defaultProfile.jpg";
		}

		//$data = array("account"=>$account);
		$this->theme->load("account/editForm",$account);
	}

	//修改個人資訊
	public function modifyPersonalInfo(){
		$data = array(
			'company_name' => $this->input->post('company_name'),
			'company_site' => $this->input->post('company_site'),
			'contact_name' => $this->input->post('contact_name'),
			'tel' => $this->input->post('tel'),
			'email' => $this->input->post('email')
			);
		$this->account_model->modify($this->session->userdata('account')['userid'],$data);

		//重新取得資料，寫入session
		$accountData = $this->account_model->findOne($this->session->userdata('account')['userid']);
    	$newdata = array(
           'account'  => $accountData,
           'logged_in' => TRUE
       	);
       	$this->session->set_userdata($newdata);		
	}//function modifyPersonalInfo()	

	//修改個人密碼
	public function modifyPersonalPw(){
		if($this->input->post('password') == $this->session->userdata('account')['password']){ //原密碼不和
			if($this->input->post('new_password') == $this->input->post('check_password') && $this->input->post('new_password')!=""){
				$data = array(
							'password' => $this->input->post('new_password')							
							);
				$this->account_model->modify($this->session->userdata('account')['userid'],$data);
				echo "OK";
			}else{
				echo "New Password is not equals checked password";
			}
		}else{
			echo "Origin password is not Correct!";
		}
		
	}//function modifyPersonalPw()


	//儲存個人圖片
	public function savePersonalProfile($profileImg){
		$data = array(
					'profileImg' => $profileImg							
					);
		$this->account_model->modify($this->session->userdata('account')['userid'],$data);
		echo "OK";
	}//function savePersonalProfile()

	//語系設定
	public function locale($lang){
		$locale = array('locale'=>$lang);
		$this->session->set_userdata($locale);

		$uri = $this->input->post('uri');		  
		redirect($uri, 'refresh');
	}	
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('comment_model');

		
	}
	public function postComment()
	{
		date_default_timezone_set('Asia/Taipei');
		$datestring = "%Y-%m-%d %H:%i:%s";
		$time = time();
		$timestamp = mdate($datestring, $time);
		$data = array(
      		'sid' => $this->input->post('sid'),
      		'userid' => $this->session->userdata('account')['userid'],
      		'content' => $this->input->post('content'),
      		'timestamp' => $timestamp
			);

		
		$this->comment_model->postComment($data);		
	}

	//回應問題
	function replyComment($cid)
	{
		date_default_timezone_set('Asia/Taipei');
		$datestring = "%Y-%m-%d %H:%i:%s";
		$time = time();
		$timestamp = mdate($datestring, $time);
		$data = array(
      		'feedback' => $this->input->post('feedback'),
      		'feedback_timestamp' => $timestamp
			);	
		$this->comment_model->replyComment($cid, $data);	
	}
}

